from typing import Optional, List, Dict, Tuple
from pyqtree import Index
from operator import itemgetter
import networkx as nx
import numpy as np

import dsd.hydra.benchmarks.models.schemas as schemas
import dsd.hydra.benchmarks.models.compare_schema as cs
from dsd.hydra.benchmarks.models.schemas import GroundTruth, Prediction, Datasets
from dsd.hydra.benchmarks.modules.metrics import (
    MetricsCalculator,
    generate_metrics_list,
    get_iou_component_list,
    evaluate_file_quality_metrics,
    evaluate_subset_quality_metrics,
)


def calculate_iou(
    parent: str, children: List, bbox_dict: Dict, threshold: float
) -> cs.CalculatedIOU:
    """
    Calculates the iou of multiple bounding boxes to one bounding boxes.
    The multiple bounding boxes could be groundtruth or prediction boxes, and
    the same for the one bounding boxes.

    Arguments:
        - parent (str): A key to extract bounding box coordinates
        - children (list): A list of keys to extract to extract bounding
                           box coordinates
        - bbox_dict (Dict): Dictionary of all gt and pred bboxes for the file that
                            is being investigated.
        - threshold (float): IOU threshold

    Returns:
        - (CalculatedIOU): A dataclass containing a list with IOU for
                           each parent and child

    """

    parent_box = np.array(bbox_dict[parent].bounding_box)
    # children_boxes = np.array(list(itemgetter(*children)(bbox_dict)))
    children_boxes_info = itemgetter(*children)(bbox_dict)
    # If a single child is found then only a single BBoxInfo object is passed
    if isinstance(children_boxes_info, cs.BBoxInfo):
        children_boxes_info = [children_boxes_info]
    children_boxes = np.array([x.bounding_box for x in children_boxes_info])

    if len(children_boxes.shape) < 2:
        children_boxes = children_boxes.reshape((-1, children_boxes.shape[0]))

    # 1. calculate the inters coordinate
    if children_boxes.shape[0] > 0:
        ixmin = np.maximum(children_boxes[:, 0], parent_box[0])
        ixmax = np.minimum(children_boxes[:, 2], parent_box[2])
        iymin = np.maximum(children_boxes[:, 1], parent_box[1])
        iymax = np.minimum(children_boxes[:, 3], parent_box[3])

        iw = np.maximum(ixmax - ixmin + 1.0, 0.0)
        ih = np.maximum(iymax - iymin + 1.0, 0.0)

        # 2.calculate the area of inters
        inters = iw * ih

        # 3.calculate the area of union
        uni = (
            (children_boxes[:, 2] - children_boxes[:, 0] + 1.0)
            * (children_boxes[:, 3] - children_boxes[:, 1] + 1.0)
            + (parent_box[2] - parent_box[0] + 1.0)
            * (parent_box[3] - parent_box[1] + 1.0)
            - inters
        )

        # 4.calculate the overlaps and find the max overlap ,the max overlaps
        #   index for pred_box
        iou = inters / (uni + threshold)

        iou_list = []

        for iou_value, child_value in zip(iou, children):
            iou_model = cs.IOU(parent=parent, child=child_value, iou_value=iou_value)
            iou_list.append(iou_model)

        calculated_iou = cs.CalculatedIOU(iou=iou_list)

        return calculated_iou


def get_region_connected_components(  # noqa C901
    bbox_identifier: str, file_regions: cs.PageRegions, threshold: float
) -> List[cs.Components]:
    """
    Computes the connected components to a bounding box from the
    predicitions and groundtruth bounding boxes.

    Arguments:
        - bbox_identifier (str): A key to extract bounding box coordinates
        - file_regions (PageRegions): A dataclass containing quad trees, bbox keys,
                                        bbox dictionary, node keys and texts
        - threshold (float): IOU threshold

    Returns:
        - (Components): A dataclass containing all the connected components

    """
    connected_boxes = []
    children_boxes = [bbox_identifier]
    G = nx.Graph()
    while children_boxes:

        children = []
        temp_children = []

        for _ in range(len(children_boxes)):

            parent = children_boxes.pop(0)
            connected_boxes.append(parent)

            # Get parent bounding box
            parent_bbox = file_regions.bbox_dict[parent].bounding_box

            # When parent has pred keyword
            # parent is pred node, tree is truth_tree
            if parent.startswith("pred"):
                # Get list of all intersecting bounding box identifiers
                # from truth_tree
                children = file_regions.truth_tree.intersect(parent_bbox)

            # when parent has truth keyword
            # parent is pred node, tree is pred_tree
            else:
                # Get list of all intersecting bounding box identifiers
                # from pred_tree
                children = file_regions.pred_tree.intersect(parent_bbox)

            # If interecting child nodes are found for this parent node
            if len(children) > 0:
                # Get list of tuples containing parent, child, and IOU
                calculated_iou_list = calculate_iou(
                    parent, children, file_regions.bbox_dict, threshold
                )

                nodes_iou = list(
                    map(lambda iou: tuple(iou.dict().values()), calculated_iou_list.iou)
                )
                # Add parent, child, and IOU to graph
                G.add_weighted_edges_from(nodes_iou)
                # Add children to temporary list
                temp_children.extend(children)

            # If no child node is found for this parent
            else:
                G.add_node(parent)

        children_boxes = list(set(temp_children).difference(set(connected_boxes)))

    cc_list = []
    while G.number_of_nodes() > 0:

        if G.number_of_edges() > 0:
            # Get edge from current graph with highest IOU
            # Shape of an edge : { ("node1_id", "node2_id") : {"weight" : <IOU value>} }
            max_edge = max(dict(G.edges).items(), key=lambda x: x[1]["weight"])
            # max_edge[0] --> ("node1_id", "node2_id")
            # max_edge[1] --> {"weight" : <IOU value>}
            iou = max_edge[1]["weight"]
        else:
            nodes = list(G.nodes)
            max_edge = [(nodes.pop(0), None)]
            iou = 0

        # Add edge and IOU to a list
        # max_edge[0][0] --> "node1_id". ideally should be pred**
        # max_edge[0][1] --> "node2_id". ideally should be truth**
        # max_edge[1] --> {"weight" : <IOU value>}
        if "truth" in max_edge[0][0]:
            pred_key = max_edge[0][1]
            truth_key = max_edge[0][0]
        else:
            pred_key = max_edge[0][0]
            truth_key = max_edge[0][1]

        # The predicted bounding box info for this connected component
        if pred_key in file_regions.bbox_dict.keys():
            pred_box = file_regions.bbox_dict[pred_key].bounding_box
            pred_text = file_regions.bbox_dict[pred_key].text
        else:
            pred_box = (0.0, 0.0, 0.0, 0.0)
            pred_text = None

        # The gt bounding box info for this connected component
        if truth_key in file_regions.bbox_dict.keys():
            truth_box = file_regions.bbox_dict[truth_key].bounding_box
            truth_text = file_regions.bbox_dict[truth_key].text
        else:
            truth_box = (0.0, 0.0, 0.0, 0.0)
            truth_text = None

        cc_list.append(
            cs.Components(
                pkey=pred_key,
                tkey=truth_key,
                iou=iou,
                pbb=pred_box,
                tbb=truth_box,
                ptext=pred_text,
                ttext=truth_text,
            )
        )

        # Remove nodes
        # Removes edge from graph with key : ("node1_id", "node2_id")
        # if any edges are connected to node1_id and node2_id they are
        # removed from the graph
        G.remove_nodes_from(max_edge[0])

    return cc_list


def get_page_connected_components(
    page_number: int, page_regions: cs.PageRegions, min_iou_threshold: float
) -> cs.PageComponents:
    """
    Retrieves the connected components for each file. The connected components
    could include any type of region example : Tables, Paragraphs, Words.

    Arguments:
        - page_number (int): page in the file
        - page_regions (PageRegions) : Model that stores dictionaries of pred and
                                       gt bboxes as well as pred and gt trees to
                                       encapsulate all regions of a file
        - min_iou_threshold (float) : Minimum iou threshold to use to calculate

    Returns:
        - (PageComponents) : List of Components (regions) that are connected
                                Example : pred0 is connected to truth0


    """

    # ---- Generate connected components and get IOU's for regions of this file ----
    cc_list = []

    while len(page_regions.node_keys) > 0:

        bbox_identifier = page_regions.node_keys.pop(0)

        # Generate a list of { (pred_key, truth_key) :
        #                      {pred : pred_key, truth : truth_key, iou : iou} }
        region_cc_list: List[cs.Components] = get_region_connected_components(
            bbox_identifier, page_regions, min_iou_threshold
        )

        # Loop over the list of connected components
        # This loop will remove elements from the trees
        for node in region_cc_list:
            pred_key = node.pkey
            truth_key = node.tkey

            if pred_key:
                page_regions.pred_tree.remove(
                    pred_key, page_regions.bbox_dict[pred_key].bounding_box
                )

            if pred_key in page_regions.node_keys:
                page_regions.node_keys.remove(pred_key)

            if truth_key:
                page_regions.truth_tree.remove(
                    truth_key, page_regions.bbox_dict[truth_key].bounding_box
                )

            if truth_key in page_regions.node_keys:
                page_regions.node_keys.remove(truth_key)

            # # Add the connected component to a list
            cc_list.append(cs.Components(**node.dict()))

    page_component = cs.PageComponents(page=page_number, components=cc_list)
    return page_component


def get_region_np(regions: List[schemas.RegionInfo], level: str) -> np.array:
    """
    Get the coordinates of all regions in the page and provide it
    as a numpy array

    Arguments:
        - regions (List[RegionInfo]) : List of regions in the page
        - level (str): The level of region for evaluation:
                        - level=regions -> tables, ocr text, blinding text (Default)
                        - level=regions_text -> cells within tables

    Returns:
        - (np.array) : A numpy array of region coordinates in the page

    """
    regions_list = []
    if level == cs.RegionLevels.regions.value:
        regions_list = [region.coordinates for region in regions]

    elif level == cs.RegionLevels.regions_text.value:
        for region in regions:
            for row in region.text:
                for cell in row:
                    regions_list.append(cell.cell_coordinates)

    regions_np = np.array(regions_list)

    return regions_np


def get_page_w_h(
    gt_page: schemas.PageInfo, pred_page: schemas.PageInfo, level: str
) -> Tuple:
    """
    Get the width and height of the page for the Quad trees

    Arguments:
        - gt_page (PageInfo) : GT Page with number and region lists
        - pred_page (PageInfo) : Pred Page with number and region lists
        - level (str): The level of region for evaluation:
                        - level=regions -> tables, ocr text, blinding text (Default)
                        - level=regions_text -> cells within tables

    Returns:
        - (Tuple) : The (width, height) of the page to be used to build the
                    quad tree

    """

    gt_regions, pred_regions = [], []

    # If gt does not have any regions
    if len(gt_page.regions) == 0:
        pred_regions = get_region_np(regions=pred_page.regions, level=level)
        gt_regions = pred_regions
    # if pred page does not have any regions
    elif len(pred_page.regions) == 0:
        gt_regions = get_region_np(regions=gt_page.regions, level=level)
        pred_regions = gt_regions
    # if both pages (gt and pred) have some regions
    else:
        gt_regions = get_region_np(regions=gt_page.regions, level=level)
        pred_regions = get_region_np(regions=pred_page.regions, level=level)

    gt_xmax = max(gt_regions[:, 2])
    gt_ymax = max(gt_regions[:, 3])

    pred_xmax = max(pred_regions[:, 2])
    pred_ymax = max(pred_regions[:, 3])

    xmax = max(gt_xmax, pred_xmax)
    ymax = max(gt_ymax, pred_ymax)

    return (xmax, ymax)


def evaluate_subset_file_regions(  # noqa C901
    gt_: GroundTruth,
    prediction: Prediction,
    level: str,
    iou_values: List[float],
    min_iou_threshold: float,
) -> Tuple[List[cs.FileInfo], List[cs.FileComponents]]:

    """
    Computes the data from ground truths, output files, and iou list to return a
    list FileComponents dataclasses. It also computes the confusion matrix for
    each file and constantly updates the global subset confusion matrix

    Arguments:
        - gt_ (GroundTruth): A pydantic model that holds all region info about the
                             subset ground truth
        - prediction (GroundTruth): A pydantic model that holds all region info about
                                the subset predictions provided by an algorithm
        - level (str): The level of region for evaluation:
                        - level=regions -> tables, ocr text, blinding text (Default)
                        - level=regions_text -> cells within tables
        - iou_values (List): List of IOU values to evalute the detetion performance
                              on
        - min_iou_threshold (float) : Minimum iou threshold to use to calculate

    Returns:
        - (List[FileInfo], List[FileComponents]):
                        - List[FileInfo] -> List of detection metrics for each file
                                            in the subset
                        - List[FileComponents] -> List of IOU components within which
                                            a list of PageComponents with their
                                            updaed connected components list is provided
    """

    # Create a new object for every file that is evaluated
    metric_calc = MetricsCalculator(iou_values)

    # List of predictions for each file in subset
    pred_file_list = []
    gt_file_list = []

    file_info_list: List[cs.FileInfo] = []

    # Ensure that the files are sorted in pred.files as per gt_.files
    for gt_file in gt_.files:
        for pred_file in prediction.files:
            if pred_file.filename == gt_file.filename:
                pred_file_list.append(pred_file)
                gt_file_list.append(gt_file)
                # The file won't appear more than once hence
                # terminating the loop once file is found.

    # Loop over each file in the subset to generate connected components
    file_connected_component_list: List[cs.FileComponents] = []
    for gt_file, pred_file in zip(gt_file_list, pred_file_list):

        # List[IOUComponent] for each file
        file_iou_component_list: List[cs.IOUComponent] = get_iou_component_list(
            iou_values
        )

        # ensure the pages are sorted in pred_file.pages as per_file.pages
        pred_page_list = []
        gt_page_list = []
        for gt_regions in gt_file.pages:
            for pred_regions in pred_file.pages:
                if pred_regions.page == gt_regions.page:
                    pred_page_list.append(pred_regions)
                    gt_page_list.append(gt_regions)

        # Holds list of page components per page
        page_component_list: List[cs.PageComponents] = []
        # Holds metrics on each page
        page_info_list: List[cs.PageInfo] = []

        # ------------------- Loop over the page -------------------------
        for gt_page, pred_page in zip(gt_page_list, pred_page_list):
            # ---------- Get size of the file and generate GT and Pred tree ----------

            # If page in both gt and pred does not have any regions
            if len(gt_page.regions) == 0 and len(pred_page.regions) == 0:
                continue

            w, h = get_page_w_h(gt_page, pred_page, level)

            # Generate trees for predictions and groudn truth
            page_regions = cs.PageRegions(
                truth_tree=Index(bbox=[0, 0, w, h]),
                pred_tree=Index(bbox=[0, 0, w, h]),
                bbox_dict={},
                pred_keys=[],
                node_keys=[],
            )

            # ------- Pick the region to be evaluated -----------------
            if level == cs.RegionLevels.regions.value:
                # Add GT bboxes to bbox_dict and generate gt_tree
                for index, region in enumerate(gt_page.regions):
                    key = f"truth{index}"
                    page_regions.truth_tree.insert(key, region.coordinates)
                    page_regions.bbox_dict[key] = cs.BBoxInfo(
                        bounding_box=region.coordinates,
                        text=region.text if isinstance(region.text, str) else None,
                    )
                    page_regions.node_keys.append(key)

                # Add Pred bboxes to bbox_dict and generate pred_tree
                for index, region in enumerate(pred_page.regions):
                    key = f"pred{index}"
                    page_regions.pred_tree.insert(key, region.coordinates)
                    page_regions.bbox_dict[key] = cs.BBoxInfo(
                        bounding_box=region.coordinates,
                        text=region.text if isinstance(region.text, str) else None,
                    )
                    page_regions.node_keys.append(key)

            elif level == cs.RegionLevels.regions_text.value:
                # Add GT bboxes to bbox_dict and generate gt_tree
                for region_id, region in enumerate(gt_page.regions):
                    for row_id, row in enumerate(region.text):
                        for cell_id, cell in enumerate(row):
                            key = f"truth_{region_id}_{row_id}_{cell_id}"
                            page_regions.truth_tree.insert(key, cell.cell_coordinates)
                            # page_regions.bbox_dict[key] = cell.cell_coordinates
                            page_regions.bbox_dict[key] = cs.BBoxInfo(
                                bounding_box=cell.cell_coordinates, text=cell.cell_text
                            )
                            page_regions.node_keys.append(key)

                # Add Pred bboxes to bbox_dict and generate pred_tree
                for region_id, region in enumerate(pred_page.regions):
                    for row_id, row in enumerate(region.text):
                        for cell_id, cell in enumerate(row):
                            key = f"pred_{region_id}_{row_id}_{cell_id}"
                            page_regions.pred_tree.insert(key, cell.cell_coordinates)
                            # page_regions.bbox_dict[key] = cell.cell_coordinates
                            page_regions.bbox_dict[key] = cs.BBoxInfo(
                                bounding_box=cell.cell_coordinates, text=cell.cell_text
                            )
                            page_regions.node_keys.append(key)

            # get all the connected components for the page
            page_component: cs.PageComponents = get_page_connected_components(
                gt_page.page, page_regions, min_iou_threshold
            )
            page_component_list.append(page_component)

            # Calculate Page Metrics at multiple IOUs
            # page_metrics : List[Metrics]
            # iou_page_component_list : List[PageComponent] ->
            (
                page_metrics,
                iou_page_component_list,
            ) = metric_calc._compute_page_metrics(page_component)

            # Generate a PageInfo model for this page
            page_info = cs.PageInfo(page=gt_page.page, page_metrics=page_metrics)
            page_info_list.append(page_info)

            # Add to iou based PageComppnent to file list of IOUComponents
            for iou_idx, iou_value in enumerate(iou_values):
                file_iou_component_list[iou_idx].pages.append(
                    # Add the PageComponent at this IOU to the File
                    # level of [IOUComponents]
                    iou_page_component_list[iou_idx]
                )

        # ------ End of Page metrics calculations ------------------------

        file_component = cs.FileComponents(
            name=gt_file.filename, ious=file_iou_component_list
        )
        file_connected_component_list.append(file_component)

        # ------- Loop over each page to add metrics to file --------------
        file_metrics: List[cs.Metric] = generate_metrics_list(iou_values)
        for page_info in page_info_list:
            # Update the file metrics for all pages
            file_metrics = metric_calc._add_metrics(
                parent_metrics=file_metrics,
                child_metrics=page_info.page_metrics,
            )
        # --------- End of loop over each page -----------------------------

        #  Update the file recall, precision and f1 for the file
        file_metrics = metric_calc._calculate_precision_recall_f1(
            metrics_list=file_metrics
        )
        # Generate a FileInfo class for this file
        file_info = cs.FileInfo(
            name=gt_file.filename,
            file_exec_time=pred_file.exec_time,
            file_metrics=file_metrics,
            pages=page_info_list,
        )

        file_info_list.append(file_info)
    # -------- End of loop for file ------------------

    print("--" * 20)
    print("Started Calculating File CER and text accuracy")
    print("--" * 20)
    if (
        (
            prediction.dataset == Datasets.tabular.value
            and level == cs.RegionLevels.regions_text.value
        )
        or prediction.dataset == Datasets.ocr.value
        or prediction.dataset == Datasets.blinding.value
    ):
        evaluate_file_quality_metrics(file_info_list, file_connected_component_list)
    return file_info_list, file_connected_component_list


def analyze_predictions(  # noqa C901
    gt_: GroundTruth,
    pred_file: Prediction,
    region_level: Optional[str] = cs.RegionLevels.regions.value,
    iou_values: Optional[List] = [0.5, 0.6, 0.7, 0.8, 0.9, 0.95],
) -> cs.Algorithm:
    """
    Computes the true positives, false positives and false negatives.

    Arguments:
        - algorithms (list): A list of strings of algorithms
        - subsets (list): A list of strings of subsets
        - dataset (str): The name of the dataset
        - version (str): Version of the dataset to use
        - region_level (str): The level of region for evaluation:
                              - regions -> tables, ocr text, blinding text (Default)
                              - regions_text -> cells within tables
                              - regions_text_words -> words within tables
        - iou_values (List): List of iou values to evaluate the detections on
    Returns:
        - (Algorithm): A dataclass that holds the detection and quality
                     metrics to describe how well the algorithm performed
                     on this subset

    """

    min_iou_threshold = 0.5

    # Use the IOU values in ascending order for all metric calcualtion
    iou_values = sorted(iou_values)

    # Get the list of files and connected component regions within each file
    # This function also computes the confusion matrix for each file and
    # updates the global Subset Confusion Matrix

    file_info_list, file_component_list = evaluate_subset_file_regions(
        gt_=gt_,
        prediction=pred_file,
        level=region_level,
        iou_values=iou_values,
        min_iou_threshold=min_iou_threshold,
    )  # return type: List[FileInfo], List[FileComponents]

    subset_metric_calc = MetricsCalculator(iou_values)
    # ------- Loop over each file to add metrics to subset metric -----
    subset_metrics: List[cs.Metric] = generate_metrics_list(iou_values)
    for file_info in file_info_list:
        # Update the file metrics for all pages
        subset_metrics = subset_metric_calc._add_metrics(
            parent_metrics=subset_metrics,
            child_metrics=file_info.file_metrics,
        )
    # --------- End of loop over each page -----------------------------

    #  Update the file recall, precision and f1 for the file
    subset_metrics = subset_metric_calc._calculate_precision_recall_f1(
        metrics_list=subset_metrics
    )

    algorithm_component = cs.Algorithm(
        name=pred_file.algorithm_name,
        dataset=pred_file.dataset,
        version=pred_file.version,
        subset=pred_file.subset,
        subset_exec_time=pred_file.exec_time,
        subset_metrics=subset_metrics,
        files=file_info_list,
    )

    # Call text evaluation
    # This will internaly update data_components
    print("--" * 20)
    print("Started Calculating Subset CER and text accuracy")
    print("--" * 20)
    if (
        (
            pred_file.dataset == Datasets.tabular.value
            and region_level == cs.RegionLevels.regions_text.value
        )
        or pred_file.dataset == Datasets.ocr.value
        or pred_file.dataset == Datasets.blinding.value
    ):
        evaluate_subset_quality_metrics(algorithm_component)

    return algorithm_component
