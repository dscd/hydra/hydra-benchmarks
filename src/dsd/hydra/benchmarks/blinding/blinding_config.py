from enum import Enum
from typing import List, Tuple

from pydantic import BaseModel


# coordinate pydantic models
class Coordinates(BaseModel):
    coordinates: List[Tuple[float, float, float, float]]


# analyzer configuration pydantic models
class ModelConfig(BaseModel):
    lang_code: str
    model_name: str


class PresidioConfig(BaseModel):
    nlp_engine_name: str
    models: List[ModelConfig]


# annotation pydantic models
class Target(BaseModel):
    text: str
    entity: str


class PageAnnotation(BaseModel):
    page: int
    targets: List[Target]


class BlindingAnnotation(BaseModel):
    entities: List[PageAnnotation]

    class Config:
        arbitrary_types_allowed = True


# General


class Labels(Enum):
    entities = "entities"
    page = "page"
    target = "target"
    targets = "targets"
    entity_type = "entity type"
    entity_types = "entity_type"
    entity = "entity"
    text = "text"
    p_match_found = "p_match_found"
    target_matched = "target_matched"
    match_found = "match_found"
    coordinates = "coordinates"
    exact_coordinates = "exact_coordinates"


class FileTypes(Enum):
    pdf = "pdf"
    yml = "yml"
    txt = "txt"


class FileNames(Enum):
    redacted = "_redacted"
    annotated = "_annotated"
    synthetic_mapping_filename = "file_name_mapping.json"


class LanguageCode(Enum):
    english = "en"
    french = "fr"


class Entities(Enum):
    phone_number = "PHONE_NUMBER"
    location = "LOCATION"
    person = "PERSON"
    email_address = "EMAIL_ADDRESS"
    date_time = "DATE_TIME"
    date = "DATE"
    time = "TIME"
    marital_status = "MARITAL_STATUS"
    pri = "PRI"
    age = "AGE"
    gender = "GENDER"


# Presidio
class PresidioConfigLabel(Enum):
    nlp_engine_name = "nlp_engine_name"
    models = "models"
    lang_code = "lang_code"
    model_name = "model_name"


class AlgorithmName(Enum):
    spacy = "spacy"
    bert = "bert"
    flair = "flair"
    stanza = "stanza"


class Recognizer_Entities(Enum):
    location = "LOCATION"
    loc = "LOC"
    per = "PER"
    person = "PERSON"
    organization = "ORGANIZATION"
    org = "ORG"


# Custom Recognizers
class CustomRecognizers(Enum):
    pri_recognizer = "PRIRecognizer"
    age_recognizer = "AgeRecognizer"
    marital_status_recognizer = "MaritalStatusRecognizer"
    phone_recognizer = "PhoneRecognizer"
    gender_recognizer = "GenderRecognizer"
    date_recognizer = "DateRecognizer"


class Score(Enum):
    pri_threshold = 0.9
    age_threshold = 0.75
    phone_threshold = 0.8


class Deny_List(Enum):
    married = "Married"
    single = "Single"
    male_l = "Male"
    female_l = "Female"
    female_u = "FEMALE"
    male_u = "MALE"
    non_binary = "Non-Binary"
    trans = "Transgender"


class Context(Enum):
    marital_status = "Marital Status"
    gender = "Gender"
    age = "Age"
    phone = "Phone"
    telephone = "Telephone"
    phone_number = "Phone Number"
    contact_number = "Contact Number"
    mobile_number = "Mobile Number"


# Synthetic Data
class Synthetic_Entities(Enum):
    phone_number = "PHONE_NUMBER"
    email_address = "EMAIL_ADDRESS"
    name = "NAME"
    first_name = "FIRST_NAME"
    last_name = "LAST_NAME"
    initials = "INITIALS"
    state_cd = "STATE_CODE"
    org = "ORG"
    gender = "GENDER"
    state = "STATE"
    country = "COUNTRY"
    country_cd = "COUNTRY_CODE"
    pri = "PRI"
    age = "AGE"
    marital_status = "MARITAL_STATUS"
    postal_cd = "POSTAL_CODE"
    nationality = "NATIONALITY"
    date_time = "DATE_TIME"
    city = "CITY"
    address = "ADDRESS"
    others = "OTHERS"
    other_name = "OTHER_NAME"
    other_email = "OTHER_EMAIL_ADDRESS"


class Synthetic_Labels(Enum):
    entity_type = "entity_type"
    replacement_text = "replacement_text"
    target_coordinates = "target_coordinates"
    target_matched = "target_matched"
    fontname = "fontname"
    fontsize = "fontsize"
    origin = "origin"
    page = "page"


class Synthetic_Database(Enum):
    names = "names"
    org_names = "org_names"
    genders = "genders"
    states = "states"
    state_cd = "state_codes"
    postal_cd = "postal_codes"
    cities = "cities"
    countries = "countries"
    country_cd = "country_codes"
    pri = "pri"
    marital_status = "marital_status"
    nationality = "nationality"
    phone_numbers = "phone_numbers"
    addresses = "addresses"
    email_addresses = "email_addresses"
    date_of_births = "date_of_birth"


class Email(Enum):
    address_suffix = "@gmail.com"


class Font(Enum):
    default_font = "helvetica"
