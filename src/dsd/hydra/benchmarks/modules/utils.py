from os.path import join

import yaml
from pydantic import BaseModel


def save_file(output: BaseModel, output_filename: str, output_dir: str) -> None:
    """
    This function takes the above arguments and save the contents as a YAML file.

    Parameters:
        - output (BaseModel): Pydantic's Basemodel which will hold the output data
                              to be saved as a YAML file
        - output_filename (str): A string with the name of the output file.
        - output_dir (str): A string containing the output directory path

    Returns:
        - None
    """

    with open(join(output_dir, output_filename), "w") as output_file:
        yaml.dump(output.dict(), output_file, sort_keys=False)
