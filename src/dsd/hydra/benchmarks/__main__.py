#!/usr/bin/env python
# -*- coding: utf-8 -*-
# -----------------------------------------------------------
# Hydra Module Template
#
# (C) 2022 Statistics Canada
# Author: Andres Solis Montero
# -----------------------------------------------------------
import pprint
from dsd.hydra.benchmarks.config.api import settings
from dsd.hydra.benchmarks.main import app
import sys


def main():
    pprint.pprint(settings.dict(), width=1)
    app()


if __name__ == "__main__":
    sys.exit(main())
