import json
import logging
import random
import re
from collections import namedtuple
from enum import Enum
import os
from os.path import dirname, join, realpath
from pathlib import Path
from typing import Dict, List, NamedTuple, Tuple

import pandas as pd
import yaml
from dsd.hydra.benchmarks.blinding.blinding_config import (
    FileNames,
    FileTypes,
    Synthetic_Entities,
    Synthetic_Labels,
)

logger = logging.getLogger(__name__)


def remove_punctuation_list(text_list: List[str]) -> List[str]:
    """
    Function to remove punctuations from list of text

    Argument(s):
        - text_list(List[str]): list of text to be processed

    Returns:
        - List[str]: list of text with punctuations removed
    """

    return [remove_punctution_text(text) for text in text_list]


def remove_punctution_text(text: str) -> str:
    """
    Function to remove punctuations from given text, except if the text
    is an email address.

    Argument(s):
        - text(str): dataframe to be saved

    Returns:
        - str: text with punctions removed. If the recieved is an email address,
        it will return it as is
    """

    email_res = r"^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,}$"
    if re.match(email_res, text):
        return text
    else:
        return re.sub(r"[‘’'][s]|[^\w\s\'\’\‘]", "", text)


def bb_intersection_over_union(
    boxA: Tuple[float, float, float, float], boxB: tuple[float, float, float, float]
) -> float:
    """This function is used to determine the (x, y)-
    coordinates of the intersection rectangle and calculate the area
        Args:
        - boxA (Tuple[float, float, float, float]): bounding box A
        - boxB (Tuple[float, float, float, float]): bounding box B
    Returns:
        - iou (float): intersectiong over union of the two bounding box
    """
    xA = max(boxA[0], boxB[0])
    yA = max(boxA[1], boxB[1])
    xB = min(boxA[2], boxB[2])
    yB = min(boxA[3], boxB[3])
    # compute the area of intersection rectangle
    interArea = max(0, xB - xA + 1) * max(0, yB - yA + 1)
    # compute the area of both the prediction and ground-truth
    # rectangles
    boxAArea = (boxA[2] - boxA[0] + 1) * (boxA[3] - boxA[1] + 1)
    boxBArea = (boxB[2] - boxB[0] + 1) * (boxB[3] - boxB[1] + 1)
    # compute the intersection over union by taking the intersection
    # area and dividing it by the sum of prediction + ground-truth
    # areas - the interesection area
    iou = interArea / float(boxAArea + boxBArea - interArea)
    # return the intersection over union value
    return iou


def file_to_list(filepath: str) -> List[str]:
    """
    Function to read synthetic database files
    Args:
        - input (str): path to file
    Returns:
        - List[str]: List of strings read from datafile
    """
    with open(filepath) as f:
        lines = f.read().splitlines()
    return lines


class ConfigFile(Enum):
    """
    Enumerator containig all Configuration files
    """

    config = "synthetic_data_config.yml"


def load_config() -> NamedTuple:
    """
    Function to load synthetic configurations
    Args:
        - None
    Returns:
        - NamedTuple: all stored configurations

    """
    data = {}
    directory = dirname(realpath(__file__))
    configfile = join(directory, ConfigFile.config.value)
    with open(configfile, "r") as stream:
        try:
            data = yaml.safe_load(stream)
        except yaml.YAMLError as exc:
            logger.info(exc)
    return namedtuple("Config", data.keys())(*data.values())


def get_random_text(text: str, data_list: List[str]) -> str:
    """
    Function to find a replacement text for the given original text

    Argument(s):
        - text(str): original text
        - data_list(List[str]): replacement text database

    Returns:
        - str : replacement string
    """

    threshold = len(text)
    maximum = threshold + 5
    found = True
    while found:
        fdl = [data for data in data_list if len(data) == threshold]
        if fdl:
            found = False
            replacement = random.sample(fdl, 1)[0]
        else:
            threshold += 1
            if threshold == maximum:
                replacement = random.sample(data_list, 1)[0]
                found = False
    return replacement


def dict_to_json(data: Dict[str, str], output_dir: Path) -> None:
    """
    Function to save a dictionary to a json file
    Args:
        - data (Dict[str, str]): mapping data
        - output_dir (str): Path to save the file at
    Returns:
        - None
    """
    synthetic_mapping_path = os.path.join(
        output_dir, FileNames.synthetic_mapping_filename.value
    )
    with open(synthetic_mapping_path, "w") as fp:
        json.dump(data, fp)


def get_synthetic_file_name(synthetic_data: pd.DataFrame) -> str:
    """
    Function to generate synthetic pdf file name
    Args:
        - synthetic_data (Dict[str, str]): path to file
    Returns:
        - synthetic_filename_pdf (str)
    """
    original_name = synthetic_data[
        synthetic_data[Synthetic_Labels.entity_type.value]
        == Synthetic_Entities.name.value
    ]
    synthetic_name = (
        original_name.squeeze()[Synthetic_Labels.replacement_text.value].split()
        if len(original_name) == 1
        else synthetic_data[
            synthetic_data[Synthetic_Labels.entity_type.value]
            == Synthetic_Entities.name.value
        ][Synthetic_Labels.replacement_text.value]
        .iloc[0]
        .split()
    )  # noqa E501

    synthetic_filename_pdf_replaced_name = "_".join(
        name.lower() for name in synthetic_name
    )  # noqa E501
    synthetic_filename_pdf = (
        synthetic_filename_pdf_replaced_name + "." + FileTypes.pdf.value
    )  # noqa E501
    return synthetic_filename_pdf
