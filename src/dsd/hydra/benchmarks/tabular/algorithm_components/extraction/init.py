from typing import Tuple

import numpy as np
from dsd.hydra.benchmarks.models.schemas import PageInfo, TabularConfig


# Base class for all extraction solutions
class ImageExtract:
    """
    Base class for Image to PDF OCR

    Arguments:
        allows_array (bool): Boolean indicating if numpy arrays are supported by the
            OCR module

    Returns:
        (ImageExtract): Base class for Image to PDF OCR
    """

    def __init__(self, allows_array: bool = True):
        self.allows_array = allows_array

    def extract_text(
        self,
        image: np.array = None,
        dims: Tuple[int, int] = None,
        filename: str = None,
        table_bbox: Tuple[float, float, float, float] = None,
        data_dir: str = None,
        config: TabularConfig = None,
    ) -> PageInfo:
        """
        Returns PageInfo after OCR of Image to PDF text. Interface to be implemented
        using specific modules

        Arguments:
            image (np.array) : Image to extract text from as a numpy array
            dims (Tupe[int, int]) : Dimensions of the image if in numpy array format
            filename (str) : Name of image file to extract text from
            page (int) : Page number to extract text
            data_dir (str) : Path to dataset
            config (TabularConfig) : Tabular module configurations

        Returns:
            PageInfo: Model containing word text and bounding box information of a page
        """
        raise NotImplementedError("extract_text must be implemented")
