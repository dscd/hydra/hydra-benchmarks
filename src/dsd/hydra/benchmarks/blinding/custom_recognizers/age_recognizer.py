from typing import List, Optional

from dsd.hydra.benchmarks.blinding.blinding_config import (
    Context,
    Entities,
    Score,
)
from presidio_analyzer import Pattern, PatternRecognizer


class AgeRecognizer(PatternRecognizer):
    """
    Recognize AGE numbers using regex.
    :param patterns: List of patterns to detect
    :param context: List of context words to increase confidence in detection
    """

    PATTERN = r"^(\s)?([2-6]\d)$"
    PATTERNS = [
        Pattern(
            Context.age.value,
            PATTERN,
            Score.age_threshold.value,
        ),
    ]

    CONTEXT = [
        Context.age.value,
    ]

    def __init__(
        self,
        patterns: Optional[List[Pattern]] = None,
        context: Optional[List[str]] = None,
        supported_entity: str = Entities.age.value,
    ):
        patterns = patterns if patterns else self.PATTERNS
        context = context if context else self.CONTEXT
        super().__init__(
            supported_entity=supported_entity,
            patterns=patterns,
            context=context,
        )
