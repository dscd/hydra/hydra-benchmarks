import os
from typing import List

from dsd.hydra.benchmarks.config.api import load_config, load_ocr_algorithms
from dsd.hydra.benchmarks.models.schemas import Datasets, OCREngines, RegionInfo

from mmocr.utils.ocr import MMOCR


class MMocrHelper:
    """
    This a class built on top of the MMOCR system which is based on
    PyTorch and mmdetection.
    It passes the input image through a pipeline of deep learning model
    and returns the OCR output.
    """

    def __init__(self):
        """
        This method is used to initialize the MMOCR with the custom configuration.
        In case a configuration is not selected, the default is used.

        Arguments:
            - None

        Returns:
            - None
        """

        self.config = load_config()
        self.ocr_config = self.config.modules_config.ocr
        self.model_dir = os.path.join(
            self.ocr_config.models_prefix,
            Datasets.ocr.value,
            self.ocr_config.models_version,
        )
        self.algorithm = OCREngines.mmocr.value
        self.algorithms_config = load_ocr_algorithms()

        recog_ckpt = os.path.join(
            self.config.cache_dir,
            self.model_dir,
            self.algorithm,
            self.algorithms_config.mmocr.recog_model_weight,
        )
        recog_config = os.path.join(
            self.config.cache_dir,
            self.model_dir,
            self.algorithm,
            self.algorithms_config.mmocr.recog_model_config,
        )
        det_ckpt = os.path.join(
            self.config.cache_dir,
            self.model_dir,
            self.algorithm,
            self.algorithms_config.mmocr.det_model_weight,
        )
        det_config = os.path.join(
            self.config.cache_dir,
            self.model_dir,
            self.algorithm,
            self.algorithms_config.mmocr.det_model_config,
        )
        self.ocr = MMOCR(
            recog=self.algorithms_config.mmocr.recog_model,
            recog_ckpt=recog_ckpt,
            recog_config=recog_config,
            det=self.algorithms_config.mmocr.det_model,
            det_ckpt=det_ckpt,
            det_config=det_config,
        )

    def get_regions(self, img) -> List:
        """
        This method takes a Pillow image as input and returns a list of Region objects.

        Arguments:
            - img (Image.Image): A Pillow image of what is to be extracted

        Returns:
            - (List): List of Region objects
        """

        # PaddleOCR's function to get both image and data is called and saved as
        # heterogenous list.
        # The details consist of position, confidence and text.
        #         ocr_output = self.ocr.readtext(np.array(img), details=True)
        ocr_output = self.ocr.readtext(img.filename, details=True)

        # defining an empty list to store all regions
        # each block of text detected by the ocr is a region
        regions = []

        # for each region, an RegionInfo model is created and added to the
        # regions list.
        # the coordinates in the OCR output are in the form [(x1,y1),(x2,y1),
        #                                                    (x2,y2),(x1,y2)]
        # which are converted to the standard form of (x1,y1,x2,y2).
        for val in ocr_output[0]["result"]:
            x1 = min(val["box"][0], val["box"][2], val["box"][4], val["box"][6])
            x2 = max(val["box"][0], val["box"][2], val["box"][4], val["box"][6])
            y1 = min(val["box"][1], val["box"][3], val["box"][5], val["box"][7])
            y2 = max(val["box"][1], val["box"][3], val["box"][5], val["box"][7])
            coords = tuple([x1, y1, x2, y2])
            text = val["text"]
            image_region = RegionInfo(coordinates=coords, text=text)
            regions.append(image_region)

        return regions
