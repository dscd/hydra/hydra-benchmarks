Description:
The objective of this specific unit test is to ensure that the function analyze_prediction in main.py performs accurately when called upon to process a typical file input via the OCR module.
The Ground Truth contains 1 file where Region is Present whereas the prediction has 1 file with Region Empty. The expected output should have 1 file where all values in metrics should be 0 except FN,rc and CER.CER for OCR should be 1.0.FN should be based on the number of regions present on the gt file.
