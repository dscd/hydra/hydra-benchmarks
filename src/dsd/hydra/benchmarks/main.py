#!/usr/bin/env python
# -*- coding: utf-8 -*-
# -----------------------------------------------------------
# Hydra Module Template
#
# (C) 2022 Statistics Canada
# Author: Andres Solis Montero
# -----------------------------------------------------------

import logging
import os.path
from os.path import abspath, join, basename, splitext, dirname
from typing import List, Optional

import dash
import typer
import yaml
from dsd.hydra.benchmarks.blinding.generate_predictions import (
    run_prediction_pipeline,
    run_prediction_folder,
)
from dsd.hydra.benchmarks.blinding.redaction import annotate_documents, redact_documents
from dsd.hydra.benchmarks.blinding.synthetic_data_generation.main import generate
from dsd.hydra.benchmarks.config.api import load_config, settings
from dsd.hydra.benchmarks.models.compare_schema import (
    Algorithm,
    CompareOutput,
    RegionLevels,
)
from dsd.hydra.benchmarks.models.schemas import Datasets, GroundTruth, Prediction
from dsd.hydra.benchmarks.modules.compare import analyze_predictions
from dsd.hydra.benchmarks.modules.dashapp import dash_dashapp
from dsd.hydra.benchmarks.modules.download import Download
from dsd.hydra.benchmarks.ocr.generate_synthetic_data import generate_synthetic_data_ocr
from dsd.hydra.benchmarks.ocr.run import (
    run_ocr_prediction_pipeline,
    run_ocr_prediction_pipeline_folder,
)
from dsd.hydra.benchmarks.tabular.pipeline import run_tabular_pipeline
from rich.progress import Progress, SpinnerColumn, TextColumn

logger = logging.getLogger(__name__)

app = typer.Typer()
blinding_app = typer.Typer()
app.add_typer(blinding_app, name="blinding")


@app.command()
def download(
    dataset: str = typer.Argument(..., help="name of the hydra module"),
    version: str = typer.Argument(..., help="version of the dataset to be used"),
    subsets: List[str] = typer.Argument(..., help="subset of the dataset to be used"),
    label: Optional[bool] = typer.Option(False, help="download only ground truth file"),
    data: Optional[bool] = typer.Option(False, help="download only data files"),
) -> None:

    """For downloading the dataset for each module

    Arguments:
        - dataset (str) : Dataset (ocr, blinding, tabular)
        - version (str) : Version of the dataset
        - subsets (List(str)) : Subsets of each dataset that are to be downloaded
        - label (bool): For deciding to download only the ground truth file
        - data (bool): For deciding to download only the label files

    Returns:
        - None
    """

    download_dataset = Download(dataset, subsets, version)
    if label:
        download_dataset.download_ground_truth()
    elif data:
        download_dataset.download_data_files()
    else:
        download_dataset.create_local_directory_structure()
        download_dataset.download_dataset()


@app.command("run-folder")
def run_folder(
    dataset: str = typer.Argument(..., help="name of the hydra module"),
    algorithm_name: str = typer.Argument(..., help="algorithm to be used"),
    input_dir: str = typer.Argument(
        ..., help="directory where the data files are stored"
    ),
    output_dir: str = typer.Argument(
        ..., help="directory where predictions will be stored"
    ),
    ner_threshold: Optional[float] = typer.Option(
        None, help="threshold value for the NER model"
    ),
    model_name: Optional[str] = typer.Option(None, help="type of model to be used"),
) -> None:
    """
    Arguments:
        - dataset (str): Name of the hydra module.
        - algorithm_name (str): Name of the algorithm to be used.
        - input_dir (str): Directory where the data files are stored.
        - output_dir (str): Directory where predictions will be stored.
        - ner_threshold (Optional[float]): Threshold value for the NER model
        - model_name ((Optional[str])): Type of model to be used
    Returns;
        - None
    """
    config = load_config()
    if dataset == Datasets.ocr.value:
        run_ocr_prediction_pipeline_folder(algorithm_name, input_dir, output_dir)

    elif dataset == Datasets.blinding.value:
        run_prediction_folder(
            dataset, algorithm_name, input_dir, output_dir, ner_threshold, model_name
        )

    elif dataset == Datasets.tabular.value:
        run_tabular_pipeline(
            algorithm_name,
            config,
            input_dir,
            output_dir,
        )


@app.command("run-dataset")
def run_dataset(
    dataset: str = typer.Argument(..., help="name of the hydra module"),
    version: str = typer.Argument(..., help="version of the dataset to be used"),
    subset: str = typer.Argument(..., help="subset of the dataset to be used"),
    algorithm_name: str = typer.Argument(..., help="algorithm to be used"),
    output_dir: str = typer.Argument(
        ..., help="directory where predictions will be stored"
    ),
    output_filename: Optional[str] = typer.Option(
        "output_{subset}_{algorithm_name}.yml", help="name of prediction file"
    ),
    ner_threshold: Optional[float] = typer.Option(
        None, help="threshold value for the NER model"
    ),
    model_name: Optional[str] = typer.Option(None, help="type of model to be used"),
) -> None:
    """
    Arguments:
        - dataset (str): Name of the hydra module.
        - version (str): Version of the dataset to be used.
        - subset (str): Subset of the dataset to be used.
        - algorithm_name (str): Name of the algorithm to be used.
        - output_dir (str): Directory where predictions will be stored.
        - output_filename (str): User defined name of the prediction file.
        - ner_threshold (Optional[float]): Threshold value for the NER model
        - model_name ((Optional[str])): Type of model to be used
    Returns:
        - None
    """
    # download the data files
    download(dataset, version, list([subset]), label=False, data=True)
    config = load_config()
    if dataset == Datasets.ocr.value:
        # download the groundtruth file
        download(dataset, version, list([subset]), label=True, data=False)
        groundtruth_file_path = join(
            config.cache_dir,
            config.root_dataset,
            dataset,
            version,
            subset,
            config.gt_dir,
            config.gt_file,
        )
        run_ocr_prediction_pipeline(
            groundtruth_file_path, algorithm_name, output_dir, output_filename
        )

    elif dataset == Datasets.blinding.value:
        run_prediction_pipeline(
            dataset,
            version,
            [subset],
            algorithm_name,
            output_dir,
            ner_threshold,
            model_name,
            input_dir=None,
            run_folder=False,
        )

    elif dataset == Datasets.tabular.value:
        input_dir = join(
            config.cache_dir,
            config.root_dataset,
            dataset,
            version,
            subset,
            config.dt_dir,
        )
        output_dir = join(output_dir, dataset, version, subset)
        run_tabular_pipeline(
            algorithm_name,
            config,
            input_dir,
            output_dir,
            dataset,
            version,
            subset,
        )


@app.command()
def compare(
    pred_files: List[str] = typer.Argument(..., help="List of prediction files"),
    output_file: str = typer.Argument(..., help="Location of compare output results"),
    region_level: Optional[str] = typer.Option(
        RegionLevels.regions.value,
        help="Process block of text / table or cells or words",
    ),
    ious: Optional[List[float]] = typer.Option(
        [0.5, 0.6, 0.7, 0.8, 0.9, 0.95], help="IOU threshold values for evaluation"
    ),
) -> None:
    """Wrapper compare command for each of the modules

    Arguments:
        - pred_files (List[str]) : List of prediction files locations from which
                            we can retrieve dataset, subset and version of gt_file
                            to be used for evaluation.
        - output_file (str) : Location where the file compare result will be saved
        - region_level (str): The level of region for evaluation:
                              - regions -> tables, ocr text, blinding text (Default)
                              - regions_text -> cells within tables
                              - regions_text_words -> words within tables
        - ious (List): List of iou values to evaluate the detections on

    Returns:
        - None
    """
    compare_output = CompareOutput(output=[])
    config = load_config()
    # Iterate through each prediction file
    for prediction_file in pred_files:

        # get absolute path
        pred_file_path = os.path.abspath(prediction_file)

        # Check if prediction file exists
        if not os.path.exists(pred_file_path):
            logger.error(f"Prediction file not found at {pred_file_path}")
            continue

        # Parse the prediction yaml file
        pred = Prediction(**dict(yaml.load(open(pred_file_path), yaml.Loader)))

        # Get the corresponding ground truth yaml file
        with Progress(
            SpinnerColumn(),
            TextColumn("[progress.description]{task.description}"),
            transient=True,
        ) as progress:

            progress.add_task(
                description="Getting Yaml file in the ground truth folder...",
                total=None,
            )

            # Download the ground truth labels
            download(pred.dataset, pred.version, [pred.subset], label=True, data=False)

            # Get the absolute path of the ground truth file
            label_file = abspath(
                join(
                    config.cache_dir,
                    config.root_dataset,
                    pred.dataset,
                    pred.version,
                    pred.subset,
                    config.gt_dir,
                    config.gt_file,
                )
            )

            # Check if the ground truth file exists
            if not os.path.exists(label_file):
                logger.error(f"Ground truth not found at {label_file}")
                continue

            # Parse the ground truth yaml file
            gt_ = GroundTruth(**dict(yaml.load(open(label_file), yaml.Loader)))

        # Analyze the predictions using the ground truth and other parameters
        transition_data: Algorithm = analyze_predictions(
            gt_=gt_, pred_file=pred, region_level=region_level, iou_values=ious
        )

        # Append the output to the compare_output object
        compare_output.output.append(transition_data)

    logger.info("--" * 15)
    logger.info("Started Yaml File Dump")
    logger.info("--" * 15)

    root_path, ext = splitext(output_file)
    output_dir = dirname(root_path)
    output_filename = basename(root_path)
    if region_level == RegionLevels.regions.value:
        filename = f"{output_filename}{ext}"
    else:
        filename = f"{output_filename}_{region_level}{ext}"

    os.makedirs(output_dir, exist_ok=True)

    detection_filename = join(output_dir, filename)

    # Dump the compare_output object to a yaml file
    with open(detection_filename, "w") as f:
        yaml.dump(compare_output.dict(), f, sort_keys=False)

    logger.info("Successfully dumped compare output")


@app.command()
def report(
    detection_file_path: str = typer.Argument(..., help="Path of the detection file")
):
    dash_app = dash.Dash(__name__, requests_pathname_prefix=settings.root_path())
    dash_dashapp(dash_app, detection_file_path)
    dash_app.run(debug=settings.DEBUG, port=settings.PORT)


@app.command("generate")
def generate_synthetic_data(
    dataset: str = typer.Argument(..., help="name of the hydra module"),
    version: str = typer.Argument(..., help="version of the dataset to be used"),
    subsets: List[str] = typer.Argument(..., help="subset of the dataset to be used"),
    out_dir: Optional[str] = typer.Option(
        ".", help="Directory where redacted files will be stored"
    ),
) -> None:
    """Command to generate synthetic data

    Arguments:
        - dataset (str) : module for which synthetic data is being generated
        - version (str) : version of the dataset being used
        - subsets (List[str]) : list of subsets for which the
                                synthetic data is being generated
        - out_dir (str) : Location where the synthetic data files will be stored
    Returns:
        - None
    """
    config = load_config()
    # Loop through each subset and download the specified dataset and version
    for subset in subsets:

        download(dataset, version, subsets, label=False, data=True)

        # Create the path to the directory containing the benchmark data
        benchmark_dataset_directory = os.path.abspath(
            os.path.join(
                config.cache_dir,
                config.root_dataset,
                dataset,
                version,
                subset,
                config.dt_dir,
            )
        )

        # Generate benchmarks for the specified subset
        # and output the results to the specified directory
        if dataset == "ocr":
            generate_synthetic_data_ocr(dataset, version, subset, "", "")
        elif dataset == "blinding":
            generate(benchmark_dataset_directory, out_dir)
        else:
            logger.info("Incorrect dataset entered")


@blinding_app.command("redact")
def redact(
    pred_files: List[str] = typer.Argument(..., help="List of prediction files"),
    out_dir: Optional[str] = typer.Option(
        "redacted_documents", help="Directory where redacted files will be stored"
    ),
) -> None:
    """Command to redact documents based on the prediction file

    Arguments:
        - pred_files (List[str]) : List of prediction files
        - out_dir (str) : Location where the redacted files will be stored
    Returns:
        - None
    """
    # Loop through each prediction file in the list of prediction files
    for prediction_file in pred_files:

        # Get the absolute path of the prediction file
        pred_file_path = os.path.abspath(prediction_file)

        # Load the contents of the prediction file into a Prediction object
        pred = Prediction(**dict(yaml.load(open(pred_file_path), yaml.Loader)))

        # Download the necessary data files (if not already downloaded)
        download(pred.dataset, pred.version, [pred.subset], label=False, data=True)

        # Create the output directory if it doesn't exist
        if not os.path.exists(out_dir):
            os.mkdir(os.path.abspath(out_dir))

        # Redact sensitive information from the documents in the
        # prediction file and save them in the output directory
        redact_documents(pred, out_dir)


@blinding_app.command("annotate")
def annotate(
    pred_files: List[str] = typer.Argument(..., help="List of prediction files"),
    out_dir: Optional[str] = typer.Option(
        "annotated_documents", help="Directory where annotated files will be stored"
    ),
) -> None:
    """Command to annotate documents based on the prediction file

    Arguments:
        - pred_files (List[str]) : List of prediction files
        - out_dir (str) : Location where the annotated files will be stored
    Returns:
        - None
    """
    # Loop through each prediction file in the given list of prediction files
    for prediction_file in pred_files:

        # Get the absolute path of the prediction file
        pred_file_path = os.path.abspath(prediction_file)

        # Load the prediction file as a YAML file and create a Prediction object from it
        pred = Prediction(**dict(yaml.load(open(pred_file_path), yaml.Loader)))

        # Download the data files associated with the prediction
        download(pred.dataset, pred.version, [pred.subset], label=False, data=True)

        # Create the output directory if it does not already exist
        if not os.path.exists(out_dir):
            os.mkdir(os.path.abspath(out_dir))

        # Annotate the documents in the output directory with the predicted labels
        annotate_documents(pred, out_dir)


if __name__ == "__main__":
    app()
