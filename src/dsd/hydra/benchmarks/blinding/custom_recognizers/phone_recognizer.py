from typing import List, Optional

from dsd.hydra.benchmarks.blinding.blinding_config import Context, Entities, Score
from presidio_analyzer import Pattern, PatternRecognizer


class PhoneRecognizer(PatternRecognizer):
    """
    Recognize Phone numbers using regex.
    :param patterns: List of patterns to detect
    :param context: List of context words to increase confidence in detection
    """

    PATTERN = r"(\d{3}[-\.\s]??\d{3}[-\.\s]??\d{4}|\(\d{3}\)\s*\d{3}[-\.\s]??\d{4}|\d{3}[-\.\s]??\d{4})"  # noqa E501
    PATTERNS = [
        Pattern(
            Context.phone.value,
            PATTERN,
            Score.phone_threshold.value,
        ),
    ]

    CONTEXT = [
        Context.phone.value,
        Context.telephone.value,
        Context.phone_number.value,
        Context.contact_number.value,
        Context.mobile_number.value,
    ]

    def __init__(
        self,
        patterns: Optional[List[Pattern]] = None,
        context: Optional[List[str]] = None,
        supported_entity: str = Entities.phone.value,
    ):
        patterns = patterns if patterns else self.PATTERNS
        context = context if context else self.CONTEXT
        super().__init__(
            supported_entity=supported_entity,
            patterns=patterns,
            context=context,
        )
