import os
import tempfile
from os.path import join
from typing import Tuple

from dsd.hydra.benchmarks.tabular.algorithm_components.extraction.utils import (
    get_img_scale,
    rescale_document_content,
)
from dsd.hydra.benchmarks.tabular.algorithm_components.extraction.pymupdf import PMPDF
from dsd.hydra.benchmarks.tabular.utils import generate_pdf
from dsd.hydra.benchmarks.models.schemas import TabularConfig
from dsd.hydra.benchmarks.tabular.models.tabular_schemas import ExtractedContent
from dsd.hydra.benchmarks.tabular.algorithm_components.extraction.init import (
    ImageExtract,
)


class OcrImage(ImageExtract):
    """
    Utility class for Image to PDF text OCR class using ocrmypdf + Pymupdf
    """

    def extract_text(
        self,
        dims: Tuple[int, int],
        filename: str,
        data_dir: str,
        config: TabularConfig,
    ) -> ExtractedContent:

        """Extract the table image content with ocrmupdf
        + the PyMuPDF (original python library) process. PyMuPDF here
        is used the fashion it is meant to be used where the pdf is read
        and the cleaned meta data is then used to processing.
        Note: Steps are also taken to ensure that the word bboxes
        and content match the original image size

        Arguments:
            - dims (Tupe[int, int]): Dimensions of the image if in numpy array format
            - filename (str): Name of image file to extract text from
            - data_dir (str): Path to dataset
            - config (TabularConfig): Tabular module configurations

        Returns:
            - (ExtractedContent): Words and word boxes of the converted PDF along with
                the page image
        """

        # Generate PDF of image using OCRmyPDF
        img_path = join(data_dir, filename)
        pdf_path = tempfile.NamedTemporaryFile().name
        generate_pdf(img_path, pdf_path)

        # Use PDF pipeline's PMPDF to get words, blocks and lines
        # from the new document creaed above
        pmpdf = PMPDF(doc_filename=pdf_path)
        document_content = pmpdf.get_page_content(0)

        file_height, file_width = dims[0], dims[1]
        img_scales = get_img_scale(
            document_content.image,
            file_width,
            file_height,
        )

        # rescale the document content to orginal image size from
        # PyMuPDF image size
        rescaled_document_content = rescale_document_content(
            document_content=document_content,
            img_scale=img_scales,
            org_img_xmin=None,
            org_img_ymin=None,
        )

        # Remove temporary pdf once it is processed
        os.remove(pdf_path)

        return rescaled_document_content
