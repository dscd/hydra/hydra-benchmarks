#!/usr/bin/env python
# -*- coding: utf-8 -*-
# -----------------------------------------------------------
# Hydra Module Template Models
#
# (C) 2022 Statistics Canada
# Author: Andres Solis Montero
# -----------------------------------------------------------

from typing import List, Optional, Tuple, Union
from enum import Enum
from pydantic import BaseModel
from dsd.hydra.benchmarks.tabular.models.tabular_schemas import TabularCell

# ---------- Ground Truth and Prediction file models -----------------


class RegionInfo(BaseModel):
    """
    RegionInfo class representing one annotated region of interest in a
    particular page of the document

    Attributes:
        coordinates (Tuple[float, float, float, float]): The xy coordinates of the
            annotated region of interest. Tuple of four floats in the format of
            (x1, y1, x2, y2) or (x1, y1, w, h)
        text (Optional[Union[int, str, List[List[TabularCell]]]]): Text contained
            in the region of interest. Represented either as a text or a list
            (rows) of list (size the number of columns) of TabularCells
        confidence (Optional[float]): The probability that the proposed region contains
            a region of interest - area to redact for Blinding or text region to extract
            for OCR or Tabular
        exec_time (Optional[float]): The time taken to detect and extract information in
            this region.
    """

    coordinates: Tuple[float, float, float, float]
    text: Optional[Union[int, str, List[List[TabularCell]]]] = None
    # Used in predictions
    confidence: Optional[float] = 0.0
    exec_time: Optional[float] = 0.0


class PageInfo(BaseModel):
    """
    Page class representing the information contained in a page, ie, all
    regions, along with the text.

    Attributes:
        page (int): The page number in the document
        regions (List[RegionInfo]): List of RegionInfo objects for all annotated regions
            in a page
    """

    page: int
    regions: List[RegionInfo]


class FileInfo(BaseModel):
    """
    FileInfo class representing entire information contained in a whole file
    in a subset

    Attributes:
        filename (str): Name of the file
        pages (Optional[List[PageInfo]]): List of Pageinfo objects for all annotated
            pages in the file
        exec_time: (Optional[float]): The time taken to detect regions and extract
            information from all pages of the file
    """

    filename: str
    pages: Optional[List[PageInfo]]
    exec_time: Optional[float] = 0.0


class GroundTruth(BaseModel):
    """
    GroundTruth class representing given actual annotated regions and the text contained
        of a particular subset

    Attributes:
        dataset (str): The primary type of dataset - blinding/ocr/tabular
        version (str): Version of the dataset annotated
        subset (Optional[str]): The subset of the dataset of a particular version
        files: (List[FileInfo]): List of FileInfo objects for every file in the subset
    """

    dataset: str
    version: str
    subset: Optional[str] = None
    files: List[FileInfo]


class Prediction(GroundTruth):
    """
    Prediction class representing the predicted output of the available pipelines.

    Attributes:
        dataset (str): The primary type of dataset - blinding/ocr/tabular
        version (str): Version of the dataset annotated
        subset (Optional[str]): The subset of the dataset of a particular version
        files: (List[FileInfo]): List of FileInfo objects for every file in the subset
        algorithm_name (str): Algorithm used for information extraction or blinding
        exec_time (Optional[float]): Execution time for the entire subset
    """

    algorithm_name: str
    exec_time: Optional[float] = 0.0


# --------- HydraConfig ------------------


class OCREngines(Enum):
    """
    Enumeration to hold all OCREngine names:
        - "tesseract": Name for Tesseract
        - "tesseract": Name for Tesseract
        - "easyocr": Name for EasyOCR
        - "mmocr": Name for MMOCR
        - "paddleocr": Name for Paddle OCR
        - "ocrmypdf": Name for OCRmypdf
        - "calamariocr": Name for Calamari OCR
    """

    tesseract = "tesseract"
    easyocr = "easyocr"
    mmocr = "mmocr"
    paddleocr = "paddleocr"
    ocrmypdf = "ocrmypdf"
    calamariocr = "calamariocr"


class Noise(Enum):
    """
    Noise for ocr synthetic data generation:
        - "pepper": Pepper noise
        - "salt": Salt noise
        - "salt_pepper": Salt and Pepper noise
    """

    pepper = "pepper"
    salt = "salt"
    salt_pepper = "salt_pepper"


class IntensityU8(Enum):
    """
    White and black intensity for data augmentation:
        - "dark": dark intensity number
        - "bright": bright intensity number
    """

    dark = 0
    bright = 255


class OCRConfig(BaseModel):
    """
    Configurations class for configurations specific to the OCR module

    Attributes:
        version (str): Version of the models of various OCR engines.
    """

    version: str


class BlindingConfig(BaseModel):
    """
    Configurations class for configurations specific to the Blinding module

    Attributes:
        prefix (str): Prefix to the path of the dataset directories based on the module
        version (str): Version of the dataset
        redaction_text (str): Text to fill in the redacted regions
        redaction_fill_color (List[int]): RGB color to fill in the redacted region
        redaction_text_color (List[int]): RGB color to fill the text in the redacted
                                          region
        adjustment_factor (float): Factor to adjust font size
    """

    redaction_text: str
    redaction_fill_color: List[int]
    redaction_text_color: List[int]
    adjustment_factor: float


class TabularConfig(BaseModel):
    """
    Configurations class for configurations specific to the Tabular module

    Attributes:
        prefix (str): Prefix to the path of the dataset directories based on the module
        version (str): Version of the dataset
        image_batch_size (Optional[int]) : Batch size / No. of images in a batch
        pdf_page_batch_size (Optional[int]) : Batch size / No. of PDF pages in a batch
        num_workers (Optional[int]) : Number of workers for the data loader
        device (Optional[str]): Platform to load model and run inference - cpu or gpu
        algorithms_config (Optional[str]): COnfigurations for the available algorithms
        iou_thr (Optional[Float]): IoU threshold to use for Table Transformer
        overlap_thr (Optional[float]): Overlap threshold to use to check if a bounding
            box is contained within another
        horizontal_window (Optional[int]): Horizontal window to find peaks in SLICE
        vertical_window (Optional[int]): Vertical window to find peaks in SLICE
    """

    version: Optional[str]
    image_batch_size: Optional[int]
    pdf_page_batch_size: Optional[int]
    num_workers: Optional[int]
    device: Optional[str]
    algorithms_config: Optional[str]
    iou_thr: Optional[float]
    overlap_thr: Optional[float]
    horizontal_window: Optional[int]
    vertical_window: Optional[int]


class HydraModules(BaseModel):
    """
    HydraModule class to initialize configurations to all available modules

    Attributes:
        ocr (OCRConfig): OCRConfig object for configurations pertaining to OCR module
        blinding (BlindingConfig): BlindingConfig object for configurations pertaining
            to Blinding module
        tabular (TabularConfig): TabularConfig object for configurations pertaining to
            Tabular module
    """

    ocr: OCRConfig
    blinding: BlindingConfig
    tabular: TabularConfig


class HydraConfig(BaseModel):
    """
    HydraConfig class that contains all configurations to be used for all commands and
    module pipelines

    Attributes:
        cache_dir (str): Name of cache directory
        root_dataset (str): Name of cache subdirectory
        gt_dir (str): Name of directory containing annotations label file
        gt_file (str): Name of the annotations label file
        dt_dir (str): Name of directory containing data or files
        bucket (str): Name of the bucket on MinIO
        img_mimetypes (List[str]): List of Mimetypes for image files
        doc_mimetypes (List[str]): List of Mimetypes for PDF files
        modules_config (HydraModules): Configurations for all modules
    """

    cache_dir: str
    model_dir: str
    root_dataset: str
    gt_dir: str
    gt_file: str
    dt_dir: str
    bucket: str
    img_mimetypes: List[str]
    doc_mimetypes: List[str]
    modules_config: HydraModules


class Datasets(Enum):
    """
    Enumeration to hold all available datasets:
        - "blinding": Data pertaining to blinding module
        - "tabular": Data pertaining to tabular module
        - "ocr": Data pertaining to ocr module
    """

    blinding = "blinding"
    tabular = "tabular"
    ocr = "ocr"


class MMocrModel(BaseModel):
    """
    MMocrModel class has configurations specific to the mmocr model.

    Attributes:
        recog_model (str): name of the recognition model being used.
        recog_model_weight (str): name of the recognition model weight file.
        recog_model_config (str): name of the recognition config python file.
        det_model (str): name of the detection model being used.
        det_model_weight (str): name of the detection model weight file.
        det_model_config (str): name of the detection config python file.
    """

    recog_model: str
    recog_model_weight: str
    recog_model_config: str
    det_model: str
    det_model_weight: str
    det_model_config: str


class CalamariocrModel(BaseModel):
    """
    CalamariocrModel class has configurations specific to the calamriocr model.

    Attributes:
        model_weight (str): name of the model file.
    """

    model_weight: str


class OCRModels(BaseModel):
    """
    OCRModels class that contains all configurations to be used for all ocr models.

    Attributes:

        mmocr (MMocrModel): Configurations for mmocr model.
        calamariocr (CalamariocrModel): Configurations for calamariocr model.
    """

    mmocr: MMocrModel
    calamariocr: CalamariocrModel
