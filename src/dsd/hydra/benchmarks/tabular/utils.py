"""Utility file for the pipleine


@authors : Johan Fernandes, Niranjan Ramesh
"""

import ocrmypdf
import mmcv
import yaml
import os
from os.path import join, dirname, realpath
from pathlib import Path
from tqdm.auto import tqdm
from typing import Tuple


from dsd.hydra.benchmarks.tabular.models.tabular_schemas import (
    TabularAlgorithm,
    TabularAlgorithmComponents,
)
from dsd.hydra.benchmarks.config.api import S3Location
from dsd.hydra.benchmarks.modules.s3client import S3Client


def read_yaml(file_path: str) -> dict:
    """Function to load a yaml file

    Arguments:
        - file_path (str): Path to yaml folder

    Returns:
        - (dict) : dictionary of loaded yaml file
    """

    return yaml.load(open(file_path), yaml.Loader)


def get_img(img_path: str) -> Tuple:
    """
    Get the image from specified location

    Arguments:
        - img_path (str): String path of the image to convert to PDF

    Returns:
        - (Tuple) : Provides a tuple of image, height and width of image
    """
    img = mmcv.imread(img_path)
    img_height, img_width, _ = img.shape
    return (img, img_height, img_width)


def generate_pdf(img_path: str, pdf_path: str) -> None:
    """Generate a pdf of an image

    Arguments:
        - img_path (str): String path of the image to convert to PDF
        - pdf_path (str): String path of the temporary pdf

    Returns:
        - None
    """
    ocrmypdf.ocr(img_path, pdf_path, image_dpi=200)


def download_model_data(
    bucket: str,
    local_data_dir: Path,
    model_dir_prefix: Path,
    model_weight_file: str,
) -> bool:
    """Download the moedl and config from MinIO to local folder

    Arguments:
        - local_data_dir (Path) : Local data directory to save the model
        - model_dir_prefix (Path): Path of model config and weight on
                                   Local and Minio Folder
        - model_weight_file (str): Name of the model weight file

    Returns:
        - (bool) Status of model data download.
                True for everything went well and local folder
                created False for everything did not get well

    """

    try:
        client = S3Client.get_client()

        # Generate path for MinIO

        model_w_minio_loc = S3Location(
            bucket=bucket, prefix=model_dir_prefix, basename=model_weight_file
        )

        # If model config exists only then download it
        # models/tabular/v0.1.0/cascadetabnet/cascadetabnet.py
        if client.stat_loc(model_w_minio_loc):

            minio_file_list = client.ls_objs(
                bucket=bucket, prefix=model_dir_prefix, recursive=True
            )

            # Create a local directory to save the model data
            local_model_dir = Path(join(local_data_dir, model_dir_prefix))

            # Since it exists on Minio. Create local folder
            Path.mkdir(local_model_dir, parents=True, exist_ok=True)

            for file_det in tqdm(minio_file_list, desc="Downloading files"):
                filename = os.path.relpath(Path(file_det.object_name), model_dir_prefix)
                minio_location = S3Location(
                    bucket=bucket, prefix=model_dir_prefix, basename=filename
                )
                client.fget_loc(minio_location, join(local_model_dir, filename))

        else:
            print("Algorithm doesn't exist")
            return False

    except Exception as e:
        print(e)
        return False

    return True


def get_algorithm(cfg_file_path: str, algorithm_name: str) -> TabularAlgorithm:
    """Get the algorithm to be used in the pipeline

    Arguments:
        - cfg_file_path (str) : Path of algorithms.yml
        - algorithm_name : Name of the algorithm to be used

    Returns:
        - TabularAlgorithm : Will contain component names such
                             as tabledet_model, extraction
                             and layout process

    Example:
        get_algorithm("algorithm_components/algorithms.yml,
                      "cascadetabnet_ocrmypdf")
        will provide : TabularAlgorithm(algorithm_name = "cascadetabnet_ocrmypdf",
                                        TabularAlgorithmComponents(
                                            tabledet_model : "cascadetabnet",
                                            tabledet_model_weight : "cascadetabnet.pth"
                                            extraction : "ocrmypdf",
                                            layout : "slice"
                                        ))
    """

    algorithm_directory = dirname(dirname(realpath(__file__)))
    algorithm_dict = yaml.load(
        open(join(algorithm_directory, cfg_file_path)), yaml.Loader
    )
    algorithm_list = [key for key in algorithm_dict.keys()]

    algorithm = None
    if algorithm_name is not None and algorithm_name in algorithm_list:
        algorithm_components = algorithm_dict[algorithm_name]
        algorithm = TabularAlgorithm(
            algorithm_name=algorithm_name,
            components=TabularAlgorithmComponents(**algorithm_components),
        )

    return algorithm
