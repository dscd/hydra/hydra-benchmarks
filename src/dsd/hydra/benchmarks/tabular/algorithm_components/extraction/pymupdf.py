import numpy as np
import cv2
import fitz
from fitz.fitz import Document
import io
from typing import Tuple

from dsd.hydra.benchmarks.tabular.algorithm_components.extraction.utils import (
    get_img_scale,
    rescale_document_content,
)
from dsd.hydra.benchmarks.tabular.models.tabular_schemas import (
    ExtractedContent,
    DocPage,
)
from dsd.hydra.benchmarks.tabular.algorithm_components.extraction.init import (
    ImageExtract,
)


class PMPDF:
    """
    Returns utility class to extract text and word bounding boxes in a PDF document

    Arguments:
        doc_filename (str) : Name of the document to extract text.
        document (fitz.fitz.Document) : Fitz document object if exists

    Returns
        PMPDF: Utility class to extract PDF content of a page
    """

    def __init__(self, doc_filename: str = None, document: fitz.fitz.Document = None):
        self.filename = doc_filename
        self.doc = document
        if self.doc is None:
            if self.filename is None:
                raise Exception("Either arguemnts have be non null")
            self.doc = fitz.open(self.filename)

    def get_docpage(self, page_number) -> DocPage:
        """
        Returns the image of an entire PDF page

        Arguments:
            page_number (int) : Page number to extract image of the PDF

        Returns
            (DocPage) : Image of the specific page of PDF as a numpy array along with
            metadata

        """
        page = self.doc.load_page(page_number)
        pix = page.get_pixmap()
        img = np.frombuffer(pix.samples, dtype=np.uint8).reshape(pix.h, pix.w, pix.n)

        return DocPage(
            img=img, filename=self.filename, dim=(pix.h, pix.w), page=page.number
        )

    def get_page_content(self, page_number: int) -> ExtractedContent:
        """Returns the page words, lines and whole page as an image

        Arguments:
            pdf_path (str): Path of the default pdf that is created by OCRmyPDf

        Returns
            (ExtractedContent): Tuple of page as image, page lines and page words
                                page lines : list[x1, y1, x2, y2] in the page
                                page words : list[x1, y1, x2, y2, words] in the page
        """

        page = self.doc.load_page(page_number)
        # Store blocks of words as a dict. This contains block of words + locations
        blocks = page.get_text("dict", sort=True)
        # Store the words on a page. This contains words + locations
        page_words = np.array(page.get_text("words", sort=True))
        # Store the page word locations
        page_word_bboxes = page_words[:, :4]
        # Store the page word text
        page_word_text = page_words[:, 4]

        # Go through the block list to pick lines from each block
        # Add the lines to page_lines list
        page_lines = []
        for block in blocks["blocks"]:
            if "lines" in block:
                for line in block["lines"]:
                    page_lines.append(line["bbox"])

        # Extract the page in the pdf as an image.
        # To be used to estimate the size of the page after converting image to pdf
        page_img = page.get_pixmap()

        document_content = ExtractedContent(
            image=page_img,
            lines_bboxes=page_lines,
            word_bboxes=page_word_bboxes.tolist(),
            word_text=page_word_text.tolist(),
        )

        return document_content


class PmpImage(ImageExtract):
    """
    Returns utility class for Image to PDF text OCR using Pymupdf
    """

    def extract_img_text(self, image: np.array) -> Document:
        """Returns a fitz.DOcument object from a table image.
        Table image will be in np.ndarray format

        Arguments:
            image (np.ndarray) : table image with table content
            path (str)

        Returns:
            (Document) : Fitz Document type
        """

        # Convert image as .jpg into byte array
        is_success, buffer_array = cv2.imencode(".jpg", image)
        # Create a byte array buffer to be used for PixMap creation
        buffer = io.BytesIO(buffer_array)

        # generate a pixmap with the buffer created earlier
        pixmap = fitz.Pixmap(buffer.getvalue())

        # 1-page PDF with the OCRed image
        # This function seems to be saving the bytes in PDF
        # format in the memory with file name "pdf"
        pdfbytes = pixmap.pdfocr_tobytes(language="eng")

        # read it from the memory with file name pdf
        document = fitz.open("pdf", pdfbytes)

        # Clear the buffer for future use
        buffer.flush()

        # Returns a fitz.Document.
        # Note: Don't be alarmed if result is <Document : '', <memory, doc# 2>>
        # check if you can load a page and read the words
        return document

    def extract_text(
        self,
        image: np.array,
        dims: Tuple[int, int],
        table_bbox: Tuple[float, float, float, float] = None,
    ) -> ExtractedContent:
        """
        Returns ExtractedContent - document content by OCR of the image of table region

        Arguments:
            image (np.ndarray) : Image to perform OCR as a numpy array
            dims (Tuple[int, int]) : Tuple containing the height and width of the image
            table_bbox (Tuple[float, float, float, float]) : Coordinates of table region

        Returns
            (ExtractedContent): Words and word boxes of the converted PDF along with
                the page image

        """
        # Only using numpy array from here on for an image
        if type(image) != np.array:
            image = np.array(image)

        # Perform OCR on image using PyMuPDF OCR tool
        document = self.extract_img_text(image)
        # Use PDF pipeline's PMPDF to get words, blocks and lines
        # from the new document creaed above
        pmpdf = PMPDF(document=document)
        document_content = pmpdf.get_page_content(0)

        table_img_height, table_img_width = dims
        table_img_scales = get_img_scale(
            document_content.image, table_img_width, table_img_height
        )
        # rescale the document content to orginal image size from
        # PyMuPDF image size
        rescaled_document_content = rescale_document_content(
            document_content=document_content,
            img_scale=table_img_scales,
            org_img_xmin=table_bbox[0],
            org_img_ymin=table_bbox[1],
        )
        return rescaled_document_content
