#!/usr/bin/env python
# -*- coding: utf-8 -*-
# -----------------------------------------------------------
# Hydra Benchmarks: flair_recognizer.py
#
# (C) 2022 Statistics Canada
# Author: Saptarshi Dutta Gupta
# ---------------


from typing import List, Optional, Set, Tuple

from dsd.hydra.benchmarks.blinding.blinding_config import (
    LanguageCode,
    Recognizer_Entities,
)
from dsd.hydra.benchmarks.config.blinding_algorithms import ModelNames
from flair.data import Sentence, Span
from flair.models import SequenceTagger
from presidio_analyzer import AnalysisExplanation, EntityRecognizer, RecognizerResult
from presidio_analyzer.nlp_engine import NlpArtifacts


class FlairRecognizer(EntityRecognizer):
    """
    Wrapper for a flair model, if needed to be used within Presidio Analyzer.

    :example:
    >from presidio_analyzer import AnalyzerEngine, RecognizerRegistry

    >flair_recognizer = FlairRecognizer()

    >registry = RecognizerRegistry()
    >registry.add_recognizer(flair_recognizer)

    >analyzer = AnalyzerEngine(registry=registry)

    >results = analyzer.analyze(
    >    "My name is Christopher and I live in Irbid.",
    >    language="en",
    >    return_decision_process=True,
    >)
    >for result in results:
    >    print(result)
    >    print(result.analysis_explanation)

    """

    ENTITIES = [
        Recognizer_Entities.location.value,
        Recognizer_Entities.person.value,
        Recognizer_Entities.organization.value,
    ]

    DEFAULT_EXPLANATION = "Identified as {} by Flair's NER"

    CHECK_LABEL_GROUPS = [
        (
            {Recognizer_Entities.location.value},
            {Recognizer_Entities.loc.value, Recognizer_Entities.location.value},
        ),
        (
            {Recognizer_Entities.person.value},
            {Recognizer_Entities.per.value, Recognizer_Entities.person.value},
        ),
        ({Recognizer_Entities.organization.value}, {Recognizer_Entities.org.value}),
    ]

    MODEL_LANGUAGES = {
        LanguageCode.english.value: ModelNames.flair_en.value,
        LanguageCode.french.value: ModelNames.flair_fr.value,
    }

    PRESIDIO_EQUIVALENCES = {
        Recognizer_Entities.per.value: Recognizer_Entities.person.value,
        Recognizer_Entities.loc.value: Recognizer_Entities.location.value,
        Recognizer_Entities.org.value: Recognizer_Entities.organization.value,
    }

    def __init__(
        self,
        supported_language: str = LanguageCode.english.value,
        supported_entities: Optional[List[str]] = None,
        check_label_groups: Optional[Tuple[Set, Set]] = None,
        model: SequenceTagger = None,
    ):
        """
        Function to initialize object's data variables
        Argument(s):
            - supported_language(str): supported language
            - supported_entities (List[str]): List of supported entities
                                            This is an optional parameter
            - check_label_groups (Tuple[Set, Set]): entity, label groups
            - model (SequenceTagger): Model name
        Returns:
            - None
        """
        self.check_label_groups = (
            check_label_groups if check_label_groups else self.CHECK_LABEL_GROUPS
        )

        if supported_entities:
            supported_entities = supported_entities
        else:
            supported_entities = self.ENTITIES

        self.model = (
            model
            if model
            else SequenceTagger.load(self.MODEL_LANGUAGES.get(supported_language))
        )

        super().__init__(
            supported_entities=supported_entities,
            supported_language=supported_language,
            name="Flair Analytics",
        )

    def load(self) -> None:
        """
        Load the model, not used. Model is loaded during initialization.
        Argument(s):
            - self object(FlairRecognizer): object to load
        Returns:
            - None
        """

    def get_supported_entities(self) -> List[str]:
        """
        Return supported entities by this model.
        Argument(s):
            - self object(FlairRecognizer): object whose entities are to be returned
        Returns:
            - List[str]
        """
        return self.supported_entities

    # Class to use Flair with Presidio as an external recognizer.
    def analyze(
        self, text: str, entities: List[str], nlp_artifacts: NlpArtifacts = None
    ) -> List[RecognizerResult]:

        """
        Analyze text using Text Analytics
        Argument(s):
            - text(str): The text for analysis
            - entities(List[str]): List of supported entities
            - nlp_artifacts(NlpArtifacts):
            presidio_analyzer.nlp_engine.nlp_artifacts.NlpArtifacts object

        Returns:
            - List[RecognizerResult]: The list of Presidio RecognizerResult
            constructed from the recognized Flair detections
        """
        results = []
        sentences = Sentence(text)
        self.model.predict(sentences)

        # If there are no specific list of entities, look for all of it.
        if not entities:
            entities = self.supported_entities

        for entity in entities:
            if entity not in self.supported_entities:
                continue

            for ent in sentences.get_spans("ner"):
                if not self.__check_label(
                    entity, ent.labels[0].value, self.check_label_groups
                ):
                    continue
                textual_explanation = self.DEFAULT_EXPLANATION.format(
                    ent.labels[0].value
                )
                explanation = self.build_flair_explanation(
                    round(ent.score, 2), textual_explanation
                )
                flair_result = self._convert_to_recognizer_result(ent, explanation)
                results.append(flair_result)

        return results

    def _convert_to_recognizer_result(
        self, entity: Span, explanation: str  # Noqa: F821
    ) -> RecognizerResult:
        """
        Generate recognizer results in RecognizerResult format
        Argument(s):
            - entity(flair.data.Span): annotated spans. Each such Span has a text,
            a tag value, its position in the sentence and "score" that indicates
            how confident the tagger is that the prediction is correct
            - explanation(str): Explanation string

        Returns:
            - transformers_results(RecognizerResult)
        """
        entity_type = self.PRESIDIO_EQUIVALENCES.get(entity.tag, entity.tag)
        flair_score = round(entity.score, 2)

        flair_results = RecognizerResult(
            entity_type=entity_type,
            start=entity.start_position,
            end=entity.end_position,
            score=flair_score,
            analysis_explanation=explanation,
        )

        return flair_results

    def build_flair_explanation(
        self, original_score: float, explanation: str
    ) -> AnalysisExplanation:

        """
        Function to create explanation for why this result was detected
        Argument(s):
            - original_score(float): Score given by this recognizer
            - explanation(str): Explanation string

        Returns:
            - explanation(AnalysisExplanation)
        """
        explanation = AnalysisExplanation(
            recognizer=self.__class__.__name__,
            original_score=original_score,
            textual_explanation=explanation,
        )
        return explanation

    @staticmethod
    def __check_label(
        entity: str, label: str, check_label_groups: Tuple[Set, Set]
    ) -> bool:
        """
        check if given entity and label belong to an entity, label group
        Argument(s):
            - entity(str): entity name to check
            - label(str): label name to check
            - check_label_groups (Tuple[Set, Set]): entity, label groups

        Returns:
            - bool
        """
        return any(
            [entity in egrp and label in lgrp for egrp, lgrp in check_label_groups]
        )
