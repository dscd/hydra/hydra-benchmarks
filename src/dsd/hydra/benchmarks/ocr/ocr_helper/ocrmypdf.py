import os
from typing import List

import fitz
from dsd.hydra.benchmarks.models.schemas import RegionInfo


class OCRmyPDFHelper:
    """
    This a class built on top of the OCRmyPDF for ease of use.
    It parses the image to the Tesseract engine and returns the OCR output.
    """

    def __init__(self, deskew: str = "True", image_dpi: int = 96):
        """
        This method is used to initialize the OCRmyPDF with the custom configuration.
        In case a configuration is not selected, the default for oem and psm is used.

        Arguments:
            - None

        Returns:
            - None
        """
        self.custom_config = [deskew, image_dpi]

    def get_regions(self, img) -> List:
        """
        This method takes a Pillow image as input and returns a list of Region objects.

        Arguments:
            - img (Image.Image): A Pillow image of what is to be extracted

        Returns:
            - (List): List of Region objects
        """
        # ocrmypdf's function to get image is called
        out = os.path.join(os.path.dirname(__file__), "output.pdf")

        os.system("ocrmypdf --deskew --image-dpi 96 " + img.filename + " " + out)

        # defining an empty list to store all regions
        # each block of text detected by the ocr is a region
        regions = []
        pdf_text = []

        # after that the coodrinates are setup to have them in the form of (x1,y1,x2,y2)
        # later
        with fitz.open(out) as document:
            for page_number, page in enumerate(document):
                pdf_text.extend(page.get_text("words"))

        # for each region, an RegionInfo model is created and added to the regions
        # list
        for val in pdf_text:
            coords = tuple([round(val[0]), round(val[1]), round(val[2]), round(val[3])])
            text = val[4]
            image_region = RegionInfo(coordinates=coords, text=text)
            regions.append(image_region)

        os.remove(out)
        return regions
