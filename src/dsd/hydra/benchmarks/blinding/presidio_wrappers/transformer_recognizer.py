#!/usr/bin/env python
# -*- coding: utf-8 -*-
# -----------------------------------------------------------
# Hydra Benchmarks: transformer_recognizer.py
#
# (C) 2022 Statistics Canada
# Author: Saptarshi Dutta Gupta
# ---------------


import logging
from typing import Dict, List, Optional, Set, Tuple

from dsd.hydra.benchmarks.blinding.blinding_config import (
    Recognizer_Entities,
)
from dsd.hydra.benchmarks.config.blinding_algorithms import ModelNames
from presidio_analyzer import AnalysisExplanation  # noqa: E501
from presidio_analyzer import EntityRecognizer, RecognizerResult
from presidio_analyzer.nlp_engine import NlpArtifacts

logger = logging.getLogger("presidio-analyzer")

try:
    from transformers import AutoModelForTokenClassification, AutoTokenizer, pipeline
    from transformers.models.bert.modeling_bert import (
        BertForTokenClassification,
    )  # noqa: E501
except ImportError:
    logger.error("transformers is not installed")


class TransformersRecognizer(EntityRecognizer):
    """
    Wrapper for a transformers model, if needed to be used within
    Presidio Analyzer.   # noqa: E501
    :example:
    >from presidio_analyzer import AnalyzerEngine, RecognizerRegistry
    >transformers_recognizer = TransformersRecognizer()
    >registry = RecognizerRegistry()
    >registry.add_recognizer(transformers_recognizer)
    >analyzer = AnalyzerEngine(registry=registry)
    >results = analyzer.analyze(
    >    "My name is Christopher and I live in Irbid.",
    >    language="en",
    >    return_decision_process=True,
    >)
    >for result in results:
    >    print(result)
    >    print(result.analysis_explanation)
    """

    ENTITIES = [
        Recognizer_Entities.location.value,
        Recognizer_Entities.person.value,
        Recognizer_Entities.organization.value,
    ]

    DEFAULT_EXPLANATION = (
        "Identified as {} by transformers's Named Entity Recognition"  # noqa: E501
    )

    CHECK_LABEL_GROUPS = [
        ({Recognizer_Entities.location.value}, {Recognizer_Entities.loc.value}),
        ({Recognizer_Entities.person.value}, {Recognizer_Entities.per.value}),
        ({Recognizer_Entities.organization.value}, {Recognizer_Entities.org.value}),
    ]

    PRESIDIO_EQUIVALENCES = {
        Recognizer_Entities.per.value: Recognizer_Entities.person.value,
        Recognizer_Entities.loc.value: Recognizer_Entities.location.value,
        Recognizer_Entities.org.value: Recognizer_Entities.organization.value,
    }

    DEFAULT_MODEL = ModelNames.bert.value

    def __init__(
        self,
        supported_entities: Optional[List[str]] = None,
        check_label_groups: Optional[Tuple[Set, Set]] = None,
        model: Optional[BertForTokenClassification] = None,
    ):

        if model is None:
            model = self.DEFAULT_MODEL

        self.check_label_groups = (
            check_label_groups if check_label_groups else self.CHECK_LABEL_GROUPS
        )

        supported_entities = (
            supported_entities if supported_entities else self.ENTITIES
        )  # noqa: E501

        self.model = pipeline(
            "ner",
            model=AutoModelForTokenClassification.from_pretrained(model),  # noqa: E501
            tokenizer=AutoTokenizer.from_pretrained(model),
            aggregation_strategy="average",
        )

        super().__init__(
            supported_entities=supported_entities,
            name="Transformers Analytics",
        )

    def load(self) -> None:
        """
        Load the model, not used. Model is loaded during initialization.
        Argument(s):
            - self object(TransformersRecognizer): object to load
        Returns:
            - None
        """

    def get_supported_entities(self) -> List[str]:
        """
        Return supported entities by this model.
        Argument(s):
            - self object(TransformersRecognizer): object whose entities are
            to be returned
        Returns:
            - List[str]
        """
        return self.supported_entities

    # Class to use transformers with Presidio as an external recognizer.
    def analyze(
        self, text: str, entities: List[str], nlp_artifacts: NlpArtifacts = None
    ) -> List[RecognizerResult]:
        """
        Analyze text using Text Analytics
        Argument(s):
            - text(str): The text for analysis
            - entities(List[str]): List of supported entities
            - nlp_artifacts(NlpArtifacts):
            presidio_analyzer.nlp_engine.nlp_artifacts.NlpArtifacts object

        Returns:
            - List[RecognizerResult]
        """

        results = []
        try:
            ner_results = self.model(text)
        except:  # noqa: E
            return logger.error("Error in getting model results")

        # If there are no specific list of entities, we will look for all of it.    # noqa: E501
        if not entities:
            entities = self.supported_entities

        if not ner_results:
            return None

        for entity in entities:
            if entity not in self.supported_entities:
                continue

            for res in ner_results:
                if not self.__check_label(
                    entity, res["entity_group"], self.check_label_groups
                ):
                    continue
                textual_explanation = self.DEFAULT_EXPLANATION.format(
                    res["entity_group"]
                )
                explanation = self.build_transformers_explanation(
                    round(res["score"], 2), textual_explanation
                )
                transformers_result = self._convert_to_recognizer_result(
                    res, explanation
                )

                results.append(transformers_result)

        return results

    def _convert_to_recognizer_result(
        self, res: Dict, explanation: str
    ) -> RecognizerResult:
        """
        Generate recognizer results in RecognizerResult format
        Argument(s):
            - res(Dict): NER model result
            - explanation(str): Explanation string

        Returns:
            - transformers_results(RecognizerResult)
        """

        entity_type = self.PRESIDIO_EQUIVALENCES.get(
            res["entity_group"], res["entity_group"]
        )
        transformers_score = round(res["score"], 2)

        transformers_results = RecognizerResult(
            entity_type=entity_type,
            start=res["start"],
            end=res["end"],
            score=transformers_score,
            analysis_explanation=explanation,
        )

        return transformers_results

    def build_transformers_explanation(
        self, original_score: float, explanation: str
    ) -> AnalysisExplanation:
        """
        Create explanation for why this result was detected.
        Argument(s):
            - original_score(float): Score given by this recognizer
            - explanation(str): supported language

        Returns:
            - explanation(AnalysisExplanation)
        """
        explanation = AnalysisExplanation(
            recognizer=self.__class__.__name__,
            original_score=original_score,
            textual_explanation=explanation,
        )
        return explanation

    @staticmethod
    def __check_label(
        entity: str, label: str, check_label_groups: Tuple[Set, Set]
    ) -> bool:
        """
        check if given entity and label belong to an entity, label group
        Argument(s):
            - entity(str): entity name to check
            - label(str): label name to check
            - check_label_groups (Tuple[Set, Set]): entity, label groups

        Returns:
            - bool
        """
        return any(
            [
                entity in egrp and label in lgrp for egrp, lgrp in check_label_groups
            ]  # noqa: E501
        )
