import logging
import os
from pathlib import Path

import dsd.hydra.benchmarks.ocr.ocr_helper.calamari as calamari
import dsd.hydra.benchmarks.ocr.ocr_helper.eocr as eocr
import dsd.hydra.benchmarks.ocr.ocr_helper.mmocr as mmocr
import dsd.hydra.benchmarks.ocr.ocr_helper.ocrmypdf as ocrmypdfHelper
import dsd.hydra.benchmarks.ocr.ocr_helper.paddleocr as paddleocr
import dsd.hydra.benchmarks.ocr.ocr_helper.tess as tess
import typer
from dsd.hydra.benchmarks.config.api import S3Location, load_config
from dsd.hydra.benchmarks.models.schemas import Datasets, OCREngines
from dsd.hydra.benchmarks.modules.s3client import S3Client
from tqdm.auto import tqdm

logger = logging.getLogger(__name__)


def get_ocr_helper(algorithm):
    """
    This function is used to select ocr helper class according to the parameter's value
    to be used during the run command for performing ocr on the input image/images.

    Arguments:
            - algorithm(string): string parameter which holds the name of ocr algorithm
              to be used.

    Returns:
            - (OCR helper class): According to the argument's value, appropriate ocr
              helper class is returned. If no match is made then None is returned
    """

    model_local_path = load_config().cache_dir

    if algorithm == OCREngines.tesseract.value:
        return tess.TessHelper()

    elif algorithm == OCREngines.easyocr.value:
        return eocr.EocrHelper()

    elif algorithm == OCREngines.paddleocr.value:
        return paddleocr.PaddleocrHelper()

    elif algorithm == OCREngines.mmocr.value:
        download_model = Download(Datasets.ocr.value, algorithm, model_local_path)
        download_model.create_local_directory_structure()
        download_model.download_files()
        return mmocr.MMocrHelper()

    elif algorithm == OCREngines.ocrmypdf.value:
        return ocrmypdfHelper.OCRmyPDFHelper()

    elif algorithm == OCREngines.calamariocr.value:
        download_model = Download(Datasets.ocr.value, algorithm, model_local_path)
        download_model.create_local_directory_structure()
        download_model.download_files()
        return calamari.CalamariHelper()

    else:
        return None


class Download:
    """
    This class is used to download the necesary models files for ocr engines
    which require them for the run function. This class is made use of in the
    function: get_ocr_helper
    """

    def __init__(self, dataset: str, algorithm: str, model_local_path: str) -> None:
        """
        This function is used to iniate the Download class object with necessary
        configuration details such as the dataset name, algorithm name and
        the local path to save the model files.

        Arguments:
                - dataset(string): string parameter which holds the name of dataset
                to be used. In this scenario it will be ocr.
                - algorithm(string): string parameter which holds the name of ocr
                algorithm to be used.
                - model_local_path(string):

        Returns:
                - None
        """
        self.config = load_config()
        self.dataset = dataset
        self.algorithm = algorithm
        self.model_local_path = model_local_path
        self.bucket = self.config.bucket
        self.prefix = os.path.join(
            self.config.model_dir,
            Datasets.ocr.value,
            self.config.modules_config.ocr.version,
        )
        # Instantiate MinIO Client
        self.client = S3Client.get_client()

    def create_local_directory_structure(self) -> None:
        """
        This method is used to create the local directory structure

        Arguments:
                - None

        Returns:
                - None
        """
        modified_prefix = os.path.join(self.prefix)
        objects = self.client.list_objects(
            self.bucket, prefix=modified_prefix, recursive=False
        )
        try:
            next(objects)
        except StopIteration:
            logger.error(
                f"Path not found: Given path {modified_prefix} does not\
exist. Aborting directory creation."
            )
            raise typer.Abort()
        logger.info("Creating directories")
        directory = list(
            self.client.ls_objs(
                bucket=self.bucket, prefix=modified_prefix, recursive=False
            )
        ).pop()

        local_path = Path(os.path.join(self.model_local_path, directory.object_name))
        local_path.mkdir(parents=True, exist_ok=True)

    def download_files(self) -> None:
        """
        This method is used to download all the files from MinIO.

        Arguments:
                - None

        Returns:
                - None
        """
        # Setup subset prefix minio location
        modified_prefix = os.path.join(self.prefix, self.algorithm, "")
        # Download files from subset to local directory
        for file in tqdm(
            self.client.ls_objs(
                bucket=self.bucket,
                prefix=modified_prefix,
                recursive=True,
                only_files=True,
                default_folder_file=False,
            ),
            desc="Subset file extraction",
            initial=1,
        ):

            local_path = os.path.join(self.model_local_path, file.object_name)
            basename = os.path.relpath(Path(file.object_name), modified_prefix)
            file_minio_location = S3Location(self.bucket, modified_prefix, basename)
            logger.info(f"\nDownloading from MinIO path: {file_minio_location}")
            logger.info(f"\nDownloading to local path: {local_path}")
            self.client.fget_loc(file_minio_location, local_path)
