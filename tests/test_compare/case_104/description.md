Description:
The objective of this specific unit test is to ensure that the function analyze_prediction in main.py performs accurately when called upon to process a typical file input via the OCR module.
The Ground Truth File contains 1 file where region is modified to be empty whereas for prediction there is one file where region is present. The expected output should be 1 file where all values in metrics should be 0 except FP and CER.CER should be 1.0. Also, FP will be based on the number of regions present on the prediction file.
