#!/usr/bin/env python
# -*- coding: utf-8 -*-
# -----------------------------------------------------------
# Hydra Benchmarks: utils.py
#
# (C) 2022 Statistics Canada
# Author: Saptarshi Dutta Gupta
# ---------------


import logging
from typing import Optional

from dsd.hydra.benchmarks.blinding.blinding_config import (
    CustomRecognizers,
    Entities,
)
from dsd.hydra.benchmarks.blinding.custom_recognizers.age_recognizer import (
    AgeRecognizer,
)
from dsd.hydra.benchmarks.blinding.custom_recognizers.gender_recognizer import (
    GenderRecognizer,
)
from dsd.hydra.benchmarks.blinding.custom_recognizers.marital_status_recognizer import (
    MaritalStatusRecognizer,
)
from dsd.hydra.benchmarks.blinding.custom_recognizers.phone_recognizer import (
    PhoneRecognizer,
)
from dsd.hydra.benchmarks.blinding.custom_recognizers.pri_recognizer import (
    PRIRecognizer,
)

from presidio_analyzer import EntityRecognizer, RecognizerRegistry


logger = logging.getLogger(__name__)


def get_registry_with_custom_recognizers(
    recognizer: Optional[EntityRecognizer] = None,
) -> RecognizerRegistry:
    """
    get registry with all custom recognizers
    Argument(s):
        - recognizer (EntityRecognizer): FlairRecognizer() or
        TransformerRecognizer() object

    Returns:
        - registry (RecognizerRegistry): Presidio registry object
        with all recognizers
    """

    # create an empty Presidio registry object
    registry = RecognizerRegistry()

    # load all the predefined recognizers from Presidio
    registry.load_predefined_recognizers()

    # if a custom recognizer object is passed, add it to the registry
    if recognizer:
        registry.add_recognizer(recognizer)

    # define a dictionary of custom recognizers
    custom_recognizers = {
        CustomRecognizers.pri_recognizer.value: PRIRecognizer(
            supported_entities=[Entities.pri.value]
        ),
        CustomRecognizers.age_recognizer.value: AgeRecognizer(),
        CustomRecognizers.marital_status_recognizer.value: MaritalStatusRecognizer(),
        CustomRecognizers.gender_recognizer.value: GenderRecognizer(),
        CustomRecognizers.phone_recognizer.value: PhoneRecognizer(),
    }

    # loop through the custom recognizers dictionary
    # and add each recognizer to the registry
    for recognizer_name, recognizer_obj in custom_recognizers.items():
        registry.add_recognizer(recognizer_obj)

    # remove the default date recognizer from the registry
    registry.remove_recognizer(CustomRecognizers.date_recognizer.value)

    # return the updated registry object
    return registry
