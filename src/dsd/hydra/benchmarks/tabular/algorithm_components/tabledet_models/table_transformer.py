"""Table Transformer Net Wrapper class to hold the TableTransformer
   Table Detection model implemented through HuggingFace API


@authors : Niranjan Ramesh, Johan Fernandes
"""

from typing import List
from transformers import TableTransformerForObjectDetection, DetrImageProcessor

from dsd.hydra.benchmarks.models.schemas import PageInfo, RegionInfo, TabularConfig
from dsd.hydra.benchmarks.tabular.models.tabular_schemas import BatchData
from dsd.hydra.benchmarks.tabular.algorithm_components.tabledet_models.init import (
    TableDet,
)


class TableTransformer(TableDet):
    """
    Returns wrapper class for TableTransformer model

    Arguments:
        model_dir (str): Path to the directory containing config files and weights
        device (str): Device to run the model on (cpu / cuda)

    Returns:
        TableTranformer: Wrapper class for TableTransformer model using HuggingFace
    """

    def __init__(self, model_dir: str, config: TabularConfig) -> None:
        self.model = TableTransformerForObjectDetection.from_pretrained(model_dir)
        self.pre_processor = DetrImageProcessor()
        self.iou_thr = config.iou_thr

    def get_result(
        self,
        batch: BatchData,
    ) -> List[PageInfo]:
        """Returns list of PageInfo object with the table locations of each image in the batch

        Arguments:
            batch (BatchData): List of
                - images : List
                - filenames : List
                - dims : List[(height, width)]
                - pages : List
                Each list is generated from a list of DocPage

        Returns:
            List[PageInfo] : List of RegionInfo models
        """

        encodings = self.pre_processor(batch.images, return_tensors="pt")
        output = self.model(**encodings)
        results = self.pre_processor.post_process_object_detection(
            output,
            threshold=self.iou_thr,
            # target_sizes=[x.shape[:2] for x in [i1, i2]]
            # target_sizes=target_sizes,
            target_sizes=batch.dims,
        )

        pages: List[PageInfo] = []

        for idx, result in enumerate(results):
            # result --> A dictionary consisting of keys:
            #     result["boxes"] --> All table class detections in bbox format
            #     result["scores"] --> Confidence scores for detectex bboxes
            # Results are sorted based on y1 values to show the tables from top to
            # bottom of the page
            tables = sorted(
                zip(result["boxes"], result["scores"]), key=lambda x: x[0][1]
            )

            regions = list(
                map(
                    lambda table: RegionInfo(
                        coordinates=[
                            float(table[0][0]),
                            float(table[0][1]),
                            float(table[0][2]),
                            float(table[0][3]),
                        ],
                        confidence=float(table[1]),
                    ),
                    tables,
                )
            )

            page = PageInfo(
                # Pick the page number from the collated batch
                page=batch.pages[idx],
                # List of regions where each region is a detected table
                regions=regions,
            )

            pages.append(page)

        return pages
