from typing import List

import numpy as np
from dsd.hydra.benchmarks.models.schemas import RegionInfo

from paddleocr import PaddleOCR


class PaddleocrHelper:
    """
    This a class built on top of the PaddlePaddle OCR system for ease of use.
    It parses the image to the PaddleOCR engine and returns the OCR output.
    """

    def __init__(self):
        """
        This method is used to initialize the Paddle OCR with the custom configuration.
        In case a configuration is not selected, the default is used.

        Arguments:
            - None

        Returns:
            - None
        """

        self.ocr = PaddleOCR(use_angle_cls=True, lang="en")

    def get_regions(self, img) -> List:
        """
        This method takes a Pillow image as input and returns a list of Region objects.

        Arguments:
            - img (Image.Image): A Pillow image of what is to be extracted

        Returns:
            - (List): List of Region objects
        """

        # PaddleOCR's function to get both image and data is called and saved as
        # heterogenous list.
        # The details consist of position, confidence and text.
        ocr_output = self.ocr.ocr(np.array(img), cls=True)

        # defining an empty list to store all regions
        # each block of text detected by the ocr is a region
        regions = []

        # for each region, an RegionInfo model is created and added to the regions
        # list.
        # the coordinates in the OCR output are in the form [(x1,y1),(x2,y1),
        #                                                    (x2,y2),(x1,y2)]
        # which are converted to the standard form of (x1,y1,x2,y2).
        for val in ocr_output[0]:
            x1 = min(val[0][0][0], val[0][1][0], val[0][2][0], val[0][3][0])
            x2 = max(val[0][0][0], val[0][1][0], val[0][2][0], val[0][3][0])
            y1 = min(val[0][0][1], val[0][1][1], val[0][2][1], val[0][3][1])
            y2 = max(val[0][0][1], val[0][1][1], val[0][2][1], val[0][3][1])
            coords = tuple([x1, y1, x2, y2])
            text = val[1][0]
            image_region = RegionInfo(coordinates=coords, text=text)
            regions.append(image_region)

        return regions
