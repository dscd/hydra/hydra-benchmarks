import os
from typing import List

import cv2
import numpy as np
from calamari_ocr.ocr.predictor import Predictor
from dsd.hydra.benchmarks.config.api import load_config, load_ocr_algorithms
from dsd.hydra.benchmarks.models.schemas import (
    Datasets,
    IntensityU8,
    OCREngines,
    RegionInfo,
)


class CalamariHelper:
    """
    This a class built on top of the OCRopy OCR for ease of use.
    It parses the image to the OCRopy and Kraken engine and
    it returns the OCR output.
    """

    def __init__(self):
        """
        This method is used to initialize the Calamari OCR
        with the custom configuration. In case a configuration is not selected,
        the default is used.

        Arguments:
            - None

        Returns:
            - None
        """
        self.config = load_config()
        self.ocr_config = self.config.modules_config.ocr
        self.model_dir = os.path.join(
            self.ocr_config.models_prefix,
            Datasets.ocr.value,
            self.ocr_config.models_version,
        )
        algorithm = OCREngines.calamariocr.value
        algorithms_config = load_ocr_algorithms()

        self.checkpoint = os.path.join(
            self.config.cache_dir,
            self.model_dir,
            algorithm,
            algorithms_config.calamariocr.model_weight,
        )
        self.predictor = Predictor(checkpoint=self.checkpoint)

    def get_regions(self, img) -> List:
        """
        This method takes a Pillow image as input and returns
        a list of Region objects.

        Arguments:
            - img (Image.Image): A Pillow image of what is to be extracted

        Returns:
            - (List): List of Region objects
        """

        # Read image from which text needs to be extracted
        img = cv2.imread(img.filename)

        # Preprocessing the image starts
        # Convert the image to gray scale
        gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

        # Performing OTSU threshold
        ret, thresh1 = cv2.threshold(
            gray,
            IntensityU8.dark.value,
            IntensityU8.bright.value,
            cv2.THRESH_OTSU | cv2.THRESH_BINARY_INV,
        )

        # Specify structure shape and kernel size.
        # Kernel size increases or decreases the area
        # of the rectangle to be detected.
        # A smaller value like (10, 10) will detect
        # each word instead of a sentence.
        rect_kernel = cv2.getStructuringElement(cv2.MORPH_RECT, (13, 13))

        # Applying dilation on the threshold image
        dilation = cv2.dilate(thresh1, rect_kernel, iterations=1)

        # Finding contours
        contours, hierarchy = cv2.findContours(
            dilation, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_NONE
        )

        # Creating a copy of image
        im2 = img.copy()
        # defining an empty list to store all regions
        # each block of text detected by the ocr is a region
        regions = []

        # Looping through the identified contours
        # Then rectangular part is cropped and passed on
        # to calamariocr for extracting text from it
        # Extracted text is then written into the model
        for cnt in contours:
            x, y, w, h = cv2.boundingRect(cnt)

            # Drawing a rectangle on copied image
            # Cropping the text block for giving input to OCR
            cropped = im2[y : y + h, x : x + w]
            image = np.array(cropped)
            res = list(self.predictor.predict_raw([image], progress_bar=False))
            coords = tuple([x, y, x + w, y + h])
            text = res[0].sentence
            image_region = RegionInfo(coordinates=coords, text=text)
            regions.append(image_region)

        return regions
