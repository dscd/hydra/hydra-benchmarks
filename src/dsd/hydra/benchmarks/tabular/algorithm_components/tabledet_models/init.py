from typing import List
from dsd.hydra.benchmarks.models.schemas import PageInfo, TabularConfig
from dsd.hydra.benchmarks.tabular.models.tabular_schemas import BatchData


class TableDet:
    """Returns wrapper class for table detection models
    Arguments:
        model_config (str): Path of config file path of model
        weights (str): Path of weights of the model
        device (str): device to run the model on

    Returns:
        TableDet: Wrapper for table detection models based on the class implementing.
    """

    def __init__(self, model_config: str, weights: str, config: TabularConfig) -> None:
        pass

    def get_result(
        self,
        batch: BatchData,
    ) -> List[PageInfo]:
        """Returns list of PageInfo object with the table locations of each image in the batch

        Arguments:
            batch (BatchData) : List of images of pages in a PDF or
                list of images if inputs are images

        Returns:
            List[PageInfo] : List of Pages if input is a PDF with the
                tables detected as regions in each page.
                List of Pages per image files if input
                is an image
        """
