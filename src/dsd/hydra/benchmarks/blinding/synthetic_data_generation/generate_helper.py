import copy
import itertools
import logging
import random
import re
from typing import Dict, List, Tuple

import fitz
import pandas as pd
import yaml
from dsd.hydra.benchmarks.blinding.blinding_config import (
    Email,
    FileTypes,
    Font,
    Labels,
    Synthetic_Database,
    Synthetic_Entities,
    Synthetic_Labels,
)
from dsd.hydra.benchmarks.blinding.synthetic_data_generation.utils import (
    bb_intersection_over_union,
    file_to_list,
    get_random_text,
    remove_punctuation_list,
    remove_punctution_text,
)

logger = logging.getLogger(__name__)


def assign_entities(
    target_matched: List[str], page: int, target_df: pd.DataFrame
) -> List[str]:
    """
    Function to assign entities to each target matched
    Argument(s):
        - target_matched(List[str]): List of targets
        - page(str): document page number
        - target_df(List[str]): page wise list of targets, entities and entity types

    Returns:
        - List[str] : List of entities
    """
    # filter the target dataframe to only include rows
    # where the 'page' column matches the given 'page' variable
    target_df = target_df[target_df[Labels.page.value] == page]

    # extract the list of targets from the filtered target dataframe
    targets = target_df[Labels.target.value].values.tolist()[0]

    # extract the entity type for each target from the filtered target dataframe
    entity_type = target_df[Labels.entity_type.value].values.tolist()[0]

    # create an empty list to store the matched entity types
    entity_list = []

    # iterate over each match in the 'target_matched' list
    for match in target_matched:

        # find the index of the current 'match' in the 'targets' list
        target_index = targets.index(match)

        # retrieve the corresponding entity type
        # from the 'entity_type' list using the 'target_index'
        entity = entity_type[target_index]

        # add the matched entity type to the 'entity_list'
        entity_list.append(entity)

    # return the list of matched entity types
    return entity_list


def match_found_percentage(
    target: str, text: str, match: bool, p_match: float, matched_target: List[str]
) -> pd.Series:
    """
    Function to find a target in the given text and calculate the match percentatge

    Argument(s):
        - target(str): target string to look for in the text
        - text(str): text to search the target string in
        - match(bool): indicates if a match has been found previously
        - p_match(float): previous matching percentage
        - matched_target(List[str]): the list of previously matched targets

    Returns:
        - pd.Series: a Pandas Series with True/False, indicating if a match was found,
            the matching percentage, and the target
    """
    # check if target is in text calculate the matching percentage
    if target in text:
        per = len(
            set(remove_punctuation_list(target.split(" "))).intersection(
                set(remove_punctuation_list(text.split(" ")))
            )
        ) / len(set(text.split(" ")))

        if match:  # check if a match has been found previously
            mod_target = matched_target + [target]  # add the current target to the list
            mod_per = p_match + [per]
            return pd.Series([match, mod_per, mod_target])  # return the Series
        return pd.Series(
            [True, [per], [target]]
        )  # return the Series indicating a match was found,
        #  with the matching percentage, and the target

    else:  # if target is not found in text
        if match:  # check if a match has been found previously
            return pd.Series([match, p_match, matched_target])  # return the Series
        return pd.Series(
            [False, 0, ""]
        )  # return the Series indicating no match was found,
        # with 0 matching percentage, and an empty string


def resolve_conflicts(text: str, p_match: float, target_match: List[str]) -> pd.Series:
    """
    Function to resolve conflicts when multiple targets are matched
    Argument(s):
        - text(str): text to search the target string in
        - p_match(float): matching percentage
        - matched_target(List[str]): the list of matched targets

    Returns:
        - pd.Series: a Pandas Series final matching percentage and target
    """
    # Check if there is only one match, in which case return the page and target match
    if len(target_match) == 1:
        return pd.Series([p_match, target_match])

    # If there are multiple matches, make a copy of the input lists
    p_update = copy.deepcopy(p_match)
    target_update = copy.deepcopy(target_match)

    # Create unique lists of page and target matches
    p_update_unique = [a for a, b in list(set(list(zip(p_update, target_update))))]
    target_update_unique = [b for a, b in list(set(list(zip(p_update, target_update))))]

    # Iterate over the unique page and target matches,
    # in decreasing order of page match score
    while p_update_unique:
        # Find the highest score index
        highest_idx = p_update_unique.index(max(p_update_unique))

        # If the target match isn't in the text, break out of the loop
        if target_update_unique[highest_idx] not in text:
            break

        # Otherwise, create a regex pattern to find the
        # exact target match in the text and replace it with an empty string
        exact_match_regex = r"\b" + re.escape(target_update_unique[highest_idx]) + r"\b"
        text = re.sub(exact_match_regex, "", text)

        # Remove the highest-scoring page and target match from the unique lists
        del p_update_unique[highest_idx]
        del target_update_unique[highest_idx]

    # Create a final list of target matches by subtracting
    # the unique target matches from the original list of target matches
    final_target = list(set(target_match) - set(target_update_unique))

    # Create a final list of page matches
    # corresponding to the final list of target matches
    final_p = [p_match[target_match.index(target)] for target in final_target]

    # Return the final lists of page and target matches
    return pd.Series([final_p, final_target])


def get_raw_df(raw: fitz.fitz.Document) -> pd.DataFrame:
    """
    Function reads a pdf document and adds text, coordinates of text,
    page number to a dataframe and initalizes other columns
    Argument(s):
        - raw(fitz.fitz.Document): pdf document being processed
    Returns:
        - pd.DataFrame: a Pandas DataFrame with text, coordinates of text,
                        page number information along with  three other columns
                        with default values
    """
    # Create an empty pandas DataFrame to store the extracted data
    df = pd.DataFrame()

    # Loop over each page in the input raw data
    for index, page in enumerate(raw):
        # Extract each block of text from the page
        for block in page.get_text("dict")["blocks"]:
            # Check if the block is of type 0 (i.e., regular text)
            if block["type"] == 0:
                # Extract each line of text from the block
                for lines in block["lines"]:
                    # Extract each span of text from the line
                    for span in lines["spans"]:
                        # Create a dictionary to store the extracted data for this span
                        df_content = {
                            Labels.text.value: span["text"],  # The text of the span
                            Labels.coordinates.value: tuple(
                                span["bbox"]
                            ),  # The bounding box coordinates of the span
                            # The index of the page that the span appears on
                            Labels.page.value: index,
                        }
                        content_df = pd.DataFrame.from_dict(
                            df_content, orient="index"
                        ).transpose()
                        # Concatenate the extracted data to the DataFrame
                        df = pd.concat([df, content_df], ignore_index=True)

    # Add new columns to the DataFrame to store the results of the matching process
    df[
        Labels.match_found.value
    ] = False  # Whether a target match was found for this span
    df[
        Labels.p_match_found.value
    ] = 0  # The page match score for the best matching page
    df[Labels.target_matched.value] = ""  # The target string that was matched, if any

    # Return the completed DataFrame
    return df


def get_match_found_df(raw_df, target_df) -> pd.DataFrame:
    """
    Function to compare target df with the raw document
    Argument(s):
        - raw_df(fitz.fitz.Document): pdf document being processed
        - target_df(pd.DataFrame): page wise list of targets, entities and entity types
    Returns:
        - pd.DataFrame: a Pandas DataFrame with information about targets,
        their coordinates etc.
    """
    # Create an empty pandas DataFrame to store the matched data
    matched_df = pd.DataFrame()

    # Loop over each row in the target DataFrame
    for index, row in target_df.iterrows():
        # Select all spans of text on the page
        # specified in this row of the target DataFrame
        fdf = raw_df[raw_df[Labels.page.value] == row[Labels.page.value]]

        # Loop over each target string in this row of the target DataFrame
        for index, target in enumerate(row[Labels.target.value]):
            # Apply the match_found_percentage function
            # to each span of text in the filtered DataFrame
            fdf[
                [
                    Labels.match_found.value,
                    Labels.p_match_found.value,
                    Labels.target_matched.value,
                ]
            ] = fdf.apply(
                lambda r: match_found_percentage(
                    target,
                    r[Labels.text.value],
                    r[Labels.match_found.value],
                    r[Labels.p_match_found.value],
                    r[Labels.target_matched.value],
                ),
                axis=1,
            )

        # Concatenate the filtered DataFrame with the matched DataFrame
        matched_df = pd.concat([matched_df, fdf])

    # Return the completed matched DataFrame
    return matched_df


def assign_coordinates(  # noqa C901
    document: fitz.fitz.Document,
    page_no: int,
    text: str,
    targets_matched: List[str],
    coordinates: Tuple[float, float, float, float],
    p_match_found: float,
) -> Dict[str, Tuple[float, float, float, float]]:
    """
    Function to assign exact coordinates for replacement for every targets matched
    Argument(s):
        - document(fitz.fitz.Document): pdf document being processed
        - page_no(int): document page number
        - text(str): text to search the target string in
        - targets_matched(List[str]): the list of matched targets
        - coordinates(Tuple[float, float, float, float]):
        indicates if a match has been found previously
        - p_match_found(float): matching percentage

    Returns:
        - Dict[str, Tuple[float, float, float, float]]:
        a dictionary with target and its exact coordinates
    """
    page = document[page_no]

    exact_coordinate_map = {}

    all_words = page.get_text("words", clip=coordinates)
    # store the coordinates of every word in the span text
    word_coordinate_map = {}
    for word in all_words:
        clean_text = remove_punctution_text(word[4])
        if clean_text not in word_coordinate_map.keys():
            word_coordinate_map[clean_text] = [(word[0], word[1], word[2], word[3])]
        else:
            word_coordinate_map[clean_text].append((word[0], word[1], word[2], word[3]))

    def find_coordinate_combination(
        token_coordinate: List[List[Tuple[float, float, float, float]]]
    ) -> List[Tuple[float, float, float, float]]:
        """
        Function to generate all possible coordinate combinations for a target
        Argument(s):
            - token_coordinate(List[List[Tuple[float, float, float, float]]]):
            coordinates of tokens

        Returns:
            - List[Tuple[float, float, float, float]]:list of tuple of coordinates
        """
        coordinates_cross = []
        for element in itertools.product(*token_coordinate):

            min_x = min_y = float("inf")
            max_x = max_y = float("-inf")
            for coordinate_quads in element:
                x0, y0, x1, y1 = coordinate_quads
                min_x = min(min_x, x0)
                min_y = min(min_y, y0)
                max_x = max(max_x, x1)
                max_y = max(max_y, y1)

            merged_coordinates = (min_x, min_y, max_x, max_y)
            coordinates_cross.append(merged_coordinates)
        return coordinates_cross

    for p_match, target in sorted(zip(p_match_found, targets_matched), reverse=True):
        if p_match == 0.0:
            continue
        # split the target into tokens
        target_tokens = remove_punctuation_list(target.split(" "))
        # assign coordinates to every word in the target
        try:
            token_coordinate = [word_coordinate_map[tokens] for tokens in target_tokens]
        except KeyError:
            logger.info(f"Key Error Encountered for target{target}")
            continue
        # find all possible coordinate combination for the target
        coordinates_cross = find_coordinate_combination(token_coordinate)

        # search for the target in the coordinate area
        search_for_coordinates = page.search_for(target, clip=coordinates)

        # find the best match between coordinate_cross and search_for_coordinates
        iou_max = 0
        best_coordinate = coordinates_cross[0]
        for coordinates in coordinates_cross:
            for search_coordinates in search_for_coordinates:
                iou = bb_intersection_over_union(coordinates, search_coordinates)
                if iou >= iou_max:
                    iou_max = iou
                    best_coordinate = coordinates

        exact_coordinate_map[target] = best_coordinate

        # remove the target text from the word_coordinate_map and match it
        for token, coordinate in zip(target_tokens, token_coordinate):
            if len(coordinate) == 1:
                del word_coordinate_map[token]
            else:
                iou_max = 0
                bc = coordinate[0]
                for coord in coordinate:
                    iou = bb_intersection_over_union(coord, best_coordinate)
                    if iou >= iou_max:
                        iou_max = iou
                        bc = coord
                word_coordinate_map[token].remove(bc)
    return exact_coordinate_map


def assign_replacement_text(  # noqa C901
    replacement_dict: Dict[str, str]
) -> Dict[str, str]:
    """
    Function to assign replacementment text to every target
    Argument(s):
        - replacement_dict(Dict[str, str]): a dictionary with targets and their
          respective entity types as keys and blank as value
    Returns:
        - Dict[str, str]: dictionary with replacement text filled as values
    """
    synthetic_database_path = (
        "src/dsd/hydra/benchmarks/blinding/synthetic_data_generation/datafiles/"
    )
    key_name = [
        key for key in replacement_dict if key[0] == Synthetic_Entities.name.value
    ]
    original_text = key_name[0][1]
    filetype = "." + FileTypes.txt.value
    name_list = file_to_list(
        synthetic_database_path + Synthetic_Database.names.value + filetype
    )
    replacement_text = get_random_text(original_text, name_list)
    key = (key_name[0][0], key_name[0][1])
    replacement_dict[key] = replacement_text

    for key in replacement_dict.keys():
        entity_type = key[0]
        original_text = key[1]

        if entity_type == Synthetic_Entities.first_name.value:
            replacement_text = [
                replacement_dict[k]
                for k, v in replacement_dict.items()
                if k[0] == Synthetic_Entities.name.value
            ][0].split()[0]

        elif entity_type == Synthetic_Entities.last_name.value:
            replacement_text = [
                replacement_dict[k]
                for k, v in replacement_dict.items()
                if k[0] == Synthetic_Entities.name.value
            ][0].split()[1]

        elif entity_type == Synthetic_Entities.name.value:
            replacement_text = [
                replacement_dict[k]
                for k, v in replacement_dict.items()
                if k[0] == Synthetic_Entities.name.value
            ][0]

        elif entity_type == Synthetic_Entities.initials.value:
            full_name = [
                replacement_dict[k]
                for k, v in replacement_dict.items()
                if k[0] == Synthetic_Entities.name.value
            ][0].split()
            replacement_text = "".join(name[0].upper() for name in full_name)

        elif entity_type == Synthetic_Entities.org.value:
            if replacement_dict[key] == "":
                org_list = file_to_list(
                    synthetic_database_path
                    + Synthetic_Database.org_names.value
                    + filetype
                )
                replacement_text = get_random_text(original_text, org_list)

        elif entity_type == Synthetic_Entities.gender.value:
            if replacement_dict[key] == "":
                gender_list = file_to_list(
                    synthetic_database_path
                    + Synthetic_Database.genders.value
                    + filetype
                )
                replacement_text = get_random_text(original_text, gender_list)

        elif entity_type == Synthetic_Entities.state.value:
            if replacement_dict[key] == "":
                state_list = file_to_list(
                    synthetic_database_path + Synthetic_Database.states.value + filetype
                )
                replacement_text = get_random_text(original_text, state_list)

        elif entity_type == Synthetic_Entities.state_cd.value:
            if replacement_dict[key] == "":
                state_cd_list = file_to_list(
                    synthetic_database_path
                    + Synthetic_Database.state_cd.value
                    + filetype
                )
                replacement_text = get_random_text(original_text, state_cd_list)

        elif entity_type == Synthetic_Entities.postal_cd.value:
            if replacement_dict[key] == "":
                postal_cd_list = file_to_list(
                    synthetic_database_path
                    + Synthetic_Database.postal_cd.value
                    + filetype
                )
                replacement_text = get_random_text(original_text, postal_cd_list)

        elif entity_type == Synthetic_Entities.nationality.value:
            if replacement_dict[key] == "":
                nationality_list = file_to_list(
                    synthetic_database_path
                    + Synthetic_Database.nationality.value
                    + filetype
                )
                replacement_text = get_random_text(original_text, nationality_list)

        elif entity_type == Synthetic_Entities.phone_number.value:
            if replacement_dict[key] == "":
                phone_list = file_to_list(
                    synthetic_database_path
                    + Synthetic_Database.phone_numbers.value
                    + filetype
                )
                replacement_text = get_random_text(original_text, phone_list)

        elif entity_type == Synthetic_Entities.email_address.value:
            full_name = [
                replacement_dict[k]
                for k, v in replacement_dict.items()
                if k[0] == Synthetic_Entities.name.value
            ][0].split()
            replacement_text = (
                ".".join(name.lower() for name in full_name)[
                    : len(original_text.split("@")[0])
                ]
                + Email.address_suffix.value
            )

        elif entity_type == Synthetic_Entities.date_time.value:
            if replacement_dict[key] == "":
                dob_list = file_to_list(
                    synthetic_database_path
                    + Synthetic_Database.date_of_births.value
                    + filetype
                )
                replacement_text = get_random_text(original_text, dob_list)

        elif entity_type == Synthetic_Entities.country.value:
            if replacement_dict[key] == "":
                country_list = file_to_list(
                    synthetic_database_path
                    + Synthetic_Database.countries.value
                    + filetype
                )
                replacement_text = get_random_text(original_text, country_list)

        elif entity_type == Synthetic_Entities.city.value:
            if replacement_dict[key] == "":
                city_list = file_to_list(
                    synthetic_database_path + Synthetic_Database.cities.value + filetype
                )
                replacement_text = get_random_text(original_text, city_list)

        elif entity_type == Synthetic_Entities.country_cd.value:
            if replacement_dict[key] == "":
                country_cd_list = file_to_list(
                    synthetic_database_path
                    + Synthetic_Database.country_cd.value
                    + filetype
                )
                replacement_text = get_random_text(original_text, country_cd_list)

        elif entity_type == Synthetic_Entities.address.value:
            if replacement_dict[key] == "":
                address_list = file_to_list(
                    synthetic_database_path
                    + Synthetic_Database.addresses.value
                    + filetype
                )
                replacement_text = get_random_text(original_text, address_list)

        elif entity_type == Synthetic_Entities.age.value:
            replacement_text = str(random.randint(21, 60))

        elif entity_type == Synthetic_Entities.marital_status.value:
            marital_status_list = file_to_list(
                synthetic_database_path
                + Synthetic_Database.marital_status.value
                + filetype
            )
            replacement_text = get_random_text(original_text, marital_status_list)

        elif entity_type == Synthetic_Entities.pri.value:
            pri_list = file_to_list(
                synthetic_database_path + Synthetic_Database.pri.value + filetype
            )
            replacement_text = get_random_text(original_text, pri_list)

        elif entity_type == Synthetic_Entities.other_name.value:
            name_list = file_to_list(
                synthetic_database_path + Synthetic_Database.names.value + filetype
            )
            replacement_text = get_random_text(original_text, name_list)

        elif entity_type == Synthetic_Entities.other_email.value:
            name_list = file_to_list(
                synthetic_database_path
                + Synthetic_Database.email_addresses.value
                + filetype
            )
            replacement_text = get_random_text(original_text, name_list)

        elif entity_type == Synthetic_Entities.others.value:
            replacement_text = original_text

        replacement_dict[key] = replacement_text
    return replacement_dict


def extract_target_coord(
    coord_dict: Dict[str, Tuple[float, float, float, float]], text: str
) -> Tuple[float, float, float, float]:
    """
    Function to extract coordinates of a given text from the dictionary
    Argument(s):
        - coord_dict (Dict[float, float, float, float]):
        a dictionary with text as keys and
        coordinates as values
        - text (str): string to find the coordinates of
    Returns:
        - Tuple[float, float, float, float]: coordinates of the text in the document
    """
    return coord_dict.get(text)


def get_synthetic_df(matched_df: pd.DataFrame) -> pd.DataFrame:
    """
    Function to assign replacementment text to every target
    Argument(s):
        - matched_df(pd.DataFrame): pandas dataframe with details of all targets like
        their entity types, coordinates, page number, exact_coordinates etc.
    Returns:
        - pd.DataFrame: a pandas DataFrame with one row generated for each
        target, entity_type pair
    """
    synthetic_df = matched_df[
        [
            Labels.page.value,
            Labels.coordinates.value,
            Labels.target_matched.value,
            Labels.entity_types.value,
            Labels.exact_coordinates.value,
        ]
    ]
    synthetic_df = synthetic_df.explode(
        [Labels.target_matched.value, Labels.entity_types.value]
    )

    synthetic_df[Synthetic_Labels.target_coordinates.value] = synthetic_df.apply(
        lambda row: extract_target_coord(
            row[Labels.exact_coordinates.value], row[Labels.target_matched.value]
        ),
        axis=1,
    )

    replacement_dict = {
        (
            row[Synthetic_Labels.entity_type.value],
            row[Synthetic_Labels.target_matched.value],
        ): ""
        for index, row in synthetic_df.iterrows()
    }

    # assign values(replacement text) to replacement dict keys(entity, target pairs)
    replacement_dict = assign_replacement_text(replacement_dict)

    def get_replacement_text(row: pd.Series) -> pd.DataFrame:
        """
        Function to extract coordinates of a given text from the dictionary
        Argument(s):
                - row (pd.Series): one row of synthetic dataframe

        Returns:
            - pd.DataFrame: replacement text for the entity_type and
            target_matched values
        in the row
        """
        key = (
            row[Synthetic_Labels.entity_type.value],
            row[Synthetic_Labels.target_matched.value],
        )
        return replacement_dict.get(key)

    synthetic_df[Synthetic_Labels.replacement_text.value] = synthetic_df.apply(
        get_replacement_text, axis=1
    )
    return synthetic_df


def get_font_details(
    synthetic_df: pd.DataFrame, raw: fitz.fitz.Document
) -> pd.DataFrame:
    """
    Function to extract font details of every target from the pdf document
    Argument(s):
        - synthetic_df (pd.DataFrame): pandas dataframe with details of all targets like
        their entity types, coordinates, replacement text, page number etc.
        - document(fitz.fitz.Document): pdf document being processed
    Returns:
        - synthetic_df (pd.DataFrame): pandas dataframe with font details of targets
        added to the recieved DataFrame
    """

    synthetic_df[Synthetic_Labels.fontname.value] = ""
    synthetic_df[Synthetic_Labels.fontsize.value] = ""
    synthetic_df[Synthetic_Labels.origin.value] = ""
    synthetic_df = synthetic_df.reset_index()
    for index, row in synthetic_df.iterrows():
        if not pd.isnull(row[Synthetic_Labels.target_matched.value]):
            page_no = row[Synthetic_Labels.page.value]
            page = raw[page_no]
            for blocks in page.get_text(
                "dict", clip=row[Synthetic_Labels.target_coordinates.value]
            )["blocks"]:
                if blocks["type"] == 0:
                    for line in blocks["lines"]:
                        for span in line["spans"]:
                            synthetic_df.at[
                                index, Synthetic_Labels.fontname.value
                            ] = span["font"]
                            synthetic_df.at[
                                index, Synthetic_Labels.fontsize.value
                            ] = span["size"]
                            synthetic_df.at[
                                index, Synthetic_Labels.origin.value
                            ] = fitz.Point(
                                span["origin"]
                            )  # the insertion point
    return synthetic_df


def insert_replacement_text(
    synthetic_data_info: pd.DataFrame, raw: fitz.fitz.Document
) -> fitz.fitz.Document:
    """
    Function to replace every recorded target with a replacement text
    Argument(s):
        - synthetic_df (pd.DataFrame): pandas dataframe with details of all targets like
        their entity types, coordinates, replacement text, page number etc.
        - document(fitz.fitz.Document): pdf document being processed
    Returns:
        - fitz.fitz.Document: pdf document with PII information replaced with
        synthetic data
    """
    base_14_fonts_yaml_path = (
        "src/dsd/hydra/benchmarks/blinding/synthetic_data_generation/base_14_fonts.yml"
    )
    base_14_fonts = yaml.load(open(base_14_fonts_yaml_path), yaml.Loader)
    synthetic_data_df = pd.DataFrame(synthetic_data_info)
    for index, page in enumerate(raw):
        filter_df = synthetic_data_df[
            synthetic_data_df[Synthetic_Labels.page.value] == index
        ]
        for index, info in filter_df.iterrows():
            if not pd.isnull(info[Synthetic_Labels.target_coordinates.value]):
                value = fitz.Rect(info[Synthetic_Labels.target_coordinates.value])
                delta = value.height * 0.2
                adjusted_pii_coordinates = value + (0, delta, 0, -delta)
                page.add_redact_annot(adjusted_pii_coordinates)  # redact the word
        page.apply_redactions()
        for index, info in filter_df.iterrows():
            if not pd.isnull(info[Synthetic_Labels.target_coordinates.value]):
                fontname = info[Synthetic_Labels.fontname.value]
                if fontname not in base_14_fonts:
                    fontname = Font.default_font.value
                tl = fitz.get_text_length(
                    info[Synthetic_Labels.replacement_text.value],
                    fontname=fontname,
                    fontsize=info[Synthetic_Labels.fontsize.value],
                )
                # then adjust fontsize, so its fits exactly
                fsize = (
                    info[Synthetic_Labels.fontsize.value]
                    * fitz.Rect(info[Synthetic_Labels.target_coordinates.value]).width
                    / tl
                )
                if fsize > info[Synthetic_Labels.fontsize.value]:
                    fsize = info[Synthetic_Labels.fontsize.value]
                page.insert_text(
                    fitz.Point(info[Synthetic_Labels.origin.value]),
                    info[Synthetic_Labels.replacement_text.value],
                    fontname=fontname,
                    fontsize=fsize,
                    color=(0, 0, 0),
                )
    return raw
