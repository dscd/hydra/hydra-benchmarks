# -*- coding: utf-8 -*-
"""
Minio wrapper class that extends functionalities to the original API.



Created on Thursday Nov 19 12:13:20 2020
@authors:  Andres Solis Montero
"""
import io
import re
import time
from os.path import basename, dirname, join
from typing import Generator, List, Optional, Union
import minio
from minio import Minio
from minio.commonconfig import CopySource
from dsd.hydra.benchmarks.config.api import S3, S3Location, settings

# assert minio.__version__ == "6.0.0"


class S3Client(Minio):
    """
    Minio wrapper class that extends functionalities
    to the original API.

    Attributes:
        config_json_path str:
            Path to a json file having the MINIO_URL|ACCESS_KEY|SECRETE_KEY
        empty_folder_name str:
            Default name while creating an "empty folder", default = '.empty'
    """

    @staticmethod
    def get_client():
        """
        Get a connection to the minimal Minio tenant

        Arguments:
            - None

        Returns:
            - (S3Client): The S3Client class
        """
        return S3Client()

    def __init__(self, empty_folder_filename: str = ".empty") -> None:
        """
        Inits S3Client

        Arguments:
            - config_json_path (str): path to a json file
                                        having the MINIO_URL|ACCESS_KEY|SECRETE_KEY
            - empty_folder_name (str): default name while creating an
                                        "empty folder", default = '.empty'
        Returns:
            - (None)
        """
        self._default_folder_file_ = empty_folder_filename

        # Get rid of http:// in minio URL
        def http(url):
            return re.sub("^https?://", "", url)

        def secure(url):
            return re.match("^https://", url)

        # Get MinIO URL + creds from environment variables -
        # if config_json_path is passed then get
        # MinIO url/creds from json. Otherwise get them
        # from environment variables.
        MINIO_URL = settings.MINIO_URL
        MINIO_ACCESS_KEY = settings.MINIO_ACCESS_KEY
        MINIO_SECRET_KEY = settings.MINIO_SECRET_KEY

        # Create the minio client.
        Minio.__init__(
            self,
            http(MINIO_URL),
            access_key=MINIO_ACCESS_KEY,
            secret_key=MINIO_SECRET_KEY,
            secure=secure(MINIO_URL),
            region="us-west-1",
        )

    def ls_objs(
        self,
        bucket: str,
        prefix: str,
        recursive: bool = False,
        only_files: bool = False,
        default_folder_file: bool = False,
    ) -> Generator:
        """
        Same API call as minio.list_objects with additional
        functionalities. It will not list the
        `.empty` files inside folders if default_folder_file
        is set to False.

        Arguments:
            - bucket (str): The name of the s3 storage bucket where we are
                            listing objects from.
            - prefix (str): The prefix to the location where we are listing
                            objects from.
            - recursive (bool): If `True`, recursively list objects in folders
                                contained at the given prefix.
            - only_files (bool): If `True`, only list files. Folder can be displayed
                                when recursive is set to False.
            - default_folder_file (bool): If `True`, displays the
                                          `self._default_folder_file_`
                                           filename if found.

        Returns:
            - (Object): Objects at a given s3 bucket and prefix.
        """
        for obj in self.list_objects(bucket, prefix=prefix, recursive=recursive):
            if only_files and obj.is_dir:
                continue
            if (
                not default_folder_file
                and basename(obj.object_name) == self._default_folder_file_
            ):
                continue
            yield obj

    def mv_obj(
        self, src_bucket: str, src_obj: str, dst_obj: str, dst_bucket: str = None
    ) -> "minio.helpers.ObjectWriteResult":
        """
        Moves an object to a different "path". It does it by
        using `copy_object`and `remove_object`
        from `minio.Minio`. If no dst_bucket is defined,
        the `src_bucket` is used. The source
        object will be deleted.

        !!! note
            The copying and removing of objects happens
            entirely on the server side.


        Arguments:
            - src_bucket (str): The source S3 bucket.
            - src_obj (str): The source S3 object prefix.
            - dst_obj (str): The destination S3 bucket.
            - dst_bucket (str): The destination S3 object prefix.

        Returns:
            - (minio.helpers.ObjectWriteResult): The new location of
                                                    the moved object.
        """
        new_object = self.cp_obj(src_bucket, src_obj, dst_obj, dst_bucket)
        self.remove_object(src_bucket, src_obj)
        return new_object

    def cp_obj(
        self, src_bucket: str, src_obj: str, dst_obj: str, dst_bucket: str = None
    ) -> "minio.helpers.ObjectWriteResult":
        """
        Copies a object from a bucket/path to another bucket
        path in the server side. If no
        dst_bucket is defined, the src_bucket is used.
        This is a thin wrapper around
        `Minio.copy_object`.

        Arguments:
            - src_bucket (str): The source S3 bucket.
            - src_obj (str): The source S3 object prefix.
            - dst_obj (str): The destination S3 bucket.
            - dst_bucket (str): The destination S3 object prefix.

        Returns:
            - (minio.helpers.ObjectWriteResult): The new location of
                                                    the copied object.
        """
        dst_bucket = dst_bucket if dst_bucket else src_bucket

        return self.copy_object(dst_bucket, dst_obj, CopySource(src_bucket, src_obj))

    def mkdir(self, bucket: str, folder: str) -> None:
        """
        Creates an empty "folder" by creating an empty file
        `self._default_folder_file` in the
        specified folder path. This is used because S3 storage
         is not technically a file system.
        It only preserves a prefix name if there is at least
        one file (object) under that prefix.
        The convention to deal with this is to include a
        dummy hidden file called ".empty" at the
        empty prefix location we want to preserve.

        Arguments:
            - bucket (str): The name of the S3 storage bucket that the folder is in.
            - folder (str): The folder path that we want to make in S3 storage.

        Returns:
            - (None)
        """
        _object_ = join(folder, self._default_folder_file_)
        content = ""
        content_as_bytes = content.encode("utf-8")
        content_as_stream = io.BytesIO(content_as_bytes)
        self.put_object(
            bucket, _object_, content_as_stream, len(content_as_bytes), "text/plain"
        )

    def rm(self, bucket: str, folder: str, remove_dir: bool = False) -> None:
        """
        Removes all the content of a "folder", all objects
        with the same prefix will be deleted.

        Arguments:
            - bucket (str): The bucket containing the content to be removed.
            - folder (str): The folder (prefix) containing the content to be removed.
            - remove_dir (bool): If `True`, also remove the `.empty` default file,
                                otherwise delete the `.empty` file as well
                                and remove the prefix location from S3 storage.
        Returns:
            - (None)
        """
        for obj in self.ls_objs(
            bucket,
            prefix=folder,
            recursive=True,
            only_files=True,
            default_folder_file=remove_dir,
        ):
            self.remove_object(bucket, obj.object_name)

    def mv_loc(self, src: S3Location, dst: S3Location):
        """
        Moves an object from a source S3Location to another.
        This operation is done in the server. No downloads.

        Arguments:
            - src (S3Location): Source S3Location
            - dst (S3Location): Destination S3Location

        Returns:
            - See minin API for the output of minio.mv_obj
        """
        return self.mv_obj(
            src.bucket, S3.loc_to_obj(src), S3.loc_to_obj(dst), dst.bucket
        )

    def cp_loc(self, src: S3Location, dst: S3Location):
        """
        Copies an object from a S3Location to a S3Location

        Arguments:
            - src (S3Location): Source S3Location
            - dst (S3Location): Destination S3Location

        Returns:
            - See minin API for the output of minio.cp_obj

        """
        return self.cp_obj(
            src.bucket, S3.loc_to_obj(src), S3.loc_to_obj(dst), dst.bucket
        )

    def stat_loc(self, location: S3Location) -> Union[object, None]:
        """
        Retrieve objects stats from a S3Location.
        Throws minio.error.NoSuchKey exception if object is not found

        Arguments:
            - location (S3Location): S3Location of the object

        Returns:
            - (Union[object, None]): see minio.stat_object for details
        """
        try:
            return self.stat_object(location.bucket, S3.loc_to_obj(location))
        except Exception:
            return None

    def _get_last_updated(self, loc: S3Location) -> Optional[time.struct_time]:
        """
        Gets the time that an object was last updated.

        Arguments:
            - loc (S3Location): An S3Location object

        Returns:
            - (Optional[time.struct_time])
        """
        stats = self.stat_loc(loc)
        return stats.last_modified if stats else None

    def dependency_change(
        self, deps: List[S3Location], outputs: List[S3Location]
    ) -> bool:
        """
        Checks if a dependency has changed more recently than its output.

        Arguments:
            - deps (List[S3Location]): A list of S3Locations
            - output (List[S3Location]): A list of S3Locations

        Returns:
            - (bool): True if dependency has changed more than output
        """
        if not deps or not outputs:
            return True
        deps_list = [self._get_last_updated(dep) for dep in deps]
        outputs_list = [self._get_last_updated(out) for out in outputs]
        # Get the date of the most recently modified dependency
        if len(deps_list) == 1:
            last_modified_dep = deps_list.pop()
        else:
            last_modified_dep = max(*deps_list)
        # Get the last modified date of the output
        if len(outputs_list) == 1:
            last_modified_output = outputs_list.pop()
        else:
            last_modified_output = min(*outputs_list)
        # If the output was modified more recently than any of the
        # inputs, then there has not been
        # a dependency change since it was last computed.
        # Otherwise there has and it needs to be
        # recomputed.
        return last_modified_dep > last_modified_output

    def locs_exist(self, locs: List[S3Location]) -> bool:
        """
        Given a list of S3Locations, returns true if every
        one exists and returns false otherwise.

        Arguments:
            - locs (List[S3Location]): A list of S3Locations

        Returns:
            - (bool): True if everyone exists

        """
        return None not in [self.stat_loc(loc) for loc in locs]

    def put_loc(self, loc: S3Location, data, length, **kawrgs):
        """
        A wrapper on top of minio.put_object.
        It uploads data from a stream to an object in a bucket

        Arguments:
            - loc (S3Location): An S3Location
            - data (Object): An object having callable read() returning bytes object
            - length (int): Data size; -1 for unknown size and set valid part_size

        Returns:
            - See minin API for the output of minio.put_object
        """
        return self.put_object(loc.bucket, S3.loc_to_obj(loc), data, length, **kawrgs)

    def fput_loc(self, loc: S3Location, file_path: str, **kwargs):
        """
        Wrapper function of minio API fput_object but using an S3Location

        Arguments:
            - loc (S3Location): A S3Location namedtuple
            - file_path (str): The local filename to copy to the S3 Location

        Returns:
           - See minio API for the output of minio.fput_object
        """
        return self.fput_object(loc.bucket, S3.loc_to_obj(loc), file_path, **kwargs)

    def fget_loc(self, loc: S3Location, localpath: str, **karwgs):
        """
        Wrapper function of minio API fget_object but using an S3Location

        Arguments:
            - loc (S3Location): A S3Location namedtuple
            - localpath (str): Local path to download the S3 Location file to

        Returns:
            - See minio API for the output of minio.fget_object
        """
        return self.fget_object(loc.bucket, S3.loc_to_obj(loc), localpath, **karwgs)

    def ls_locs(
        self,
        loc: S3Location,
        recursive: bool = False,
        only_files: bool = False,
        default_folder_file: bool = False,
    ) -> Generator:
        """
        Extended version of ls_objs to use S3Locations objects.

        Arguments:
            - loc (S3Location): An S3Location
            - recursive (bool): If `True`, recursively list objects in folders
                    contained at the given prefix
            - only_files (bool): If `True`, only list files. Folder can be displayed
                                    when recursive is set to False
            - default_folder_file (bool): If `True`, displays the
                                            `self._default_folder_file_`
                                             filename if found.

        Returns:
            - (Generator): Yields objects at a given s3 bucket and prefix
        """
        for obj in self.ls_objs(
            loc.bucket,
            loc.prefix,
            recursive=recursive,
            only_files=only_files,
            default_folder_file=default_folder_file,
        ):
            yield S3Location(
                obj.bucket_name,
                prefix=join(dirname(obj.object_name), ""),
                basename=basename(obj.object_name),
            )
