from typing import List, Optional

from dsd.hydra.benchmarks.blinding.blinding_config import Context, Deny_List, Entities
from presidio_analyzer import PatternRecognizer


class GenderRecognizer(PatternRecognizer):
    """
    Recognize Gender using Deny List feature.
    :param deny_list: A predefined list of words to detect
    :param context: list of context words
    """

    DENY_LIST = [
        Deny_List.male_l.value,
        Deny_List.female_l.value,
        Deny_List.female_u.value,
        Deny_List.male_u.value,
        Deny_List.non_binary.value,
        Deny_List.trans.value,
    ]
    CONTEXT = [Context.gender.value]

    def __init__(
        self,
        deny_list: Optional[List[str]] = None,
        context: Optional[List[str]] = None,
        supported_entity: str = Entities.gender.value,
    ):
        deny_list = deny_list if deny_list else self.DENY_LIST
        context = context if context else self.CONTEXT
        super().__init__(
            supported_entity=supported_entity,
            deny_list=deny_list,
            context=context,
        )
