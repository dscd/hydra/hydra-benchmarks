from typing import List

import easyocr
from dsd.hydra.benchmarks.models.schemas import RegionInfo


class EocrHelper:
    """
    This a class built on top of the EasyOCR system which is based on a template
    matching algorithm.
    It parses the input image and compares it with a set of pre-defined templates
    to find the best match and then returns the OCR output.
    """

    def __init__(self):
        """
        This method is used to initialize the Easy OCR with the custom configuration.
        In case a configuration is not selected, the default is used.

        Arguments:
            - None

        Returns:
            - None
        """

        self.reader = easyocr.Reader(["en"], gpu=False)

    def get_regions(self, img) -> List:
        """
        This method takes a Pillow image as input and returns a list of Region objects.

        Arguments:
            - img (Image.Image): A Pillow image of what is to be extracted

        Returns:
            - (List): List of Region objects
        """

        # EasyOCR's function to get both image and data is called and saved as
        # heterogenous list.
        # The details consist of position, confidence and text.
        ocr_output = self.reader.readtext(img.filename, detail=1)

        # defining an empty list to store all regions
        # each block of text detected by the ocr is a region
        regions = []

        # for each region, an RegionInfo model is created and added to the regions
        # list.
        # the coordinates in the OCR output are in the form [(x1,y1),(x2,y1),
        #                                                    (x2,y2),(x1,y2)]
        # which are converted to the standard form of (x1,y1,x2,y2).
        for val in ocr_output:
            coords = tuple(val[0][0] + val[0][2])
            text = val[1]
            image_region = RegionInfo(coordinates=coords, text=text)
            regions.append(image_region)

        return regions
