from typing import Dict, List

import pandas as pd
import yaml
import dash
from dash import dash_table, dcc, html
from dash.dependencies import Input, Output
from dsd.hydra.benchmarks.models.compare_schema import CompareOutput
from dsd.hydra.benchmarks.modules.report import (
    Label,
    ReportData,
    SummaryType,
    get_metrics,
    get_run_info,
)


def create_tables(
    df: pd.DataFrame,
    columns: List[str],
    header_df: pd.DataFrame,
    render_dict: Dict[str, Dict[str, Dict[str, float]]],
) -> html.Div:
    """
    Generates the table layout for DataFrame

    Arguments:
        - df (pd.DataFrame) : The dataframe with which the table needs to be updated
        - columns: List[str] : list of table columns

    Returns:
        - table_layout (html.Div)
    """
    table_layout = html.Div(
        [
            html.H1(Label.title.value, style={"textAlign": "center"}),
            html.Div(
                id="header-df",
                children=[
                    dash_table.DataTable(
                        data=header_df.to_dict("records"),
                        columns=[{"name": col, "id": col} for col in header_df.columns],
                        style_cell={
                            "font-size": "18px",
                            "font-weight": "bold",
                            "text-align": "center",
                        },
                    )
                ],
            ),
            html.Br(),
            html.Br(),
            html.Hr(),
            html.Br(),
            html.Br(),
        ]
    )

    for index, (algorithm, subset_dict) in enumerate(render_dict.items()):
        label_algorithm = Label.algorithm_index.value.format(
            index=index + 1, total=algorithm
        )
        table_layout.children.append(
            html.H2(
                id=f"{algorithm}-div",
                children=label_algorithm,
                style={
                    "display": "flex",
                    "margin": "auto",
                    "align-items": "center",
                },
            )
        )
        table_layout.children.append(html.Br())
        for index, (subset, subset_data) in enumerate(subset_dict.items()):
            filtered_df = df[(df["Algorithm"] == algorithm) & (df["Subset"] == subset)]
            filtered_df = filtered_df.drop(["Algorithm", "Subset"], axis=1)
            filtered_df.columns = columns
            filtered_df = filtered_df.round(4)
            label_index = Label.subset_index.value.format(index=index + 1, total=subset)
            table_layout.children.append(
                html.H3(
                    id=f"{algorithm}-{subset}-div",
                    children=label_index,
                    style={
                        "display": "flex",
                        "margin": "auto",
                        "align-items": "center",
                    },
                )
            )
            table_layout.children.append(html.Br())
            et = subset_dict[subset]["exec_time"]
            exec_time = Label.exec_time.value.format(exec_time=et)

            table_layout.children.append(
                html.H3(
                    id=f"{algorithm}-{subset}-div",
                    children=exec_time,
                    style={
                        "display": "flex",
                        "margin": "auto",
                        "align-items": "center",
                    },
                )
            )

            table_layout.children.append(html.Br())

            table_layout.children.append(
                html.Div(
                    id=f"{algorithm}-{subset}-df-div",
                    children=[
                        dash_table.DataTable(
                            data=filtered_df.to_dict("records"),
                            columns=[{"name": i, "id": i} for i in filtered_df.columns],
                            style_cell={
                                "font-size": "15px",
                                "font-weight": "bold",
                                "text-align": "center",
                            },
                            sort_action="native",
                            editable=True,
                        )
                    ],
                )
            )

            table_layout.children.append(html.Br())
            table_layout.children.append(html.Hr())
            table_layout.children.append(html.Br())
    return table_layout


def compute_displaying_data(filename: str) -> ReportData:
    """
    Function to get the processed data from the DetectionFile
    Arguments:
        - filename (str) : detection file path
    Returns:
        - ReportData
    """
    detection_file = CompareOutput(**dict(yaml.load(open(filename), yaml.Loader)))
    metrics_per_subset, metrics_per_file = get_metrics(detection_file)
    header_df, render_dict = get_run_info(detection_file)
    report_data = ReportData(
        metrics_per_subset=metrics_per_subset,
        metrics_per_file=metrics_per_file,
        header_df=header_df,
        render_dict=render_dict,
    )
    return report_data


def dash_dashapp(dash_app: dash.Dash, detection_file_path: str) -> None:
    """
    Function to define the Dash layout and callbacks
    Arguments:
        - dash_app (dash.Dash)
        - detection_file_path (str)
    """

    dash_app.layout = html.Div(
        [
            dcc.Tabs(
                id="tabs",
                value="summary",
                children=[
                    dcc.Tab(
                        label=Label.summary_report.value,
                        value=SummaryType.summary.value,
                        children=[html.Div(id="summary-content")],
                    ),
                    dcc.Tab(
                        label=Label.detailed_report.value,
                        value=SummaryType.detailed.value,
                        children=[html.Div(id="detailed-content")],
                    ),
                ],
            ),
            html.Div(id="tab-content"),
        ]
    )

    report = compute_displaying_data(detection_file_path)

    @dash_app.callback(
        Output(component_id="tab-content", component_property="children"),
        [Input(component_id="tabs", component_property="value")],
    )
    def render_content(tab):

        columns = [
            Label.iou_thr.value,
            Label.filename.value,
            Label.precision.value,
            Label.recall.value,
            Label.f1.value,
            Label.region_counts.value,
            Label.text_accuracy.value,
            Label.cer.value,
        ]
        if tab == SummaryType.summary.value:
            columns.remove(Label.filename.value)
            return create_tables(
                report.metrics_per_subset,
                columns,
                report.header_df,
                report.render_dict,
            )

        elif tab == SummaryType.detailed.value:
            return create_tables(
                report.metrics_per_file,
                columns,
                report.header_df,
                report.render_dict,
            )
