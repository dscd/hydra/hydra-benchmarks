import os
from enum import Enum
from os.path import join

import matplotlib.pyplot as plt
import pytest
import yaml
from conftest import Comparison
from typing import Optional
from py._path.local import LocalPath
from pydantic import ValidationError

from dsd.hydra.benchmarks.models.compare_schema import Algorithm
from dsd.hydra.benchmarks.models.schemas import GroundTruth, Prediction
from dsd.hydra.benchmarks.modules.compare import analyze_predictions

# When dev_output contains a string value and it's not None,
# two useful functions become available:
# 1. dump_current_model: Write the current output of the
#    analyze_prediction function in the corresponding
#    case test folder (in a yaml file).
# 2. plot_metrics: Generates plots for performance metrics
#    based on the current dev output.


# Select test_cases
case_numbers = [
    "001",
    "002",
    "003",
    "004",
    "005",
    "006",  # Tabular
    "007",
    "008",
    "009",
    "010",
    "011",
    "100",
    "101",
    "102",
    "103",
    "104",
    "105",
    "106",  # ocr
    "108",
    "110",
]

#
# case_numbers= ["200"]

comparisons = []
for case_num in case_numbers:
    comparison = Comparison(
        gt=f"case_{case_num}/gt.yaml",
        pred=f"case_{case_num}/prediction.yaml",
        exp_output=f"case_{case_num}/expected_output.yaml",
        curr_output="current_output.yaml",
        tcase=f"case_{case_num}",
    )
    comparisons.append(comparison)


class ColumnLabels(Enum):
    iou = "IOU"
    fn = "FN"
    fp = "FP"
    tp = "TP"
    reg_count = "Reg. Count"
    file_count = "File Count"
    cer = "CER"
    ta = "TA"


# @pytest.mark.compare
@pytest.mark.parametrize("comparison", comparisons)
def test_compare(comparison: Comparison, datadir: LocalPath, dev_output: Optional[str]):
    """
    Test the analyze_prediction function for a given test case.

    Args:
    - comparison (Comparison): The comparison object for the current test case.
    - datadir (LocalPath): Data fixture of the path to the directory containing
        the test data.
    - dev_output (str): Data fixture containing the name of output directory.
                        None if not specified.
    """
    prediction_model, gt_model, expected_model = load_model(
        comparison=comparison, datadir=datadir
    )

    current_model: Algorithm = analyze_predictions(
        pred_file=prediction_model, gt_=gt_model
    )
    if dev_output is not None:
        # Dump Yaml file of the current output of the compare function
        dump_current_model(
            current_model=current_model,
            curr_file=comparison.curr_output,
            output=dev_output,
            tcase=comparison.tcase,
        )

        # Plot metrics as a png in the case curr_output.
        plot_metrics(current_model, datadir, comparison, dev_output, comparison.tcase)

    compare_algorithm_outputs(current_model, expected_model)


def compare_algorithm_outputs(
    current_model: Algorithm, expected_model: Algorithm, tolerance: float = 0.05
) -> bool:
    """
    Compare the actual output of an algorithm with the expected output and
    raise an AssertionError if they are not similar enough.

    Args:
    - current_model (Algorithm): The current output of the function.
    - expected_model (Algorithm): The expected output of the function.
    - tolerance (float): The maximum percentage difference between actual and
        expected values allowed. Default is 0.05.
    """
    assert expected_model is not None
    assert current_model is not None
    compare_iou_list = [metric.iou for metric in current_model.subset_metrics]
    expected_iou_list = [metric.iou for metric in expected_model.subset_metrics]

    if len(compare_iou_list) != len(expected_iou_list) or sorted(
        compare_iou_list
    ) != sorted(expected_iou_list):
        raise AssertionError(
            "IOU lists differ: expected {}, but got {}".format(
                expected_iou_list, compare_iou_list
            )
        )

    for i, (compare_metric, expected_metric) in enumerate(
        zip(current_model.subset_metrics, expected_model.subset_metrics)
    ):
        for field in ["fn", "fp", "tp", "rc", "fc", "p", "r", "f1"]:
            expected_value = getattr(expected_metric.detection, field)
            actual_value = getattr(compare_metric.detection, field)

            if field in ["p", "r", "f1"]:
                diff = abs(actual_value - expected_value)
                max_diff = tolerance * abs(expected_value)
                if diff > max_diff:
                    raise AssertionError(
                        f" {field} field of DetectionMetrics differ \
                            by more than {tolerance*100}% at IOU \
                            {compare_metric.iou}: expected {expected_value}, \
                                but got {actual_value}"
                    )
            else:
                if actual_value != expected_value:
                    raise AssertionError(
                        f" {field} field of DetectionMetrics differ \
                            at IOU {compare_metric.iou}: \
                            expected {expected_value}, but got {actual_value}"
                    )

        for field in ["ta", "cer"]:
            expected_value = getattr(expected_metric.quality, field)
            actual_value = getattr(compare_metric.quality, field)
            diff = abs(actual_value - expected_value)
            max_diff = tolerance * abs(expected_value)
            if diff > max_diff:
                raise AssertionError(
                    f" {field} field of QualityMetrics differ by more than \
                        {tolerance*100}% at IOU {compare_metric.iou}: \
                        expected {expected_value}, but got {actual_value}"
                )
    return True


def plot_metrics(
    compareOutput: Algorithm,
    datadir: LocalPath,
    comparison: Comparison,
    output: str,
    tcase: str,
) -> None:
    """
    Plot the detection metrics (precision, recall, and f1-score)
        versus the IoU values and save the plot in the test case folder.

    Args:
    - compareOutput (Algorithm): The current output of the function to
        plot the metrics for.
    - datadir (LocalPath): The path to the directory containing the test data.
    - comparison (Comparison): The comparison object for the current test case.
    - output (str): String containing the name of the output directory.
    - tcase (str): String containing the current test case number.
    """
    # Extract the subset metrics
    subset_metrics = compareOutput.subset_metrics

    # Extract the iou values for each metric
    ious = [metric.iou for metric in subset_metrics]

    # Extract the precision, recall, and f1-score values for each metric
    precisions = [metric.detection.p for metric in subset_metrics]
    recalls = [metric.detection.r for metric in subset_metrics]
    f1_scores = [metric.detection.f1 for metric in subset_metrics]

    # Create the plot
    fig, ax = plt.subplots(nrows=2, ncols=1, figsize=(6, 8))

    # Add the lines for precision, recall, and f1-score on the first subplot
    ax[0].plot(ious, precisions, label="Precision")
    ax[0].plot(ious, recalls, label="Recall")
    ax[0].plot(ious, f1_scores, label="F1-Score")

    # Add labels and title to the first subplot
    ax[0].set_xlabel("IoU")
    ax[0].set_ylabel("Value")
    ax[0].set_title("Detection Metrics vs IoU for test_subset")
    ax[0].legend()

    # Create the table on the second subplot
    table_data = []
    for metric in subset_metrics:
        table_data.append(
            [
                metric.iou,
                metric.detection.fn,
                metric.detection.fp,
                metric.detection.tp,
                metric.detection.rc,
                metric.detection.fc,
                metric.quality.cer,
                metric.quality.ta,
            ]
        )

    table_columns = [e.value for e in ColumnLabels]
    table = ax[1].table(
        cellText=table_data,
        colLabels=table_columns,
        loc="center",
        cellLoc="center",
        colLoc="center",
    )

    # Adjust table properties
    table.auto_set_font_size(False)
    table.set_fontsize(7)
    table.scale(1, 1.5)

    # Remove the axis ticks and labels on the second subplot
    ax[1].axis("off")

    # Adjust plot properties to make room for table
    fig.subplots_adjust(bottom=0.3)

    # Save plot
    # TODO what's case_num
    # case_num = comparison.tcases
    plot_path = join(output, tcase, "metrics.png")
    plt.savefig(plot_path)


def load_model(comparison: Comparison, datadir: LocalPath):
    """
    Loads and returns prediction, ground truth, and expected algorithm models as
    Pydantic models.

    Args:
        comparison: A Comparison object that contains the file names of the prediction,
            ground truth, and expected algorithm models.
        datadir: A LocalPath object that represents the path to the directory containing
            the model files.

    Returns:
        A tuple containing the loaded prediction, ground truth, and expected
        algorithm models as Pydantic models.
        If loading the expected algorithm model fails, the corresponding element in the
        tuple will be None.
    """

    try:
        expected_model = Algorithm(
            **yaml.load(
                (datadir / comparison.exp_output).read_text(encoding="utf-8"),
                yaml.Loader,
            )
        )
    except (yaml.YAMLError, ValidationError):
        expected_model = None

    prediction_model = Prediction(
        **yaml.load(
            (datadir / comparison.pred).read_text(encoding="utf-8"), yaml.Loader
        )
    )

    gt_model = GroundTruth(
        **yaml.load((datadir / comparison.gt).read_text(encoding="utf-8"), yaml.Loader)
    )

    return prediction_model, gt_model, expected_model


def dump_current_model(
    current_model: Algorithm, curr_file: str, output: str, tcase: str
) -> None:
    """
    Dumps the specified algorithm model as a YAML file to the specified directory with
    the specified file name.

    Args:
        - current_model: An Algorithm object representing the model to be dumped.
        - curr_output: A string representing the file name of the output file.
        - directory: A string representing the directory where the output file will be
                    saved.
        - output (str): String containing the name of the output directory.
        - tcase (str): String containing the current test case number.

    Returns:
        None. The function only writes the YAML file to disk.

    Raises:
        OSError: If the output file cannot be opened or written to.
    """
    path_testCompare = join(output, tcase)
    os.makedirs(path_testCompare, exist_ok=True)

    with open(join(path_testCompare, curr_file), "w") as f:
        yaml.dump(current_model.dict(), f, sort_keys=False)
