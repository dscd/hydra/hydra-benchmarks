#!/usr/bin/env python
# -*- coding: utf-8 -*-
# -----------------------------------------------------------
# Hydra Benchmarks: generate_ground_truth.py
#
# (C) 2022 Statistics Canada
# Author: Saptarshi Dutta Gupta
# ---------------

import glob
import logging
import os
import re
from typing import List

import fitz
import typer
import yaml
from dsd.hydra.benchmarks.blinding.blinding_config import (
    BlindingAnnotation,
    FileTypes,
    Labels,
)
from dsd.hydra.benchmarks.blinding.utils import (
    get_target_df,
    read_document,
    save_yaml_file,
)
from dsd.hydra.benchmarks.config.api import load_config
from dsd.hydra.benchmarks.main import download
from dsd.hydra.benchmarks.models.schemas import (
    FileInfo,
    GroundTruth,
    PageInfo,
    RegionInfo,
)

logger = logging.getLogger(__name__)

app = typer.Typer()

config = load_config()


def get_region_info(
    page: fitz.fitz.Page,
    page_number: int,
    filtered_text: List[str],
    filtered_entities: List[str],
) -> List[RegionInfo]:
    """
    Function that stores every region information per page
    Argument(s):
        - page(fitz.fitz.Page): page to be processed
        - page_number (int)
        - filtered_text (List[str]): list of target text for a page
        - filtered_entities (List[str]): list of target entities for a page
    Returns
        - all_region_info(List[RegionInfo])
    """

    # Create an empty list to store all the region information
    all_region_info: List[RegionInfo] = []

    # Loop through all the text blocks in the page
    for block in page.get_text("dict")["blocks"]:

        # If the block doesn't have any lines, skip to the next block
        if "lines" not in block.keys():
            continue

        # Loop through all the lines in the block
        for lines in block["lines"]:

            # Loop through all the text spans in the line
            for span in lines["spans"]:

                # If the span has no text content, skip to the next span
                if span["text"].strip():

                    # Search for all the filtered text in the span's content
                    match_found = [
                        re.search(re.escape(target), span["text"])
                        for target in filtered_text[0]
                    ]

                    # Get the indices of all the matched targets
                    where_found = [
                        index
                        for index, bvalue in enumerate(match_found)
                        if bvalue is not None
                    ]

                    # Loop through all the matched targets and their coordinates
                    for index in where_found:
                        target_text = match_found[index].group(0)
                        pii_coordinates = page.search_for(
                            target_text, clip=span["bbox"]
                        )

                        # Create a RegionInfo object for each
                        # matched target and append it to the list
                        for coordinates in pii_coordinates:
                            region_info = RegionInfo(
                                coordinates=tuple(coordinates), text=target_text
                            )
                            all_region_info.append(region_info)

    # Return the list of all the extracted region information
    return all_region_info


def process_files(dataset: str, version: str, subset: str) -> GroundTruth:
    """
    Function to save the GroundTruth pydantic model
    Argument(s):
        - dataset(str): name of the HYDRA module to be used
        - version (str): version of the dataset to be used
        - subset (str): subset to be used
    Returns:
        - GroundTruth
    """

    # Set the path to the benchmark dataset directory
    benchmark_dataset_directory = os.path.abspath(
        os.path.join(
            config.cache_dir,
            config.root_dataset,
            dataset,
            version,
            subset,
            config.dt_dir,
        )
    )

    # Check if the directory exists
    if not os.path.exists(benchmark_dataset_directory):
        logger.error(f"Path {benchmark_dataset_directory} not found")
        raise typer.Exit()

    # Log that the processing of files in the directory has started
    logger.info(f"Started processing files in {benchmark_dataset_directory}")

    # Create an empty list to store information for all files in the directory
    all_file_info: List[FileInfo] = []

    # Loop through all directories in the benchmark dataset directory
    for dataset_dir in os.listdir(benchmark_dataset_directory):

        # Set the path to the current directory
        dataset_dir_path = os.path.join(benchmark_dataset_directory, dataset_dir)

        # Get the path to the raw pdf file inside the current directory
        try:
            raw_pdf_path = glob.glob(
                os.path.join(dataset_dir_path, f"*{FileTypes.pdf.value}")
            )[0]
        except IndexError:
            logger.error(f"PDF not found inside {dataset_dir_path}")
            raise typer.Exit()

        # Get the path to the annotations file inside the current directory
        try:
            annotations_path = glob.glob(
                os.path.join(dataset_dir_path, f"*{FileTypes.yml.value}")
            )[0]
        except IndexError:
            logger.error(f"YAML file not found inside {dataset_dir_path}")
            typer.Exit()

        # Load the annotations file and create a BlindingAnnotation object
        annotations = BlindingAnnotation(
            **dict(yaml.load(open(annotations_path), yaml.Loader))
        )

        # Get the target dataframe from the entities in the annotations
        target_df = get_target_df(annotations.entities)

        # Read the raw pdf file and create PageInfo objects for each page in the file
        resume_original = read_document(raw_pdf_path)
        all_page_info: List[PageInfo] = []
        for index, page in enumerate(resume_original):

            # Filter the target dataframe to only include entities on the current page
            target_df_filter_by_page = target_df[target_df[Labels.page.value] == index]
            filtered_text = target_df_filter_by_page[
                Labels.target.value
            ].values.tolist()
            filtered_entities = target_df_filter_by_page[
                Labels.entity_type.value
            ].values.tolist()

            # If there are entities on the current page,
            # get RegionInfo objects for each entity
            if not target_df_filter_by_page.empty:
                regions_info: List[RegionInfo] = get_region_info(
                    page, index, filtered_text, filtered_entities
                )
            else:
                regions_info = []

            # Create a PageInfo object for the current page
            page_info: PageInfo = PageInfo(page=index, regions=regions_info)
            all_page_info.append(page_info)

        # Set the filename for the current file
        filename = dataset_dir + "." + FileTypes.pdf.value

        # Create a FileInfo object for the current file
        file_info: FileInfo = FileInfo(filename=filename, pages=all_page_info)
        all_file_info.append(file_info)

    ground_truth: GroundTruth = GroundTruth(
        dataset=dataset, version=version, subset=subset, files=all_file_info
    )
    return ground_truth


@app.command()
def generate(
    dataset: str = typer.Argument(..., help="name of the hydra module"),
    version: str = typer.Argument(..., help="version of the dataset to be used"),
    subsets: List[str] = typer.Argument(..., help="subset of the dataset to be used"),
    out_dir: str = typer.Argument(
        ..., help="directory where predictions will be stored"
    ),
) -> None:
    """
    Function to generate ground truth file for the given dataset
    Argument(s):
        - dataset(str): name of the HYDRA module to be used
        - version (str): version of the dataset to be used
        - subset (str): subset to be used
        - out_dir (str): directory where predictions will be stored
    Returns:
        - None
    """

    # Download the subsets of the dataset having a specified version
    download(dataset, version, subsets, label=False, data=True)

    # Loop through the subsets
    for subset in subsets:
        logger.info(f"Processing subset:{subset}")
        # Process each subset
        gt_output: GroundTruth = process_files(dataset, version, subset)
        logger.info("Finished processing subset")

        # Create output directory if it does not exist
        out_dir_path = os.path.abspath(out_dir)
        if not os.path.exists(out_dir_path):
            os.mkdir(out_dir_path)

        # Create a complete path for saving the ground truth
        gt_filename = config.gt_file
        save_path = os.path.join(out_dir_path, gt_filename)

        # Save the ground truth file
        save_yaml_file(gt_output, save_path)
        logger.info(f"Successfully dumped {gt_filename} at {out_dir_path}")


if __name__ == "__main__":
    app()
