Description:
The objective of this specific unit test is to ensure that the function analyze_prediction in main.py performs accurately when called upon to process a typical file input via the OCR module.
The Ground Truth has 1 file with all values of metrics populated. The prediction contains 1 file
with an empty structure. The expected output should be 1 file where FN should be 0 and all values in metrics should be 0 except CER.CER is 1.0.
