#!/usr/bin/env python
# -*- coding: utf-8 -*-
# -----------------------------------------------------------
# Hydra benchmarks: download/utils.py
#
# (C) 2022 Statistics Canada
# Author: Saptarshi Dutta Gupta
# -----------------------------------------------------------

import logging
import os
from pathlib import Path
from typing import List

import typer
from dsd.hydra.benchmarks.config.api import S3Location, load_config
from dsd.hydra.benchmarks.models.schemas import Datasets
from dsd.hydra.benchmarks.modules.s3client import S3Client
from tqdm.auto import tqdm

logger = logging.getLogger(__name__)


class Download:
    def __init__(self, dataset: str, subsets: List[str], version: str):
        self.config = load_config()
        self.dataset = dataset
        self.subsets = subsets
        self.version = version

        if self.dataset.lower() == Datasets.ocr.value:
            self.bucket = self.config.bucket
            self.prefix = os.path.join(self.config.root_dataset, Datasets.ocr.value, "")
        elif self.dataset.lower() == Datasets.blinding.value:
            self.bucket = self.config.bucket
            self.prefix = os.path.join(
                self.config.root_dataset, Datasets.blinding.value, ""
            )
        elif self.dataset.lower() == Datasets.tabular.value:
            self.bucket = self.config.bucket
            self.prefix = os.path.join(
                self.config.root_dataset, Datasets.tabular.value, ""
            )

        # Instantiate MinIO Client
        self.client = S3Client.get_client()

    def create_local_directory_structure(self) -> None:
        """
        A method to create the local directory structure
        """
        for subset in self.subsets:
            modified_prefix = os.path.join(self.prefix, self.version, subset, "")
            objects = self.client.list_objects(
                self.bucket, prefix=modified_prefix, recursive=False
            )
            try:
                next(objects)
            except StopIteration:
                logger.error(
                    f"Path not found: Given path {modified_prefix} does not\
 exist. Aborting directory creation."
                )
                raise typer.Abort()
            for directory in tqdm(
                self.client.ls_objs(
                    bucket=self.bucket, prefix=modified_prefix, recursive=False
                ),
                desc="Creating directories",
            ):
                local_path = Path(
                    os.path.join(self.config.cache_dir, directory.object_name)
                )
                local_path.mkdir(parents=True, exist_ok=True)

    def download_dataset(self) -> None:
        """
        A method to download all the files from MinIO
        """

        # loop through list of subsets
        for subset in self.subsets:
            # Setup subset prefix minio location
            modified_prefix = os.path.join(self.prefix, self.version, subset, "")
            # Download files from subset to local directory
            for file in tqdm(
                self.client.ls_objs(
                    bucket=self.bucket,
                    prefix=modified_prefix,
                    recursive=True,
                    only_files=True,
                    default_folder_file=False,
                ),
                desc="Subset file extraction",
                initial=1,
            ):

                local_path = os.path.join(self.config.cache_dir, file.object_name)
                basename = os.path.relpath(Path(file.object_name), modified_prefix)
                file_minio_location = S3Location(self.bucket, modified_prefix, basename)
                logger.info(f"\nDownloading from MinIO path: {file_minio_location}")
                logger.info(f"\nDownloading to local path: {local_path}")
                self.client.fget_loc(file_minio_location, local_path)

    def download_ground_truth(self) -> None:
        """
        A method to download the ground truth file from a subset on MinIO
        """

        for subset in self.subsets:

            # Setup subset prefix minio location
            local_dir = os.path.join(
                self.config.cache_dir,
                self.prefix,
                self.version,
                subset,
                self.config.gt_dir,
            )
            if not os.path.exists(local_dir):
                local_dir = Path(local_dir)
                local_dir.mkdir(parents=True, exist_ok=True)

            file_local_path = os.path.join(local_dir, self.config.gt_file)

            modified_prefix = os.path.join(
                self.prefix, self.version, subset, self.config.gt_dir
            )
            file_minio_location = S3Location(
                self.bucket, modified_prefix, self.config.gt_file
            )
            logger.info(f"\nDownloading from MinIO path: {file_minio_location}")
            logger.info(f"\nDownloading to local path: {file_local_path}")
            self.client.fget_loc(file_minio_location, file_local_path)

    def download_data_files(self) -> None:
        """
        A method to download all the data files from a subset on MinIO
        """

        # Setup subset prefix minio location
        local_dir = os.path.join(self.config.cache_dir)
        if not os.path.exists(local_dir):
            local_path = Path(local_dir)
            local_path.mkdir(parents=True, exist_ok=True)

        for subset in self.subsets:
            # Setup subset prefix minio location
            modified_prefix = os.path.join(
                self.prefix, self.version, subset, self.config.dt_dir
            )
            # Download files from subset to local directory
            for file in tqdm(
                self.client.ls_objs(
                    bucket=self.bucket,
                    prefix=modified_prefix,
                    recursive=True,
                    only_files=True,
                    default_folder_file=False,
                ),
                desc="Subset file extraction",
                initial=1,
            ):

                local_path = os.path.join(self.config.cache_dir, file.object_name)
                basename = os.path.relpath(Path(file.object_name), modified_prefix)
                file_minio_location = S3Location(self.bucket, modified_prefix, basename)
                logger.info(f"\nDownloading from MinIO path: {file_minio_location}")
                logger.info(f"\nDownloading to local path: {local_path}")
                self.client.fget_loc(file_minio_location, local_path)
