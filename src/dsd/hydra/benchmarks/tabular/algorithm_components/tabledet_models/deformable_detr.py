"""Deformable Detr Wrapper class to hold the
   DeformableDetr single and two stage Table Detection models


@authors : Johan Fernandes, Niranjan Ramesh
"""

from typing import List


from dsd.hydra.benchmarks.models.schemas import RegionInfo, PageInfo, TabularConfig
from dsd.hydra.benchmarks.tabular.algorithm_components.tabledet_models.init import (
    TableDet,
)
from dsd.hydra.benchmarks.tabular.models.tabular_schemas import BatchData
from mmdet.apis import init_detector, inference_detector


class DeformableDETR(TableDet):
    """
    Returns Wrapper class for DeformableDETR model

    Arguments:
        config (str): Model config path
        weights (str): Model weights path
        device (str): Device to run the model on (cpu / cuda)

    Returns:
        DeformableDETR: Wrapper for DeformableDETR using mmdetect
    """

    def __init__(self, model_config: str, weights: str, config: TabularConfig) -> None:
        self.model = init_detector(model_config, weights, device=config.device)

    def get_result(
        self,
        batch: BatchData,
    ) -> List[PageInfo]:
        """Returns list of PageInfo object with the table locations of each image in the batch

        Arguments:
            batch (BatchData): List of
                - images : List
                - filenames : List
                - dims : List[(height, width)]
                - pages : List
                Each list is generated from a list of DocPage

        Returns:
            List[PageInfo] : List of RegionInfo models
        """

        # Get the images out
        # images = [x.img for x in image_list]
        results = inference_detector(self.model, batch.images)

        # Holds the list of pages.
        # Each page will contain a list of regions at the end of the loop
        pages: List[PageInfo] = []

        for idx, result in enumerate(results):
            # results[0] --> bbox results
            #     results[0][0] --> All table class detections in bbox format
            # results[1] --> mask results
            #     results[1][0] --> All table class detections in mask format
            # Results are sorted based on y1 values to show the tables from top to
            # bottom of the page
            tables = sorted(result[0][0], key=lambda x: x[1])

            regions = list(
                map(
                    lambda table: RegionInfo(
                        coordinates=[
                            float(table[0]),
                            float(table[1]),
                            float(table[2]),
                            float(table[3]),
                        ],
                        confidence=float(table[4]),
                    ),
                    tables,
                )
            )

            page = PageInfo(
                # Pick the page number from the collated batch
                page=batch.pages[idx],
                # List of regions where each region is a detected table
                regions=regions,
            )

            pages.append(page)

        return pages
