"""Table Text extraction pipeline
   Built to detect and extract table text from images


@authors : Johan Fernandes, Niranjan Ramesh
"""

import time
import copy
from tqdm import tqdm
from typing import List

from dsd.hydra.benchmarks.models.schemas import FileInfo, PageInfo, TabularConfig
from dsd.hydra.benchmarks.tabular.algorithm_components.extraction.init import (
    ImageExtract,
)
from dsd.hydra.benchmarks.tabular.algorithm_components.extraction.utils import (
    check_if_content_in_table,
)
from dsd.hydra.benchmarks.tabular.algorithm_components.layout.init import TableLayout
from dsd.hydra.benchmarks.tabular.algorithm_components.tabledet_models.init import (
    TableDet,
)


def get_file_results(
    tabular_dataloader,
    model: TableDet,
    extraction_class: ImageExtract,
    layout_class: TableLayout,
    data_dir,
    config: TabularConfig,
) -> List[FileInfo]:
    """
    Returns list of FileInfo containing detected regions and corresponding output text
    to the dataset of images

    Arguments:
        tabular_dataloader (Dataloader): Dataloader object for batching all
            images of the dataset
        model (CascadeTabNet/DeformableDETR/TableTransformer): Machine learning
            Model object that detects table regions in images
        extraction_class (PmpImage/OcrImage): The extraction class to be used
        layout_class (SLICE): The utility to use for arranging content into table cells
        data_dir: Path to source directory of the datasets
        config (TabularConfig): Configurations for table detection and extraction
            algorithms

    Returns
        List[FileInfo]: List of FileInfo model containing information of pages
            and regions of a PDF
    """
    # Stores all the output for all regions of all files in the subset
    file_results = []

    for batch_idx, batch in enumerate(tqdm(tabular_dataloader)):
        # Batch will contain an image in np.ndarray or PIL format

        # Get Table Detection output
        # The  batch here will be of dimension [batch_size, [batch no. of images],
        #                                                   [batch no. of labels]]
        start_time = time.time()
        batch_outputs: List[PageInfo] = model.get_result(batch)
        end_time = time.time()
        time_per_img = (end_time - start_time) / len(batch_outputs)

        # loop over outputs for each file.
        # if an output contains results for two images this loop will run twice.
        for output_id, page in enumerate(batch_outputs):

            start_time = time.time()
            filename = batch.filenames[output_id]

            image = batch.images[output_id]
            dims = batch.dims[output_id]

            page_content = None

            # OCRmyPDF doesn't work with PIL or np.ndarray.
            # Need to provide an actual image file
            if not extraction_class.allows_array:
                page_content = extraction_class.extract_text(
                    dims=dims, filename=filename, data_dir=data_dir, config=config
                )

            for rid, region in enumerate(page.regions):
                table_bbox = copy.deepcopy(region.coordinates)

                if extraction_class.allows_array:
                    table_img = image[
                        int(table_bbox[1]) : int(table_bbox[3]),
                        int(table_bbox[0]) : int(table_bbox[2]),
                    ]
                    table_img_height, table_img_width, _ = table_img.shape
                    extracted_content = extraction_class.extract_text(
                        image=table_img,
                        dims=(table_img_height, table_img_width),
                        table_bbox=table_bbox,
                    )
                else:
                    extracted_content = check_if_content_in_table(
                        table_bbox, config.overlap_thr, page_content
                    )

                table_str = layout_class.slice_table(
                    table_bbox=table_bbox, document_content=extracted_content
                )
                region.text = table_str

            # ----------------- End here --------------------------------

            end_time = time.time()
            file_info = FileInfo(
                filename=filename,
                pages=[page],
                exec_time=time_per_img + (end_time - start_time),
            )

            # Save the results for this processed file
            file_results.append(file_info)

    return file_results
