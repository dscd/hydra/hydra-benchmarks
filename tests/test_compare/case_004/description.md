Description:
The objective of this specific unit test is to ensure that the function analyze_prediction in main.py performs accurately when called upon to process a typical file input via the tabular module.
The Ground Truth File contains 1 file and the region is modified to be taken as [] whereas the prediction file has 1 file but the region is populated. In this case the expected output should contain 1 file with false positive as 1.
