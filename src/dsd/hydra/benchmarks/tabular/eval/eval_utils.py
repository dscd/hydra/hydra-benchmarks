"""Functions to evaluate bounding box predictions
   1. overlap_percentage : calculate the overlap between
      two bounding boxes. The share of overlap between
      each bbox is provided as separate values
   2. bbox_iou : calculate the iou between two bboxes

@authors: Johan Fernandes, Niranjan Ramesh
"""


from typing import List


def overlap_percentage(box1: List, box2: List):
    """Calculate overlap between two bounding boxes

    :param box1: list of location in [x1, y1, x2, y2] format
    :type box1: List
    :param box2: list of location in [x1, y1, x2, y2] format
    :type box2: List

    :return: Tuple of overlap of box1 and overlap of box 2
    :rtype: Tuple(float, float)

    """

    x1 = max(box1[0], box2[0])
    y1 = max(box1[1], box2[1])
    x2 = min(box1[2], box2[2])
    y2 = min(box1[3], box2[3])

    width = x2 - x1
    height = y2 - y1
    intersection_area = 0
    if width >= 0 and height >= 0:
        intersection_area = width * height

    if intersection_area == 0:
        return 0, 0

    box1_area = abs((box1[2] - box1[0]) * (box1[3] - box1[1]))
    box2_area = abs((box2[2] - box2[0]) * (box2[3] - box2[1]))

    overlap_box1 = float(intersection_area / box1_area)
    overlap_box2 = float(intersection_area / box2_area)

    return overlap_box1, overlap_box2


def bbox_iou(box1, box2):
    """Calculate the IOU between two bounding boxes

    :param box1: bounding box of 4 co-ordinate values. (x1, y1, x2, y2) format
    :type img_id: List[float|int]
    :param box2: bounding box of 4 co-ordinate values. (x1, y1, x2, y2) format
    :type img_id: List[float|int]

    :return: IOU value between the two boxes
    :rtype: float
    """

    x1 = max(box1[0], box2[0])
    y1 = max(box1[1], box2[1])
    x2 = min(box1[2], box2[2])
    y2 = min(box1[3], box2[3])

    width = x2 - x1
    height = y2 - y1
    intersection_area = width * height
    if intersection_area <= 0:
        return 0

    box1_area = abs((box1[2] - box1[0]) * (box1[3] - box1[1]))
    box2_area = abs((box2[2] - box2[0]) * (box2[3] - box2[1]))

    iou = intersection_area / float(box1_area + box2_area - intersection_area)

    return iou
