from typing import List

import pytesseract
from dsd.hydra.benchmarks.models.schemas import RegionInfo


class TessHelper:
    """
    This a class built on top of the Tesseract OCR for ease of use.
    It parses the image to the Tesseract engine and it returns the OCR output.
    """

    def __init__(self, custom_config: str = "--oem 3 --psm 6"):
        """
        This method is used to initialize the Tesseract OCR
        with the custom configuration. In case a configuration is not selected,
        the default for oem and psm is used.

        Arguments:
            - None

        Returns:
            - None
        """
        self.custom_config = custom_config

    def get_regions(self, img) -> List:
        """
        This method takes a Pillow image as input and returns a list of Region objects.

        Arguments:
            - img (Image.Image): A Pillow image of what is to be extracted

        Returns:
            - (List): List of Region objects
        """
        # Tesseract's function to get both image and data is called and saved
        # as dataframe
        # details consist of position, confidence, page_num, etc.
        output = pytesseract.image_to_data(
            img, config=self.custom_config, output_type="data.frame"
        )

        # defining an empty list to store all regions
        # each block of text detected by the ocr is a region
        regions = []

        # The output with confidence of -1 is removed
        # (those values also had high height and width values with text being Nan)
        # after that the coodrinates are setup to have them in the form of
        # (x1,y1,x2,y2) later
        # Removed the \n which tesseract ocr adds in the predicted text.
        output = output[output["conf"] > -1]
        output["width"] = output["left"] + output["width"]
        output["height"] = output["top"] + output["height"]
        output["text"] = output["text"].astype("str")
        output["text"] = output["text"].str.strip()

        # The columms needed for the region model are selected
        # lis of the values is generated
        cols = ["left", "top", "width", "height", "text"]
        region = output[cols].values.tolist()

        # for each region, an RegionInfo model is created and added to the
        # regions list
        for val in region:
            coords = tuple(val[:4])
            text = val[-1]
            image_region = RegionInfo(coordinates=coords, text=text)
            regions.append(image_region)

        return regions
