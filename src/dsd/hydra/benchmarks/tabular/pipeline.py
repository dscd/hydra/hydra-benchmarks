"""Table Text extraction pipeline
   Built to detect and extract table text from images


@authors : Johan Fernandes, Niranjan Ramesh
"""

import os
import yaml
from os.path import join
import mimetypes
from typing import List

from dsd.hydra.benchmarks.models.schemas import (
    HydraConfig,
    Prediction,
    FileInfo,
    Datasets,
)

from dsd.hydra.benchmarks.tabular.algorithm_components.tabledet_models import (
    init_models,
)
from dsd.hydra.benchmarks.tabular.algorithm_components.extraction.extract import (
    init_extract,
    init_ocr_extract,
)
from dsd.hydra.benchmarks.tabular.algorithm_components.layout.table_layout import (
    init_layout,
)
from dsd.hydra.benchmarks.tabular.utils import (
    download_model_data,
    get_algorithm,
)
from dsd.hydra.benchmarks.tabular.models.dataset import (
    build_image_dataset,
    build_image_dataloader,
    build_pdf_dataset,
    build_pdf_dataloader,
)
from dsd.hydra.benchmarks.tabular.models.tabular_schemas import Files
from dsd.hydra.benchmarks.tabular.modules.image_branch import (
    get_file_results as get_image_results,
)
from dsd.hydra.benchmarks.tabular.modules.pdf_branch import (
    get_file_results as get_pdf_results,
    determine_if_scanned,
)


def run_tabular_pipeline(  # noqa : C901
    algorithm_name: str,
    config: HydraConfig,
    input_dir: str,
    output_dir: str,
    dataset: str = Datasets.tabular.value,
    version: str = "Unavailable",
    subset: str = "Unavailable",
) -> None:
    """Table text extraction pipeline processes a set of images or PDF files
       in the provided subset folder.

    Arguments:
        - algorithm_name (str): Algorithm to use for the pipeline extraction tasks
        - config (str): Configurations for the pipeline
        - input_dir (str): Path to the directory containing input files to process
        - output_dir (str): Output directory to store results
        - version (str): Version of the dataset to be used example : v0.2.0
        - subset (str): Name of the subset to process
        - algorithm_name (str): Name of the algorithm to execute

    Returns:
        - None
    """

    # <Path of src folder>/cache/models/tabular/<version>/
    model_dir = join(
        config.cache_dir,
        config.model_dir,
        Datasets.tabular.value,
        config.modules_config.tabular.version,
    )

    # Provides the TabularAlgorithm class with all algorithm specific details
    algorithm = get_algorithm(
        cfg_file_path=config.modules_config.tabular.algorithms_config,
        algorithm_name=algorithm_name,
    )

    # Initialize Model
    model_status = True

    if os.path.exists(join(model_dir, algorithm.components.tabledet_model)) is False:
        model_status = download_model_data(
            bucket=config.bucket,
            local_data_dir=config.cache_dir,
            model_dir_prefix=join(
                config.model_dir,
                Datasets.tabular.value,
                config.modules_config.tabular.version,
                # model_info.version,
                algorithm.components.tabledet_model,
            ),
            model_weight_file=algorithm.components.tabledet_model_weight,
        )

    # If the model Download fails stop execution
    if not model_status:
        raise Exception("Unable to download model data")

    # Get tabular config
    tabular_config = config.modules_config.tabular

    # Get model configs and weights folder
    model_files = join(model_dir, algorithm.components.tabledet_model)

    model = init_models.init_table_det_model(
        model_files=model_files,
        algorithm_config=algorithm,
        tabular_config=tabular_config,
    )
    extraction_method = algorithm.components.extraction
    layout_class = init_layout(algorithm.components.layout, tabular_config)

    # Extracting list of image files and PDFs from the source directory
    filenames = os.listdir(input_dir)

    images_list, docs_list = [], []
    for filename in filenames:
        mtype = mimetypes.guess_type(join(input_dir, filename))
        if mtype[0] in config.doc_mimetypes:
            docs_list.append(filename)
        elif mtype[0] in config.img_mimetypes:
            images_list.append(filename)

    # Holds the extraction results for each file
    file_results: List[FileInfo] = []

    if len(images_list) != 0:

        image_dataset = build_image_dataset(fld_path=input_dir, images_list=images_list)
        image_dataloader = build_image_dataloader(
            image_dataset=image_dataset,
            batch_size=tabular_config.image_batch_size,
            num_workers=tabular_config.num_workers,
        )
        extraction_class = init_ocr_extract(extraction_method)
        file_results = get_image_results(
            tabular_dataloader=image_dataloader,
            model=model,
            extraction_class=extraction_class,
            layout_class=layout_class,
            data_dir=input_dir,
            config=tabular_config,
        )

    if len(docs_list) != 0:

        for doc in docs_list:
            print(f"PDF file to be processed {doc}")
            doc_filename = join(input_dir, doc)
            # :TODO determine type file
            # Intialize the dataset with the correct Extraction Class
            if determine_if_scanned(doc_filename):
                # Determine the OCR method init_extract()
                extraction_class = init_ocr_extract(extraction_method)
            else:
                extraction_class = init_extract(extraction_method, doc_filename)

            # Build the dataset for this PDF file
            pdf_dataset = build_pdf_dataset(extraction_class)

            pdf_dataloader = build_pdf_dataloader(
                pdf_dataset,
                batch_size=tabular_config.pdf_page_batch_size,
                num_workers=tabular_config.num_workers,
            )
            result = get_pdf_results(
                pdf_dataloader=pdf_dataloader,
                page_count=len(pdf_dataset),
                model=model,
                extraction_class=extraction_class,
                layout_class=layout_class,
                config=tabular_config,
            )
            file_results.append(result)

    # Load the model outputs to the pydantic model
    model_output = Prediction(
        algorithm_name=algorithm_name,
        dataset=dataset,
        version=version,
        subset=subset,
        files=file_results,
    )
    output_filename = Files.prediction_file.value.format(algorithm_name)

    # Create a result folder if there isn't one
    if os.path.exists(output_dir) is False:
        os.makedirs(output_dir)

    # Save the results to a yaml file
    with open(join(output_dir, output_filename), Files.write_mode.value) as f:
        yaml.dump(model_output.dict(), f, sort_keys=False)
