#!/usr/bin/env python
# coding: utf-8


from typing import List

import numpy as np
from dsd.hydra.benchmarks.blinding.blinding_config import Entities, Score
from presidio_analyzer import EntityRecognizer, RecognizerResult
from presidio_analyzer.nlp_engine import NlpArtifacts


class PRIRecognizer(EntityRecognizer):
    """
    Recognize PRI numbers using mod 11 checksum.
    """

    SCORE = Score.pri_threshold.value  # expected confidence level for this recognizer

    def load(self) -> None:
        """No loading is required."""

    def analyze(
        self, text: str, entities: List[str], nlp_artifacts: NlpArtifacts
    ) -> List[RecognizerResult]:
        """
        Analyzes text to find tokens which represent PRI numbers
        (exclusive to Statistics Canada).

        Args:
        :param text: Text to be analyzed
        :param entities: Entities this recognizer can detect
        :param nlp_artifacts: Output values from the NLP engine

        Returns:
        - results (RecognizerResult): Recognizer results
        """
        results = []

        # iterate over the spaCy tokens, and call `token.like_num`
        for token in nlp_artifacts.tokens:
            if len(token) == 8 and token.text.isdigit():
                text = token.text[0:-1]
                check_digit = self.get_mod_eleven_check_digit(text)
                if int(token.text[-1]) == check_digit:
                    result = RecognizerResult(
                        entity_type=Entities.pri.value,
                        start=token.idx,
                        end=token.idx + len(token),
                        score=self.SCORE,
                    )
                    results.append(result)
        return results

    @staticmethod
    def get_mod_eleven_check_digit(text: str) -> int:
        """
        Calculate mod eleven check digit
        Argument(s)
        text: number whose check digit needs to be calculated

        returns
        check_digit: calculated check digit
        """

        digit_array = [int(digit) for digit in str(text)]
        weights = np.arange(2, len(digit_array) + 2)[::-1]
        check_digit = 11 - np.dot(digit_array, weights) % 11
        return check_digit
