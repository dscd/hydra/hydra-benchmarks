Description:
The objective of this specific unit test is to ensure that the function analyze_prediction in main.py performs accurately when called upon to process a typical file input via the tabular module.
The Ground Truth File is modified to have duplicated correct GT and the Prediction File contains Duplicated correct Predictions. The expected output should have 4 files with 4 region counts.
