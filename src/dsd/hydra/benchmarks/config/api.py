#!/usr/bin/env python
# -*- coding: utf-8 -*-
# -----------------------------------------------------------
# Hydra Module Template Config
#
# (C) 2022 Statistics Canada
# Author: Andres Solis Montero
# -----------------------------------------------------------

import json
import os
from collections import namedtuple
from enum import Enum
from functools import lru_cache
from os.path import basename, dirname, exists, join, realpath

import yaml
from dsd.hydra.benchmarks.models.schemas import HydraConfig, OCRModels, Datasets
from pydantic import BaseModel, BaseSettings, Field

"""
S3 Locations relevant to our application.
"""
S3Location = namedtuple(
    "S3Location", ["bucket", "prefix", "basename"], defaults=["", "", ""]
)


class ConfigFile(Enum):
    """
    ConfigFile is an Enumerator which stores all the Configuration files
    Enum is the base class inherited by it.
    """

    environment = ".env"
    config = "config.yml"
    ocr_config = "ocr_algorithms.yml"
    aaw_minio = "/vault/secrets/minio-standard-tenant-1.json"


class LogLevel(Enum):
    """
    LogLevel is an Enumerator which mentions the supported log levels
    and stores the integer code for each of it.
    Enum is the base class inherited by it.
    """

    CRITICAL = 50
    ERROR = 40
    WARNING = 30
    INFO = 20
    DEBUG = 10


class LoggingConfig(BaseModel):
    """
    LoggingConfig is a Pydantic model which consists of settings
    to configure logging throughout the application.
    The pydantic Basemodel is inherited.
    """

    # Set the trace log to None to disable trace logging
    # trace_log_path: Optional[Path] = Path(f"{home}/pipeline.log")

    # how to format messages
    console_msg_format: str = "[%(levelname)s] [%(asctime)s] %(message)s"
    trace_msg_format: str = (
        "%(asctime)s|%(name)s|%(levelname)s|%(funcName)s|%(message)s"  # noqa E501
    )

    # Default level for messages
    console_level: LogLevel = LogLevel.INFO


class Settings(BaseSettings):
    """
    Settings is a Pydantic model which contains necesary enviroment
    variables needed for authentication.
    If these variables are not set, any file including this module
    will stop executing.
    BaseSettings pydanctic model is inherited.
    """

    HOST: str = Field(env="DASH_HOST", default="127.0.0.1")
    PORT: int = Field(env="DASH_PORT", default=8000)
    DEBUG: bool = Field(env="DASH_DEBUG", default=True)

    nb_prefix: str = Field(env="NB_PREFIX", default="")

    MINIO_URL: str
    MINIO_ACCESS_KEY: str
    MINIO_SECRET_KEY: str

    logging: LoggingConfig = LoggingConfig()

    @staticmethod
    @lru_cache()
    def load(cls) -> "Settings":
        return Settings()

    def root_path(self):
        """
        Provides uvicorn root path if necessary. Automatically checks
        if AAW NB_PREFIX variable is set to set the root path to aaw prefix path.
        If AAW NB_PREFIX isn't present, then the empty string will be returned.
        No path (i.e., empty string) is the default behaviour of root_path.

        Arguments:
            - Takes no arguments

        Returns:
            - (str) : returns path as a string.
        """
        return f"{self.nb_prefix}/proxy/{self.PORT}/" if self.nb_prefix else ""

    class Config:
        """
        Provides the path to a ".env" file containing a list of environments
        variables to be set in case that they don't exist yet.
        """

        env_file: str = ConfigFile.environment.value
        env_file_encoding: str = "utf-8"


def aaw_minio_credentials():
    """
    It reads the minio configurations file and loads the credential
    details as the operating system's environment variables.

    Arguments:
        - Takes no arguments

    Returns:
        - Returns nothing
    """

    if exists(ConfigFile.aaw_minio.value):
        with open(ConfigFile.aaw_minio.value) as f:
            data = json.load(f)
            for var, val in data.items():
                os.environ[var] = str(val)


settings = Settings()


class Factory(object):
    """
    Factory class to read configurations files only once.
    Inherits the object class.
    """

    @staticmethod
    @lru_cache()
    def s3locs() -> "Factory":
        """
        Static Factory method to load ConfigFile.s3Locations into python objs

        Arguments:
            - Takes no arguments

        Returns:
            - (nametuple) : returns a collection called as namedtuple
                            with s3 locations loaded in it.
        """
        with open(join(dirname(__file__), ConfigFile.s3locations.value)) as f:
            locs = json.load(f)

        loc = [(k, S3Location(**v)) for k, v in locs.items()]
        keys, values = zip(*loc)

        return namedtuple("S3Locs", keys, defaults=values)()


@lru_cache
def load_config() -> HydraConfig:
    """
    Configurations for all modules.

    Arguments:
        - Takes no arguments

    Returns:
        - (HydraConfig) : Single Pydantic model to hold
                        all configurations for each module
    """

    directory = dirname(realpath(__file__))
    config_filename = ConfigFile.config.value
    config_filepath = join(directory, config_filename)
    modules_cfg = HydraConfig(**yaml.load(open(config_filepath), yaml.Loader))

    return modules_cfg


@lru_cache
def load_ocr_algorithms() -> OCRModels:
    """
    Configurations for all modules.

    Arguments:
        - Takes no arguments

    Returns:
        - (OCRModels) : Single Pydantic model to hold
                        all ocr models configurations.
    """

    directory = dirname(realpath(__file__))
    ocr_config_filename = ConfigFile.ocr_config.value
    ocr_config_filepath = join(directory, ocr_config_filename)
    ocr_algorithms_cfg = OCRModels(**yaml.load(open(ocr_config_filepath), yaml.Loader))

    return ocr_algorithms_cfg


class S3:
    """
    Util functions to generate unique files following our folder
    structures and organization. The set of static methods here
    uses our S3Locations to generate the correct folder structure.

    !!! check
        **Developers should always use the `S3` proxy class instead
        of hard-coding S3 buckets, prefixes, or basenames.** This will
        significantly reduce the overhead associated with remembering
        naming conventions and also substantially reduce the probability
        of making an error when hard-coding path names.

    """

    OCR = os.path.join(load_config().root_dataset, Datasets.ocr.value, "")

    BLINDING = os.path.join(load_config().root_dataset, Datasets.blinding.value, "")
    TABULAR = os.path.join(load_config().root_dataset, Datasets.tabular.value, "")
    CACHE = load_config().cache_dir

    @staticmethod
    def obj_to_loc(bucket: str, obj: str) -> S3Location:
        """
        **Description:**

        A helper method that is used to translate between the S3 `obj` type
        and our project-specific `S3Location` type. This is the inverse method
        to `S3.loc_to_obj`.

        **Example:**
            ```
            # List all the objects from the loc S3location
            objs = client.ls_objs(
                loc.bucket,
                recursive=True,
                prefix=loc.prefix,
                only_files=True,
                default_folder_file=False,
            )

            # Recode all objs to S3Location instances
            s3locs = [ S3.obj_to_loc(loc.bucket, obj.object_name) for obj in objs ]
            ```

        Args:
            bucket: str
                The name of the S3 bucket where the object is located.
            obj: str
                The `obj` string returned by an S3 client.

        Returns:
            S3Location:
                The instance of `S3Location` that corresponds to the given
                `bucket` and `obj` strings passed.
        """
        return S3Location(bucket, join(dirname(obj), ""), basename(obj))

    @staticmethod
    def loc_to_obj(location: S3Location) -> str:
        """
        **Description:**

        A helper method that is used to translate between  our project-specific
        `S3Location` type and the S3 `obj` type and. This is the inverse method
        to `S3.obj_to_loc`. While translating from a S3Location to obj, the bucket
        name is lost. The returned string is the concatenation of
        S3Location.prefix + S3Location.basename.

        **Example:**
            ```
            path =  S3.loc_to_obj(s3loc)
            print(s3loc.bucket, path)
            ```

        Args:
            location: S3Location
                An instance of our project-specific `S3Location` where
                we want to get the corresponding path as a string.

        Returns:
            str
                An `obj` string that can be used in an S3 client or other
                use case that requires a path as a string.
        """
        return join(location.prefix, location.basename)
