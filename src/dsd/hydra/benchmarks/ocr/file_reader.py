import tempfile
from mimetypes import MimeTypes
from os.path import basename, join

import fitz
from dsd.hydra.benchmarks.config.api import load_config
from PIL import Image


class FileReader:
    """This a class built on top of fitz and Pillow for ease of use.
    It parses and opens a file (pdf, image).
    """

    def __init__(self, file_path: str):
        """
        This initializes for the FileReader Class
        Arguments:
            - file_path (str): A path to the file

        Returns:
            - None

        """
        self.mime = MimeTypes()
        self.path = file_path
        self.temp_file_name = basename(file_path)
        self.config = load_config()

    def reader(self, page_no: int):
        """
        This method opens the page of the file and
        returns the page as an image

        Arguments:
            - page_no (int): The page number

        Returns:
            - Image

        """
        file_type = self.mime.guess_type(self.path)[0]
        if file_type in self.config.doc_mimetypes:

            with tempfile.TemporaryDirectory() as tmpdirname:

                temp_file_location = join(tmpdirname, self.temp_file_name)

                doc = fitz.open(self.path)
                page = doc.load_page(page_no)
                img = page.get_pixmap()
                img.save(temp_file_location)
                image = Image.open(temp_file_location)

        elif file_type in self.config.img_mimetypes:
            image = Image.open(self.path)

        else:
            image = None

        return image
