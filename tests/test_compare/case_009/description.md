Description:
The objective of this specific unit test is to ensure that the function analyze_prediction in main.py performs accurately when called upon to process a typical file input via the tabular module.The Ground Truth File contains 2 files with Unique GT (False positive)and Prediction File
contains 2 files with Unique incorrect Pred (False positive). The output expected should have 2 files and only False positives.
