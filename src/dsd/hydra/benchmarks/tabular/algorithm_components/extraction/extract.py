from dsd.hydra.benchmarks.tabular.algorithm_components.extraction.pymupdf import (
    PMPDF,
    PmpImage,
)
from dsd.hydra.benchmarks.tabular.algorithm_components.extraction.ocrmypdf import (
    OcrImage,
)
from dsd.hydra.benchmarks.tabular.models.tabular_schemas import ExtractionMethod


def init_extract(extraction_method: str, doc_filename: str):
    """Returns extraction class for PDF files

    Arguments:
        extraction_method (str) : Module to use for text extraction
        doc_filename (str) : Name of the document to be extracted

    Returns:
        PMPDF : PDF text extraction class
    """

    extraction_class = 0
    if extraction_method == ExtractionMethod.pymupdf.value:
        extraction_class = PMPDF(doc_filename)

    elif extraction_method == ExtractionMethod.ocrmypdf.value:
        pass
    elif extraction_method == ExtractionMethod.pytesseract.value:
        pass
    elif extraction_method == ExtractionMethod.pdfplumber.value:
        pass

    return extraction_class


def init_ocr_extract(extraction_method: str):
    """Returns image to PDF OCR utility class

    Arguments:
        extraction_method (str) : Module to use for OCR

    Returns:
        ImageExtract : Any implementation of ImageExtract class
    """
    extraction_class = 0
    if extraction_method == ExtractionMethod.pymupdf.value:
        extraction_class = PmpImage()
    elif extraction_method == ExtractionMethod.ocrmypdf.value:
        extraction_class = OcrImage(allows_array=False)
    elif extraction_method == ExtractionMethod.pytesseract.value:
        pass
    elif extraction_method == ExtractionMethod.pdfplumber.value:
        pass
    return extraction_class
