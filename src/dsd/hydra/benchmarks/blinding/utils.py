#!/usr/bin/env python
# -*- coding: utf-8 -*-
# -----------------------------------------------------------
# Hydra Benchmarks: utils.py
#
# (C) 2022 Statistics Canada
# Author: Saptarshi Dutta Gupta
# ---------------


import logging
import sys
from pathlib import Path
from typing import Dict, List, Union

import fitz
import pandas as pd
import yaml
from dsd.hydra.benchmarks.blinding.blinding_config import (
    AlgorithmName,
    Labels,
    LanguageCode,
    PageAnnotation,
    PresidioConfig,
    PresidioConfigLabel,
)
from dsd.hydra.benchmarks.config.blinding_algorithms import ModelNames
from dsd.hydra.benchmarks.config.api import load_config
from presidio_analyzer import AnalyzerEngine, EntityRecognizer, RecognizerRegistry
from presidio_analyzer.nlp_engine import NlpEngineProvider

config = load_config()

logger = logging.getLogger(__name__)


def get_target_df(entities: List[PageAnnotation]) -> pd.DataFrame:
    """
    Function to process the annotation dataframe

    Argument(s):
        - entities(List[PageAnnotations])

    Returns
        - pd.DataFrame: page wise list of targets, entities and entity types
    """
    # create a list of dictionaries for each entity, containing the page and targets
    entity_dict_list = [
        {Labels.page.value: entity.page, Labels.targets.value: entity.targets}
        for entity in entities
    ]

    # create a pandas DataFrame from the list of dictionaries
    target_df = pd.DataFrame(entity_dict_list)

    # create a new column in the DataFrame for the target text of each entity
    target_df[Labels.target.value] = target_df[Labels.targets.value].apply(
        lambda group: [element.text for element in group]
    )

    # create a new column in the DataFrame for the entity type of each entity
    target_df[Labels.entity_type.value] = target_df[Labels.targets.value].apply(
        lambda group: [element.entity for element in group]
    )

    # return the final DataFrame with columns
    # for page, targets, target text, and entity type
    return target_df


def read_document(filename: Path) -> fitz.fitz.Document:
    """
    Function to read PDF document
    Argument(s):
        - filename(Path): path to file

    Returns:
        - fitz.fitz.Document: document in fitz format
    """
    logger.info(f"Reading document: {filename}")
    document = fitz.open(filename)
    logger.info(f"Finished reading document: {filename}")
    return document


def read_yml_file(input: Path) -> Dict:
    """
    Function to read YAML file
    Argument(s):
        - input (Path): path to file

    Returns:
        - Dict: input parsed by YAML
    """
    logging.info(f"Reading YAML file: {input}")

    # Read and load an YAML file
    with open(input) as file:
        config = yaml.load(file, Loader=yaml.Loader)

    logging.info("Finished reading YAML file")
    return config


def save_yaml_file(object, save_path: Path) -> None:
    """
    Function to save YAML file
    Argument(s):
        - object: object to be saved
        - save_path (Path): path where yaml object is to be saved
    Returns:
        - None
    """
    logging.info(f"Saving YAML file to: {save_path}")

    # Save an object to a YAML file
    with open(save_path, "w") as file:
        yaml.dump(object, file)

    logging.info("Finished saving YAML file")


def create_default_analyzer(recognizer: EntityRecognizer) -> AnalyzerEngine:
    """
    Function to create a presidio transformer analyzer
    Argument(s):
        - recognizer (EntityRecognizer)

    Returns:
        - AnalyzerEngine: Presidio analyzer object

    """
    # create a RecognizerRegistry object
    registry = RecognizerRegistry()

    # load predefined recognizers into the registry
    registry.load_predefined_recognizers()

    # add a custom recognizer to the registry
    registry.add_recognizer(recognizer)

    # create an AnalyzerEngine object using the recognizer registry
    analyzer = AnalyzerEngine(registry=registry)

    # return the AnalyzerEngine object for further use
    return analyzer


def create_custom_analyzer(
    configuration: Dict[str, Union[str, List[Dict[str, str]]]],
    supported_languages: List[str],
) -> AnalyzerEngine:
    """
    Function to create analyzer based on configurations provided
    Argument(s):
        - configuration (Dict): configuration containing engine name and models
        - supported_language (List[str]): List of supported languages

    Returns:
        - analyzer (AnalyzerEngine): Presidio analyzer object
    """
    # Create configuration containing engine name and models
    configuration = configuration

    # Create NLP engine based on configuration
    provider = NlpEngineProvider(nlp_configuration=configuration)
    nlp_engine = provider.create_engine()

    # Pass the created NLP engine and supported_languages to AnalyzerEngine
    analyzer = AnalyzerEngine(
        nlp_engine=nlp_engine,
        supported_languages=supported_languages,
    )

    return analyzer


def save_document(document: fitz.fitz.Document, file_path: Path) -> None:
    """
    Function to save a PDF document
    Argument(s):
        - document (fitz.fitz.Document): PDF document to be saved
        - file_path (Path): path where PDF is to be saved
    """
    logging.info(f"Saving PDF to {file_path}")
    document.save(file_path)


def setup_logger(log_config: Dict) -> None:
    """Configure both console and a rotating file log.

    Argument(s):
        - log_config(Dict): Settings for how to configure logger.

    Returns:
        - None
    """

    root_logger = logging.getLogger()
    root_logger.setLevel(logging.INFO)

    # Only create a trace log if there's a path specified.
    if log_config.trace_log_path is not None:
        # Create the directory to hold the logs if it doesn't exist.
        log_config.trace_log_path.parent.mkdir(parents=True, exist_ok=True)

        trace_log_fmt = logging.Formatter(log_config.trace_msg_format)
        trace_log_handler = logging.FileHandler(log_config.trace_log_path)
        trace_log_handler.setFormatter(trace_log_fmt)
        trace_log_handler.setLevel(logging.DEBUG)
        root_logger.addHandler(trace_log_handler)

    # Basic console logger
    console_log_fmt = logging.Formatter(log_config.console_msg_format)
    console_log_handler = logging.StreamHandler(sys.stdout)
    console_log_handler.setFormatter(console_log_fmt)
    console_log_handler.setLevel(log_config.console_level.value)
    root_logger.addHandler(console_log_handler)


def get_spacy_configurations() -> PresidioConfig:
    """
    Function to get spacy configurations
    Returns:
        - PresidioConfig
    """

    nlp_engine_name = AlgorithmName.spacy.value
    # create a list of dictionaries
    # representing the models to use with Presidio
    models = [
        {
            PresidioConfigLabel.lang_code.value: LanguageCode.english.value,
            PresidioConfigLabel.model_name.value: ModelNames.spacy.value,
        }
    ]
    # create a PresidioConfig object with the specified NLP engine and models
    config = PresidioConfig(nlp_engine_name=nlp_engine_name, models=models)

    return config.dict()


def get_stanza_configurations() -> PresidioConfig:
    """
    Function to get stanza configurations
    Returns:
        - PresidioConfig
    """
    nlp_engine_name = AlgorithmName.stanza.value
    # create a list of dictionaries
    # representing the models to use with Presidio
    models = [
        {
            PresidioConfigLabel.lang_code.value: LanguageCode.english.value,
            PresidioConfigLabel.model_name.value: ModelNames.stanza.value,
        }
    ]
    # create a PresidioConfig object with the specified NLP engine and models
    config = PresidioConfig(nlp_engine_name=nlp_engine_name, models=models)
    return config.dict()
