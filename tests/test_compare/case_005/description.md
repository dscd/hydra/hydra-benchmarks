Description:
The objective of this specific unit test is to ensure that the function analyze_prediction in main.py performs accurately when called upon to process a typical file input via the tabular module.
The Ground Truth File has Unique correct GT whereas the prediction file has been modified to have Duplicated correct Prediction. The expected output should have 2 duplicated files with 2 region counts.
