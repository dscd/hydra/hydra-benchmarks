"""Table Text extraction pipeline
   Built to detect and extract table text from a PDF file


@authors : Johan Fernandes, Niranjan Ramesh
"""

from os.path import basename
from typing import List
from tqdm import tqdm
import copy
import time
from torch.utils.data import DataLoader

from dsd.hydra.benchmarks.tabular.algorithm_components.extraction.utils import (
    check_if_content_in_table,
)
from dsd.hydra.benchmarks.tabular.algorithm_components.extraction.init import (
    ImageExtract,
)
from dsd.hydra.benchmarks.tabular.algorithm_components.layout.init import TableLayout
from dsd.hydra.benchmarks.models.schemas import FileInfo, PageInfo, TabularConfig
from dsd.hydra.benchmarks.tabular.models.tabular_schemas import ExtractedContent
from dsd.hydra.benchmarks.tabular.algorithm_components.tabledet_models.init import (
    TableDet,
)


def determine_if_scanned(doc_filename) -> bool:
    """
    Checks if the given document consists of scanned pages

    Arguments:
        doc_filename (str) : Name of the document

    Returns:
        bool: Boolean indicating if the document consists of scanned pages
    """
    return False


def get_file_results(
    pdf_dataloader: DataLoader,
    page_count: int,
    model: TableDet,
    extraction_class: ImageExtract,
    layout_class: TableLayout,
    config: TabularConfig,
) -> FileInfo:
    """
    Returns list of FileInfo containing detected regions and corresponding output text
    to the dataset of images

    Arguments:
        pdf_dataloader (Dataloader): Dataloader object for the PDF that batches images
            of every page in the PDF
        page_count (init): Number of pages in the PDF
        model (CascadeTabNet/DeformableDETR/TableTransformer): Machine learning Model
            object that detects table regions in images
        extraction_class (PMPDF/OCRIMAGE): Extraction utility for extracting word text
            and bounding boxes from PDF page
        layout_class (SLICE): The utility to use for arranging content into table cells
        config (TabularConfig): COnfigurations for table detection and extraction
            algorithms

    Returns
        FileInfo: FileInfo model containing information of pages and regions of a PDF
    """
    # Stores all the output for all regions of all files in the subset
    pages = []
    start_time = time.time()

    # Progress bar to show the processing of the file
    bar = tqdm(total=page_count)

    for batch in pdf_dataloader:
        # Batch will contain an image in np.ndarray or PIL format

        # Get Table Detection output
        # The  batch here will be of dimension [batch_size, BatchData]
        # Returns a list of pages based on the number of pages sent in BatchData
        batch_pages: List[PageInfo] = model.get_result(batch)

        # loop over outputs for each file.
        # if an output contains results for two images this loop will run twice.

        for output_id, page in enumerate(batch_pages):

            # OCRmyPDF doesn't work with this branch as the document contains all
            # required text.
            filename = batch.filenames[output_id]
            page_number = batch.pages[output_id]

            # Use the previously determined extraction class's get_result
            document_content: ExtractedContent = extraction_class.get_page_content(
                page_number=page_number
            )

            # Loop over the regions in each result to generate the layout
            # The loop updates the region with the correct table layout
            for region in page.regions:
                table_bbox = copy.deepcopy(region.coordinates)
                table_document_content = check_if_content_in_table(
                    table_bbox=table_bbox,
                    overlap_thr=config.overlap_thr,
                    document_content=document_content,
                )

                # Use SLICE for layout
                table_str = layout_class.slice_table(
                    table_bbox=table_bbox, document_content=table_document_content
                )

                # Update the region in this page
                region.text = table_str

                # ----------------- End here --------------------------------

        # Add the list of pages from this batch to the total pages of the file
        pages.extend(batch_pages)

        # Update the progress bar
        bar.update(len(batch.filenames))

    end_time = time.time()
    # Note: batch.filenames has [<path to file>/filename,...]
    # Hence the need to get basename
    file_info = FileInfo(
        filename=basename(filename), pages=pages, exec_time=(end_time - start_time)
    )

    # Close the PDF
    extraction_class.doc.close()

    return file_info
