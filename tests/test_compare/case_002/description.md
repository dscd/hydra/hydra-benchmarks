Description:
The objective of this specific unit test is to ensure that the function analyze_prediction in main.py performs accurately when called upon to process a typical file input via the tabular module.
The Ground Truth and the Prediction files contains 1 file and the Pages are modified to be []. The expected output should also contain 1 file with pages as [] and all the metrics present as 0.
