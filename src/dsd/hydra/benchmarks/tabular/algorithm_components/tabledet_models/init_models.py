"""Common Initialization function for all
   Table Detection models


@authors : Johan Fernandes
"""

import torch
from os.path import join
from dsd.hydra.benchmarks.tabular.algorithm_components.tabledet_models import (
    cascadetabnet,
    deformable_detr,
    table_transformer,
)
from dsd.hydra.benchmarks.models.schemas import TabularConfig
from dsd.hydra.benchmarks.tabular.models.tabular_schemas import (
    DetectionMethod,
    TabularAlgorithm,
)


def init_table_det_model(
    model_files: str, algorithm_config: TabularAlgorithm, tabular_config: TabularConfig
) -> torch.nn.Module:
    """Returns wrapper for a table det model that is available to us

    Arguments:
        model_files (str): Path of the directory containing model weights and config
        algorithm_config (TabularAlgorithm) : Stores details of the algorithm and each
                                              of its components
        tabular_config (TabularConfig): Configs such as type of device to run model on

    Returns:
        (torch.nn.Module) : MMDet / Pytorch trained model to detect tables
    """
    model = ""
    if (
        algorithm_config.components.tabledet_model
        == DetectionMethod.table_transformer.value
    ):
        model = table_transformer.TableTransformer(
            model_dir=model_files, config=tabular_config
        )
    else:

        model_cfg = join(model_files, algorithm_config.components.tabledet_model_cfg)
        model_weights = join(
            model_files, algorithm_config.components.tabledet_model_weight
        )

        if (
            algorithm_config.components.tabledet_model
            == DetectionMethod.cascadetabnet.value
        ):
            model = cascadetabnet.CascadeTabNet(
                model_config=model_cfg, weights=model_weights, config=tabular_config
            )
        elif (
            algorithm_config.components.tabledet_model
            == DetectionMethod.deformable_detr.value
        ):
            model = deformable_detr.DeformableDETR(
                model_config=model_cfg, weights=model_weights, config=tabular_config
            )

    return model
