#!/usr/bin/env python
# -*- coding: utf-8 -*-
# -----------------------------------------------------------
# Testing Hydra Module Template
#
# (C) 2022 Statistics Canada
# Author: Rafik Chemli
# -----------------------------------------------------------

import hashlib
import os
from distutils import dir_util

import pytest
from _pytest.fixtures import SubRequest
from py._path.local import LocalPath
from pydantic import BaseModel


def pytest_addoption(parser):
    """
    Function responsible for creation of command line option named output.

    Args:
        parser(Parser): Parser for command line arguments and ini-file values.

    Returns:
        None
    """
    parser.addoption("--output", action="store", nargs="?", const="output")


@pytest.fixture
def dev_output(pytestconfig) -> bool:
    """
    Fixture responsible for creation of an output folder with
    extra files such as "current_output.yml" and metrics.png in it.

    Args:
        pytestconfig (Config): Session-scoped fixture that returns
                                the session's pytest.Config object.

    Returns:
        Bool: True/False based on the value of --output option.
              True means that an output directory would be created
              whereas False means no output directory.
    """
    return pytestconfig.option.output


@pytest.fixture
def datadir(tmpdir: LocalPath, request: SubRequest) -> LocalPath:
    """
    Fixture responsible for searching a folder with the same name of test
    module and, if available, moving all contents to a temporary directory so
    tests can use them freely.

    Allows:
        - generates file path to auxiliar test case files.
        - possible concurrent test case access to files
        - prevents modifying the original file by a test case.

    Example:
        Folder structure.
        tests/                # test folder root
            |- conftest.py    # global fixtures automatically imported by pytest
            |- test_case.py   # test file
            |- test_case/     # matching test_case folder containing resources
                |⨽ file1.txt
                |⨽ subfolder/
                     |⨽file2.txt


        # content of test_case.py
        import os
        import pytest

        def test_case_1(datadir):
            full_path = datadir.join('file.txt')
            assert os.path.exists(full_path)

        def test_case_2(datadir):
            assert datadir.join('sub_folder','file2.txt') # prefered
            assert datadir.join('sub_folder/file2.txt')   #possible

    Args:
        tmpdir (LocalPath): global pytest fixture returning the temp
                            directory location where a test is running.
        request (SubRequest): global pytest fixture to access test case
                              metadata

    Returns:
        LocalPath: returning the temp directory location where a test is
                   running. All files in a folder called with the same
                   test case name will be avilable here.
    """
    filename = request.module.__file__
    test_dir, _ = os.path.splitext(filename)

    if os.path.isdir(test_dir):
        dir_util.copy_tree(test_dir, str(tmpdir))

    return tmpdir


def checksum(filename: str) -> str:
    """Computes a filename checksum using cryptography secured blake2b.
       It's also considerably faster than using md5

    Args:
        filename (str): path of the file

    Returns:
        str: checksum of the file content in a hexagesimal string representation
    """
    file_hash = None
    with open(filename, "rb") as f:
        file_hash = hashlib.blake2b()
        while chunk := f.read(8192):
            file_hash.update(chunk)
    return file_hash.hexdigest()


class Comparison(BaseModel):
    gt: str
    pred: str
    exp_output: str
    curr_output: str
    tcase: str
