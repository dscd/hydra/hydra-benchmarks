#!/usr/bin/env python
# -*- coding: utf-8 -*-
# -----------------------------------------------------------
# Hydra Module Blinding: save_redacted_outputs.py
#
# (C) 2022 Statistics Canada
# Author: Saptarshi Dutta Gupta
# ---------------

import glob
import logging
import os
from pathlib import Path
from typing import List, Optional

import fitz
import typer
from dsd.hydra.benchmarks.blinding.blinding_config import (
    AlgorithmName,
    Coordinates,
    Entities,
    FileNames,
    FileTypes,
    LanguageCode,
)
from dsd.hydra.benchmarks.blinding.presidio_wrappers.flair_recognizer import (
    FlairRecognizer,
)
from dsd.hydra.benchmarks.blinding.presidio_wrappers.transformer_recognizer import (
    TransformersRecognizer,
)
from dsd.hydra.benchmarks.blinding.utils import (
    create_custom_analyzer,
    create_default_analyzer,
    get_spacy_configurations,
    get_stanza_configurations,
    read_document,
)
from dsd.hydra.benchmarks.config.api import load_config
from dsd.hydra.benchmarks.models.schemas import PageInfo, Prediction, RegionInfo
from presidio_analyzer import AnalyzerEngine
from tqdm import tqdm

logger = logging.getLogger(__name__)

app = typer.Typer()

config = load_config()


def get_redaction(  # noqa C901
    document: fitz.fitz.Document,
    analyzer: AnalyzerEngine,
    score: float,
    language: str,
    entity_list: Optional[List],
) -> List[PageInfo]:
    """
    Function to loop through every text in the PDF, run the NER model and redact the PDF
    Args:
        - document (fitz.fitz.Document): document to process
        - analyzer (AnalyzerEngine): Microsoft Presidio service for detecting
        PII entities in text. It runs a set of different PII Recognizers, each
        one in charge of detecting one or more PII entities using
        different mechanisms.
        - score (float): NER threshold for redaction
        - language (str): language code used by Presidio for redatcing
        - entity_list(optional[List]): entities used by presidio for redacting
    Returns:
        - List[PageInfo]
    """

    # list of entities related to dates that
    # should be filtered out from the final redaction results
    entity_list_date = [
        Entities.date_time.value,
        Entities.date.value,
        Entities.time.value,
    ]

    # adjust the height of the redacted region.
    adjustment_factor = config.modules_config.blinding.adjustment_factor

    # keeps track of the number of PII (personally identifiable information)
    # elements that were redacted from the document
    count_redacted = 0

    # list of PageInfo objects,
    # each of which contains the information about
    # the redacted PII regions from a single page of the document.
    all_page_info: List[PageInfo] = []

    # Iterate over each page in the document.
    for index, page in enumerate(document):
        # Get the displaylist for the page
        # and extract the dictionary elements from it.
        page_display = page.get_displaylist()
        dictionary_elements = page_display.get_textpage().extractDICT()

        # list of RegionInfo objects
        # each of which contains the information
        # about a single redacted PII region from the current page.
        regions_dict: List[RegionInfo] = []

        # Iterate over each block, line,
        # and span in the dictionary elements to find PII regions.
        for block in dictionary_elements["blocks"]:
            for line in block["lines"]:
                for span in line["spans"]:
                    if span["text"] is not None:
                        # Call the analyzer's analyze method
                        # on the text span to find any PII entities.
                        if entity_list:
                            result_analyzer_en = analyzer.analyze(
                                span["text"], language=language, entities=entity_list
                            )
                        else:
                            result_analyzer_en = analyzer.analyze(
                                span["text"], language=language
                            )

                        # If any PII entities were found in the span,
                        # filter out the date-related entities and
                        # keep the ones with a score greater than or
                        # equal to the specified threshold.
                        if result_analyzer_en:
                            filtered_result = [
                                result
                                for result in result_analyzer_en
                                if result.score >= score
                                if result.entity_type not in entity_list_date
                            ]

                            # If any filtered PII entities were found,
                            # redact them from the page and add
                            # their information to regions_dict.
                            if filtered_result:
                                text = span["text"][
                                    filtered_result[0].start : filtered_result[0].end
                                ]
                                pii_coordinates = page.search_for(
                                    text, clip=span["bbox"]
                                )
                                if pii_coordinates:
                                    confidence = filtered_result[0].score
                                    delta = (
                                        pii_coordinates[0].height * adjustment_factor
                                    )
                                    adjusted_pii_coordinates = pii_coordinates[0] + (
                                        0,
                                        delta,
                                        0,
                                        -delta,
                                    )

                                    regions = RegionInfo(
                                        coordinates=tuple(adjusted_pii_coordinates),
                                        text=text,
                                        confidence=confidence,
                                    )
                                    regions_dict.append(regions)
                                    count_redacted += 1

        # Add the PageInfo object containing the
        # redacted PII region information for the current page to all_page_info.
        page_info = PageInfo(page=index, regions=regions_dict)
        all_page_info.append(page_info)

    # Log the number of PII elements
    # redacted from the current page and return all_page_info.
    logger.info(f"{str(count_redacted)} elements redacted in {page}")
    logger.info("Finished redacting page")
    return all_page_info


def get_analyzer(algorithm_name: str, model_name: Optional[str]) -> AnalyzerEngine:
    """
    Function to get the presidio analyzer
    Arguments:
        - algorithm_name (str): name of the algorithm to be used
        - model_name (str): name of the model to be used
    Returns:
        - AnalyzerEngine
    """

    # Define the language code
    lang_code_english = LanguageCode.english.value
    supported_languages = [lang_code_english]

    # Define the analyzer depending on the algorithm
    # For Bert and Flair the wrapper class is being passed to the analyzer
    # Spacy and Stanza is inbuilt in Presidio. The configurations are being defined
    # and passed to the analyzer
    if algorithm_name == AlgorithmName.bert.value:
        analyzer = create_default_analyzer(TransformersRecognizer())
    elif algorithm_name == AlgorithmName.flair.value:
        analyzer = create_default_analyzer(FlairRecognizer())
    elif algorithm_name == AlgorithmName.spacy.value:
        spacy_configuration = get_spacy_configurations()
        analyzer = create_custom_analyzer(spacy_configuration, supported_languages)
    elif algorithm_name == AlgorithmName.stanza.value:
        stanza_configuration = get_stanza_configurations()
        analyzer = create_custom_analyzer(stanza_configuration, supported_languages)
    return analyzer


def get_coordinates_to_redact(
    prediction: Prediction, filename: str, page_number: int
) -> Coordinates:
    """
    Function to get the coordinates of a particular file and page
    Arguments:
        - prediction (Prediction) : a Prediction pydantic model
        - filename (str) : name of the file from which coordinates are to be extracted
        - page_number (int): page from which coordinates are to be extracted
    Returns:
        - Coordinates
    """
    # create a list of region coordinates for the specified page in the specified file
    coordinates = [
        region.coordinates
        for file in prediction.files  # iterate through all files in the prediction
        if file.filename == filename  # filter files to the specified filename
        for page in file.pages  # iterate through all pages in the selected file
        if page.page == page_number  # filter pages to the specified page number
        for region in page.regions  # iterate through all regions in the selected page
    ]

    # create a Coordinates object with the region coordinates
    return Coordinates(coordinates=coordinates)


def redact_documents(prediction: Prediction, out_dir: str) -> None:
    """
    Driver function to redact documents
    Arguments:
        - prediction (Prediction) : a Prediction pydantic model
        - out_dir (str) : Location where the redacted files will be stored
    """
    # Get the dataset directory using the specified configuration
    dataset_dir = os.path.join(
        config.cache_dir,
        config.root_dataset,
        prediction.dataset,
        prediction.version,
        prediction.subset,
        config.dt_dir,
    )

    # Iterate through all the subdirectories in the dataset directory
    for data_dir in tqdm(os.listdir(dataset_dir)):
        # Get the path of the subdirectory
        dataset_dir_path = os.path.join(dataset_dir, data_dir)
        try:
            # Find the path of the first PDF file in the subdirectory
            raw_pdf_path = glob.glob(
                os.path.join(dataset_dir_path, f"*{FileTypes.pdf.value}")
            )[0]
        except IndexError:
            # If a PDF file is not found, log an error and exit the program
            logger.error(f"PDF not found inside {dataset_dir_path}")
            raise typer.Exit()

        # Read the raw PDF file using the pdf_redactor library
        document = read_document(raw_pdf_path)

        # Get the filename of the PDF and convert it to lowercase
        filename = os.path.basename(raw_pdf_path).lower()

        # Iterate through all the pages in the PDF document
        for index, page in enumerate(document):
            # Get the coordinates to redact
            # for the current page using the specified configuration
            coordinates: Coordinates = get_coordinates_to_redact(
                prediction, filename, index
            )
            # Iterate through all the coordinates
            # and add a redaction annotation to the page
            for coordinate in coordinates.coordinates:

                page.add_redact_annot(
                    coordinate,
                    text_color=config.modules_config.blinding.redaction_text_color,
                    fill=config.modules_config.blinding.redaction_fill_color,
                )

            # Apply the redactions to the page
            page.apply_redactions()

        # Generate the filename for the redacted PDF file
        redacted_filename = (
            os.path.basename(Path(filename).stem)
            + FileNames.redacted.value
            + "."
            + FileTypes.pdf.value
        )

        # Save the redacted PDF file to the output directory
        document.save(os.path.join(out_dir, redacted_filename))


def annotate_documents(prediction: Prediction, out_dir: str) -> None:
    """
    Driver function to annotate documents
    Arguments:
        - prediction (Prediction) : a Prediction pydantic model
        - out_dir (str) : Location where the annotated files will be stored
    """
    # Get the dataset directory using the specified configuration
    dataset_dir = os.path.join(
        config.cache_dir,
        config.root_dataset,
        prediction.dataset,
        prediction.version,
        prediction.subset,
        config.dt_dir,
    )

    adjustment_factor = config.modules_config.blinding.adjustment_factor

    # Iterate through all the subdirectories in the dataset directory
    for data_dir in tqdm(os.listdir(dataset_dir)):
        # Get the path of the subdirectory
        dataset_dir_path = os.path.join(dataset_dir, data_dir)
        try:
            # Find the path of the first PDF file in the subdirectory
            raw_pdf_path = glob.glob(
                os.path.join(dataset_dir_path, f"*{FileTypes.pdf.value}")
            )[0]
        except IndexError:
            # If a PDF file is not found, log an error and exit the program
            logger.error(f"PDF not found inside {dataset_dir_path}")
            raise typer.Exit()

        # Read the raw PDF file using the pdf_redactor library
        document = read_document(raw_pdf_path)

        # Get the filename of the PDF and convert it to lowercase
        filename = raw_pdf_path.split("/")[-1].lower()

        # Iterate through all the pages in the PDF document
        for index, page in enumerate(document):
            # Get the coordinates to redact for
            # the current page using the specified configuration
            coordinates: Coordinates = get_coordinates_to_redact(
                prediction, filename, index
            )

            # Iterate through all the coordinates
            # and add a rectangle annotation to the page
            for coordinate in coordinates.coordinates:

                # adjust the coordinates
                coordinate = fitz.Rect(coordinate)
                delta = coordinate.height * adjustment_factor
                adjusted_coordinate = coordinate - (
                    0,
                    delta,
                    0,
                    -delta,
                )
                annot = page.add_rect_annot(adjusted_coordinate)
                annot.update()

        # Generate the filename for the redacted PDF file
        annotated_filename = (
            os.path.basename(Path(filename).stem)
            + FileNames.annotated.value
            + "."
            + FileTypes.pdf.value
        )

        # Save the redacted PDF file to the output directory
        document.save(os.path.join(out_dir, annotated_filename))
