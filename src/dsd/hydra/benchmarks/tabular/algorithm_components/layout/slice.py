"""SLICE algorithm implementation
   Based on the original SLICE algorithm
   this new code contains more optimized and
   explainable sections that can be utilized
   with any OCR engine

@authors : Johan Fernandes, Niranjan Ramesh
"""

from PIL import Image, ImageDraw
from scipy.signal import find_peaks
import numpy as np
import pandas as pd
from typing import List, Tuple
import pyqtree

from dsd.hydra.benchmarks.tabular.models.tabular_schemas import (
    ExtractedContent,
    SliceTableContents,
    TabularCell,
    GridBBox,
)
from dsd.hydra.benchmarks.tabular.algorithm_components.layout.init import TableLayout


def get_table_img(
    table_bbox: Tuple[float, float, float, float], document_content: ExtractedContent
) -> SliceTableContents:
    """Returns a black and white binary image of the table

    Arguments:
        table_bbox (Tuple) : Bounding box for the table
        document_content (ExtractedContent) : Extracted line bboxes with text
            These line bboxes are oriented with the original image scale

    Returns:
        (SliceTableContents) : The table image, word bboxes and word text
    """
    table_dim = table_bbox
    width = round(table_dim[2] - table_dim[0])
    height = round(table_dim[3] - table_dim[1])
    new_table_img = Image.new("L", (width, height), (255))
    new_table_img_draw = ImageDraw.Draw(new_table_img)

    # new_word_bboxes_in_table = []
    for bbox in document_content.lines_bboxes:
        x1, y1, x2, y2 = bbox
        # Reorient the boxes from the original image to this new table image
        x1 -= table_dim[0]
        y1 -= table_dim[1]
        x2 -= table_dim[0]
        y2 -= table_dim[1]
        points = [(x1, y1), (x2, y2)]
        new_table_img_draw.rectangle(points, fill="black", outline="white")

    slice_info = SliceTableContents(
        image=new_table_img,
        image_width=width,
        image_height=height,
    )

    return slice_info


def generate_vertical_points(
    new_table_img: np.ndarray,
    width: float,
    table_loc_xmin: float,
    window_size: int = 60,
) -> List:
    """Returns the vertical points along the x-axis of the image for the columns

    Arguments:
        new_table_img (np.ndarray): Binary table image
        width (float): width of the table image
        table_loc_xmin (float): x1 value of table region to rescale
            the vertical points to match original
            image size
        window_size (int) : Window to find peaks

    Returns:
        (List): Vertical points along the x-axis
    """

    # :TODO move the hardcoded value to config
    sum_vertical = np.logical_not(np.array(new_table_img)).sum(0)
    sum_vertical_series = pd.Series(sum_vertical).rolling(window=window_size).median()
    sum_vertical_series = sum_vertical_series.fillna(0)

    peaks, _ = find_peaks(sum_vertical_series)
    vertical_points = []
    start = 0
    for peak in peaks:
        min_value = sum_vertical_series[start:peak].idxmin()
        vertical_points.append(table_loc_xmin + min_value)
        start = peak + 1

    vertical_points.append(table_loc_xmin + sum_vertical_series[start:].idxmin())
    vertical_points.append(table_loc_xmin + width)

    return vertical_points


def generate_horizontal_points(
    new_table_img: np.ndarray,
    height: float,
    table_loc_ymin: float,
    window_size: int = 10,
) -> List:
    """Returns the horizontal points along the y-axis of the image for the rows

    Arguments:
        new_table_img (np.ndarray): Binary table image
        height (float): height of the table image
        table_loc_ymin (float): y1 value of the table location to
            rescale all the horizontal points to the exact table location
            in the original image size
        window_size (int) : Window to find peaks

    Returns:
        (List) : Horizontal points along the y-axis

    """

    # :TODO move the hardcoded value to config
    sum_horizontal = np.logical_not(np.array(new_table_img)).sum(1)
    sum_horizontal_series = (
        pd.Series(sum_horizontal).rolling(window=window_size).median()
    )
    sum_horizontal_series = sum_horizontal_series.fillna(0)

    peaks, _ = find_peaks(sum_horizontal_series)
    horizontal_points = []
    start = 0
    for peak in peaks:
        min_value = sum_horizontal_series[start:peak].idxmin()
        horizontal_points.append(table_loc_ymin + min_value)
        start = peak + 1

    horizontal_points.append(table_loc_ymin + sum_horizontal_series[start:].idxmin())
    horizontal_points.append(table_loc_ymin + height)

    return horizontal_points


def generate_table_structure(rows: int, cols: int) -> List[List[TabularCell]]:
    """
    Generate a table structure to be sent to the final Pydantic model

    Arguments:
        rows (int): number of rows
        cols (int): number of columns

    Returns:
        (List[List[TabularCell]]) : Tabular structure which stores all extracted
                                    table text.
    """

    # Cannot use list comprehension here as TabularCell objects are not unique
    # which results in call be reference to a single TabularCell object
    table_str = []
    # Create multiple rows based on the row count
    for _ in range(rows):
        row = []
        for _ in range(cols):
            # Create a row the the determined number of cells based on cols count
            row.append(
                TabularCell(cell_coordinates=[0, 0, 0, 0], cell_text="", text=[])
            )
        table_str.append(row)

    return table_str


def generate_grid(  # noqa C901
    horizontal_points: List,
    vertical_points: List,
    rows: int,
    cols: int,
    table_loc: Tuple[float, float, float, float],
) -> pyqtree.Index:
    """Generate a grid to identify which word belongs to which box

    Arguments:
        horizontal_points (List): List of points along the y-axis
        vertical_points (List): List of points along the x-axis
        rows (int): number of rows
        cols (int): number of columns
        table_loc (Tuple[float, float, float, float]) : The location
            of the table is provided to generate a grid tree

    Returns
        (dict): Dictionary of {"row-id_column_id" : [bbox in grid]}

    """
    # Get min points of each bbox in the table region
    mins = []
    for h_idx, h in enumerate(horizontal_points):
        if h_idx < (len(horizontal_points) - 1):
            for v_idx, v in enumerate(vertical_points):
                if v_idx < (len(vertical_points) - 1):
                    mins.append((v, h))

    # Get max points of each bbox in the table region
    maxs = []
    for h_idx, h in enumerate(horizontal_points):
        if h_idx > 0:
            for v_idx, v in enumerate(vertical_points):
                if v_idx > 0:
                    maxs.append((v, h))

    # Create a tree with the size, location and shape of table
    grid = pyqtree.Index(table_loc)
    row_idx = 0
    col_idx = 0
    for min_point, max_point in zip(mins, maxs):
        grid_bbox = GridBBox(
            col_id=col_idx,
            row_id=row_idx,
            coordinates=[min_point[0], min_point[1], max_point[0], max_point[1]],
        )

        # Insert into the tree
        grid.insert(item=grid_bbox, bbox=grid_bbox.coordinates)

        if col_idx < (cols - 1):
            col_idx += 1
        else:
            col_idx = 0
            row_idx += 1

    return grid


def assign_cells_to_grid(
    grid: pyqtree.Index,
    table_str: List[List[TabularCell]],
    word_bboxes: List[Tuple[float, float, float, float]],
    word_text: List[str],
) -> List[List[TabularCell]]:
    """
    Assign the word cells to the table structure which is a
    List[List[TabularCell]]. Each word will belong to one cell
    which in turn will have a specific row and column id within
    that table.

    Arguments:
        grid (pyqtree.Index) : A tree of bounding boxoes for the
        table_str (List[List[TabularCell]]) : Table structure with tabular cells.
        word_bboxes (List[Tuple[float, float, float, float]]): Bounding boxes of words
            within the table
        word_text List(str): List of words with the table.

    Returns:
        List[List[TabularCell]] : Table structure with tabular cells.

    """

    for word_bbox, word_text in zip(word_bboxes, word_text):
        # Identify the word bbox with the grid tree
        grid_bbox_det = grid.intersect(word_bbox)

        if len(grid_bbox_det) > 0:
            # :TODO validate if there is only one box
            grid_box = grid_bbox_det[0]
            cell = table_str[grid_box.row_id][grid_box.col_id]
            cell.row_id = grid_box.row_id
            cell.col_id = grid_box.col_id

            if len(cell.cell_text) == 0:
                cell.cell_coordinates = word_bbox
                cell.cell_text = str(word_text)
            else:
                cell.cell_text = cell.cell_text + " " + str(word_text)
                cell_x1, cell_y1, cell_x2, cell_y2 = cell.cell_coordinates
                bbox_x1, bbox_y1, bbox_x2, bbox_y2 = word_bbox
                # Reorganize the cell-coordinates if a new word is added
                cell.cell_coordinates = [
                    min(cell_x1, bbox_x1),
                    min(cell_y1, bbox_y1),
                    max(cell_x2, bbox_x2),
                    max(cell_y2, bbox_y2),
                ]

    # :TODO check if this is passed by reference of value
    return table_str


def remove_blank_cells(table_str: List[List[TabularCell]]) -> List[List[TabularCell]]:
    """
    Removes empty cells from tablestr

    Arguments:
        table_str (List[List[TabularCell]]) : Tabular structure with table text

    Returns:
        (List[List[TabularCell]]) : Updated table structure without blank cells
    """

    updated_table_str = []

    for table_row in table_str:
        row = []
        for cell in table_row:
            # This will eliminate any empty columns
            if len(cell.cell_text) > 0:
                row.append(cell)

        # This will eliminate any empty rows
        if len(row) > 0:
            updated_table_str.append(row)

    return updated_table_str


class SLICE(TableLayout):
    def __init__(self, horizontal_window, vertical_window):
        self.horizontal_window = horizontal_window
        self.vertical_window = vertical_window

    def slice_table(
        self,
        table_bbox: Tuple[float, float, float, float],
        document_content: ExtractedContent,
        # horizontal_window: int = 10,
        # vertical_window: int = 60,
    ) -> List[List[TabularCell]]:
        """Identify the grid layout of the table and place the table text accuractely

        Arguments:
            table_bbox (Tuple) : Bounding box for the table
            document_content (ExtractedContent) : Extracted line and word bounding
                boxes with text
            horizontal_window (int) : Window to find peaks horizontally
            vertical_window (int) : Window to find peaks vertically

        Returns:
            table_df(pd.DataFrame) :  DataFrame with table text
        """
        if not document_content:
            raise TypeError("document_content cannot be None or False")

        # Get the binary table image of this specific table
        slice_info = get_table_img(table_bbox, document_content)

        table_loc_xmin, table_loc_ymin = table_bbox[0], table_bbox[1]

        # Get the vertical points along the x-axis of the image for the columns
        vertical_points = generate_vertical_points(
            slice_info.image,
            slice_info.image_width,
            table_loc_xmin,
            self.vertical_window,
        )

        # Get the horizontal points along the y-axis of the image for the rows
        horizontal_points = generate_horizontal_points(
            slice_info.image,
            slice_info.image_height,
            table_loc_ymin,
            self.horizontal_window,
        )

        rows = len(horizontal_points) - 1
        cols = len(vertical_points) - 1

        # Generate a grid to develop table cell bboxes in the table grid
        grid = generate_grid(horizontal_points, vertical_points, rows, cols, table_bbox)

        # Generate a blank table structure with rows and columns
        # :TODO will handle column spanning cells here
        table_str = generate_table_structure(rows, cols)

        # Assign the table text to the right grid
        table_str = assign_cells_to_grid(
            grid, table_str, document_content.word_bboxes, document_content.word_text
        )

        table_str = remove_blank_cells(table_str)

        return table_str
