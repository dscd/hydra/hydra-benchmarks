Description:
The objective of this specific unit test is to ensure that the function analyze_prediction in main.py performs accurately when called upon to process a typical file input via the tabular module.
The Ground Truth File contains 1 file with all the regions populated whereas the prediction file contains 1 file where region is modified to be []. The expected output has 1 file with False negative as 1 and region count as 1. Besides that all other metrics would be 0.
