#!/usr/bin/env python
# -*- coding: utf-8 -*-
# -----------------------------------------------------------
# Hydra Benchmarks: generate_output_yaml_pipeline.py
#
# (C) 2022 Statistics Canada
# Author: Saptarshi Dutta Gupta
# ---------------

import glob
import logging
import os
import sys
import time
from datetime import datetime, timedelta
from typing import List, Optional

import yaml
import fitz
import typer
from dsd.hydra.benchmarks.blinding.blinding_config import (
    AlgorithmName,
    Entities,
    FileTypes,
    LanguageCode,
)
from dsd.hydra.benchmarks.blinding.presidio_wrappers.flair_recognizer import (
    FlairRecognizer,
)
from dsd.hydra.benchmarks.blinding.presidio_wrappers.transformer_recognizer import (
    TransformersRecognizer,
)
from dsd.hydra.benchmarks.blinding.redaction import get_redaction
from dsd.hydra.benchmarks.blinding.utils import (
    create_custom_analyzer,
    create_default_analyzer,
    get_spacy_configurations,
    get_stanza_configurations,
    read_document,
    save_yaml_file,
)
from dsd.hydra.benchmarks.config.api import load_config
from dsd.hydra.benchmarks.models.schemas import (
    FileInfo,
    PageInfo,
    Prediction,
    GroundTruth,
)

logger = logging.getLogger(__name__)

app = typer.Typer()

config = load_config()


def create_redacted_output(
    document: fitz.fitz.Document,
    algorithm_name: str,
    ner_threshold: str,
    ner_model: Optional[str] = None,
) -> PageInfo:
    """
    Function to generate and return redacted output of a raw document
    based on the passed algorithm, NER threshold and NER model

    Argument(s):
        - document (fitz.fitz.Document): document to process
        - algorithm_name(str): selected algorithm like presdio_stanza, presidio_flair
        - ner_threshold(str): selected ner threshold for redacting an entity
        - ner_model(str): selected ner model. This is an optional argument

    Returns:
        - PageInfo
    """
    logger.info(f"Creating analyzer for: {algorithm_name}")

    # Set the supported language and entities based on the algorithm
    lang_code_english = LanguageCode.english.value
    entities = [
        Entities.phone_number.value,
        Entities.location.value,
        Entities.person.value,
        Entities.email_address.value,
    ]
    supported_languages = [lang_code_english]

    # Create the analyzer and extract the redacted information based on the algorithm
    if algorithm_name == AlgorithmName.bert.value:
        analyzer = create_default_analyzer(TransformersRecognizer())
        page_info = get_redaction(
            document, analyzer, ner_threshold, lang_code_english, None
        )
    elif algorithm_name == AlgorithmName.flair.value:
        analyzer = create_default_analyzer(FlairRecognizer())
        page_info = get_redaction(
            document, analyzer, ner_threshold, lang_code_english, None
        )
    elif algorithm_name == AlgorithmName.spacy.value:
        spacy_configuration = get_spacy_configurations()
        analyzer = create_custom_analyzer(spacy_configuration, supported_languages)
        page_info = get_redaction(
            document, analyzer, ner_threshold, lang_code_english, None
        )
    elif algorithm_name == AlgorithmName.stanza.value:
        stanza_configuration = get_stanza_configurations()
        analyzer = create_custom_analyzer(stanza_configuration, supported_languages)
        page_info = get_redaction(
            document, analyzer, ner_threshold, lang_code_english, entities
        )
    else:
        # If an unsupported algorithm is provided, log the error and exit the program
        logger.info("Incorrect algorithm name entered")
        sys.exit()
    return page_info


def generate_reports(  # noqa C901
    dataset: str,
    version: str,
    subset: str,
    algorithm_name: str,
    ner_threshold: str,
    model_name: str,
    input_dir: str = None,
    run_folder: bool = False,
) -> Prediction:
    """
    Function to generate the output report
    Argument(s):
        - dataset(str): name of the HYDRA module to be used
        - version (str): version of the dataset to be used
        - subset (str): subset to be used
        - algorithm_name(str): name of the selected algorithm
        - ner_threshold(str): selected threshold of NER score
        - model_name(str): name of NER model, if given
        - input_dir(str): input directory for the run folder option
        - run_folder(bool): boolean to decide whether to run a dataset or folder
                            True for run_folder and False for run_dataset
    Returns:
        - Prediction
    """
    # initialize variable for total execution time
    all_file_exec_time = 0.0

    # set path to the directory containing PDF files to be processed
    if run_folder:
        benchmark_dataset_directory = os.path.abspath(
            os.path.join(
                input_dir,
                config.dt_dir,
            )
        )
    else:
        benchmark_dataset_directory = os.path.abspath(
            os.path.join(
                config.cache_dir,
                config.root_dataset,
                dataset,
                version,
                subset,
                config.dt_dir,
            )
        )

    # check if the specified directory exists
    if not os.path.exists(benchmark_dataset_directory):
        logger.error(f"Path {benchmark_dataset_directory} not found")
        raise typer.Exit()

    # log the start of processing and initialize a
    # list to store information on all processed files
    logger.info(f"Started processing files in {benchmark_dataset_directory}")
    all_file_information: List[FileInfo] = []

    # iterate over all directories in the specified directory
    for dataset_dir in os.listdir(benchmark_dataset_directory):
        # set path to the directory containing the PDF file to be processed
        dataset_dir_path = os.path.join(benchmark_dataset_directory, dataset_dir)
        # try to find a PDF file in the specified directory
        try:
            raw_pdf_path = glob.glob(
                os.path.join(dataset_dir_path, f"*{FileTypes.pdf.value}")
            )[0]
        except IndexError:
            # Exit the program if a PDF file is not found
            logger.error(f"PDF not found inside {dataset_dir_path}")
            raise typer.Exit()

        # read the PDF file and measure the execution time for redacting its pages
        document = read_document(raw_pdf_path)

        per_file_start_time = time.monotonic()
        # Pass the document through the NER algorithm and get the PII information
        page_info: PageInfo = create_redacted_output(
            document, algorithm_name, float(ner_threshold), model_name
        )
        per_file_end_time = time.monotonic()
        per_file_exec_time = timedelta(
            seconds=per_file_end_time - per_file_start_time
        ).total_seconds()
        all_file_exec_time += per_file_exec_time

        # store the information on the processed file in the list of file information
        filename = dataset_dir + "." + FileTypes.pdf.value
        file_info = FileInfo(
            filename=filename, pages=page_info, exec_time=per_file_exec_time
        )
        all_file_information.append(file_info)

    # store the information on the processed files in a Prediction object
    prediction_output = Prediction(
        algorithm_name=algorithm_name,
        dataset=dataset,
        version=version,
        subset=subset,
        files=all_file_information,
        exec_time=all_file_exec_time,
    )

    return prediction_output


def run_prediction_folder(
    dataset: str,
    algorithm_name: str,
    input_dir: str,
    output_dir: str,
    ner_threshold: float,
    model_name: str = None,
):
    """
    Driver function to run the prediction pipeline on a folder
    Argument(s):
        - dataset(str): name of the HYDRA module to be used
        - algorithm_name(str): name of the selected algorithm
        - input_dir (str): Directory where the data files are stored.
        - output_dir (str): Directory where predictions will be stored.
        - ner_threshold(str): selected threshold of NER score
        - model_name(str): name of NER model, if given
    """
    # compute the ground truth path
    gt_file_path = os.path.join(input_dir, config.gt_dir, config.gt_file)

    # parse the ground truth
    gt_ = GroundTruth(**dict(yaml.load(open(gt_file_path), yaml.Loader)))

    # run the prediction pipeline on the input_dir
    run_prediction_pipeline(
        dataset,
        gt_.version,
        [gt_.subset],
        algorithm_name,
        output_dir,
        ner_threshold,
        model_name,
        input_dir=input_dir,
        run_folder=True,
    )


@app.command()
def run_prediction_pipeline(
    dataset: str = typer.Argument(..., help="name of the hydra module"),
    version: str = typer.Argument(..., help="version of the dataset to be used"),
    subsets: List[str] = typer.Argument(..., help="subset of the dataset to be used"),
    algorithm_name: str = typer.Argument(..., help="NER algorithm to be used"),
    output_dir: str = typer.Argument(
        ..., help="directory where predictions will be stored"
    ),
    ner_threshold: float = typer.Argument(
        ..., help="threshold value for the NER model"
    ),
    model_name: Optional[str] = typer.Option(None, help="NER model to be used"),
    input_dir: Optional[str] = typer.Option(
        None, help="input directory for the run folder option"
    ),
    run_folder: Optional[bool] = typer.Option(
        False, help="boolean to decide whether to run a dataset or folder"
    ),
) -> None:

    """
    Driver function to run pipeline to generate redacted outputs, extract annotations,
    generate the output yaml file for the given model, ner threshold and IOU threshold.
    Argument(s):
        - dataset(str): name of the HYDRA module to be used
        - version (str): version of the dataset to be used
        - subsets (List[str]): list of subset to be used
        - algorithm_name(str): name of the selected algorithm
        - output_dir (str): Directory where predictions will be stored.
        - ner_threshold(str): selected threshold of NER score
        - model_name(str): name of NER model, if given
        - input_dir(str): input directory for the run folder option
        - run_folder(bool): boolean to decide whether to run a dataset or folder
                            True for run_folder and False for run_dataset
    """

    logger.info(f"Started processing {dataset} dataset {version}")
    # Loop through the subsets
    for subset in subsets:
        logger.info(f"Processing subset:{subset}")
        # Process each subset

        prediction_output: Prediction = generate_reports(
            dataset,
            version,
            subset,
            algorithm_name,
            ner_threshold,
            model_name,
            input_dir,
            run_folder,
        )
        logger.info("Finished processing subset")

        # Create output directory if it does not exist
        out_dir_path = os.path.abspath(output_dir)
        if not os.path.exists(out_dir_path):
            os.mkdir(out_dir_path)

        # get the current date
        date = datetime.today().strftime("%Y-%m-%d")

        # define the prediction filename
        prediction_filename = (
            f"prediction_{algorithm_name}_subset_{subset}_ner-{ner_threshold}_{date}"
        )
        # Create a complete path for saving the ground truth
        save_path = os.path.join(out_dir_path, prediction_filename)

        # Save the prediction file
        save_yaml_file(prediction_output, save_path)
        logger.info(f"Successfully dumped {prediction_filename} at {out_dir_path}")


if __name__ == "__main__":
    app()
