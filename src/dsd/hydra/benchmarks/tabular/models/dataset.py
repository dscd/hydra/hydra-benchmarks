import os
import mmcv

from typing import Optional, List
from torch.utils.data import Dataset, DataLoader

from dsd.hydra.benchmarks.tabular.models.tabular_schemas import DocPage, BatchData


def collate_fn(img_batch: List) -> List[List]:
    """Batches the image as a List[List]

    Arguments:
        batch (List) : List of images & labels

    Returns:
        (List[List]) : List(images in a batch)
    """
    batch = BatchData(images=[], filenames=[], dims=[], pages=[])

    for img_info in img_batch:
        batch.images.append(img_info.img)
        batch.filenames.append(img_info.filename)
        batch.dims.append(img_info.dim)
        batch.pages.append(img_info.page)

    return batch


class ImageDataset(Dataset):
    """
    Returns ImageDataset object

    Arguments:
        fld_path (str) : Path to directory containing images
        images_list (List[str]): List of images to process

    Returns:
        ImageDataset: Implementation of Pytorch dataset for custom table detection from
        images dataset
    """

    def __init__(self, fld_path: str, images_list: List[str]) -> None:

        self.fld_path = fld_path
        self.file_names = images_list

    def __getitem__(self, idx: int):
        """
        Returns one dataset item to the Pytorch dataloader

        Arguments:
            idx (int) : index of object in list of filesnames

        Returns:
            dict : Dictionary which contains either PIL
                     or mmcv (numpy) image
        """
        img_name = os.path.join(self.fld_path, self.file_names[idx])
        image = {}
        # if self.det_type == "mmdet":
        image = mmcv.imread(img_name)
        height, width, _ = image.shape
        image_det = DocPage(
            img=image, filename=self.file_names[idx], dim=(height, width), page=0
        )
        return image_det

    def __len__(self):
        return len(self.file_names)


def build_image_dataset(fld_path: str, images_list: List[str]) -> ImageDataset:
    """Returns a Tabular Dataset for images

    Arguments:
        fld_path (str): Path to directory containing images
        images_list (List[str]) : List of images to process

    Returns:
        (ImageDataset) : Class to load images from folder
    """

    return ImageDataset(fld_path=fld_path, images_list=images_list)


def build_image_dataloader(
    image_dataset: ImageDataset,
    batch_size: int = 2,
    shuffle: Optional[bool] = False,
    num_workers: Optional[int] = 0,
) -> DataLoader:
    """Sample dataloader for any tabular dataset of images

    Arguments:
        image_dataset(ImageDataset): Image Dataset for all images in a subset
        batch_size(int) : Size of each batch (default = 2)
        shuffle(bool) : Shulffle the images or no
        num_workers (int) : Number of workers for the data loader

    Returns:
        (DataLoader) : Loader to load a batch of images from dataset
    """
    sample_dataloader = DataLoader(
        image_dataset,
        batch_size=batch_size,
        shuffle=shuffle,
        collate_fn=collate_fn,
        num_workers=num_workers,
    )

    return sample_dataloader


class PDFDataset(Dataset):
    """
    Returns a Dataset consisting of all pages of a PDF file

    Arguments:
        pdf_filename(str) : Name of the PDF file
        pdf_doc(fitz.fitz.Document): The document object of the PDF file

    Returns:
        (PDFDataset) : Dataset for batch of page images from a PDF file
    """

    def __init__(self, extraction_class):

        self.extraction_class = extraction_class

    def __getitem__(self, idx: int):
        """
        Returns the image of one page of the PDF for the Pytorch dataloader

        Arguments:
            idx (int): Index of item to return to dataloader , ie, page number

        Returns:
            (np.ndarray): Image as an numpy array
        """
        return self.extraction_class.get_docpage(page_number=idx)

    def __len__(self):
        return self.extraction_class.doc.page_count


def build_pdf_dataset(extraction_class) -> PDFDataset:
    """create a PDF Dataset consisting of images of every page of a PDF

    Arguments:
        extraction_class (str) : The module of extraction used

    Returns:
        (PDFDataset) : Class to load images from a single PDF
    """
    return PDFDataset(extraction_class)


def build_pdf_dataloader(
    pdf_dataset: PDFDataset,
    batch_size: int = 2,
    shuffle: Optional[bool] = False,
    num_workers: Optional[int] = 0,
) -> DataLoader:
    """Dataloader of images of every page in a PDF

    Arguments:
        pdf_dataset(PDFDataset): Dataset for PDF pages for a single file
        batch_size(int) : Size of each batch (default = 2)
        shuffle(bool) : Shulffle the images or no
        num_workers (int) : Number of workers for the data loader

    Returns:
        (DataLoader) : Loader to load a batch of images from a PDF
    """
    sample_dataloader = DataLoader(
        pdf_dataset,
        batch_size=batch_size,
        shuffle=shuffle,
        collate_fn=collate_fn,
        # Using the number of CPU's to determine num_workers
        num_workers=num_workers,
    )

    return sample_dataloader
