import os
import pathlib
from os.path import join
from typing import Optional

import fitz
import numpy as np
import yaml
from tqdm.auto import tqdm

from dsd.hydra.benchmarks.config.api import load_config
from dsd.hydra.benchmarks.models.schemas import (
    FileInfo,
    GroundTruth,
    IntensityU8,
    Noise,
    PageInfo,
    RegionInfo,
)
from dsd.hydra.benchmarks.modules.download import Download


class SynthDataProcessor:
    def __init__(self, pdf_path) -> None:
        """
        Creates the Class with the initial arguments.
        This Class adds noise to a selected page in a pdf.

        Arguments:
                - pdf_path (str): Path to the pdf file

        Returns:
                - (None)
        """
        self.pdf_path = pdf_path
        self.doc = fitz.open(pdf_path)

    def add_salt_noise(self, page_num: int, amount: float = 0.3) -> None:
        """
        This method applies salt noise to the page.

        Arguments:
                - page_num (int): The page to apply the noise
                - amount (float): The intensity of the salt noise

        Returns:
                - (None)
        """
        img_array = self._get_page_image_array(page_num)
        dst = img_array.copy()
        noise = np.random.random(img_array.shape)
        dst[noise < amount] = IntensityU8.bright.value
        salted = dst.astype(np.uint8)
        self._replace_page_image(page_num, salted, noise_name=Noise.salt.value)

    def add_pepper_noise(self, page_num: int, amount: float = 0.05) -> None:
        """
        This method applies pepper noise to the page.

        Arguments:
                - page_num (int): The page to apply the noise
                - amount (float): The intensity of the pepper noise

        Returns:
                - (None)
        """
        img_array = self._get_page_image_array(page_num)
        dst = img_array.copy()
        noise = np.random.random(img_array.shape)
        dst[noise < amount] = IntensityU8.dark.value
        peppered = dst.astype(np.uint8)
        self._replace_page_image(page_num, peppered, noise_name=Noise.pepper.value)

    def add_salt_pepper_noise(
        self, page_num: int, salt_amount: float = 0.1, pepper_amount: float = 0.05
    ):
        """
        This method applies salt and pepper noise to the page.

        Arguments:
                - page_num (int): The page to apply the noise
                - salt_amount (float): The intensity of the salt noise
                - pepper_amount (float): The intensity of the pepper noise

        Returns:
                - (None)
        """
        img_array = self._get_page_image_array(page_num)
        dst = img_array.copy()
        noise = np.random.random(img_array.shape)
        dst[noise < salt_amount] = IntensityU8.bright.value
        salted = dst.astype(np.uint8)

        noise = np.random.random(img_array.shape)
        salted[noise < pepper_amount] = IntensityU8.dark.value
        salt_peppered = dst.astype(np.uint8)

        self._replace_page_image(
            page_num, salt_peppered, noise_name=Noise.salt_pepper.value
        )

    def _get_page_image_array(self, page_num: int) -> np.array:
        """
        Extracts the selected page

        Arguments:
                - page_num (int): The page to apply the noise
                - amount (float): The intensity of the salt noise

        Returns:
                - (np.array): The selected page as a numpy array
        """
        page = self.doc[page_num]
        pixmap = page.get_pixmap()
        img_array = np.frombuffer(pixmap.samples, dtype=np.uint8).reshape(
            pixmap.height, pixmap.width, pixmap.n
        )[:, :, :3]
        return img_array

    def _replace_page_image(
        self, page_num: int, new_img_array: np.array, noise_name: str
    ) -> None:
        """
        This method replaces the selected page with the altered version.
        It saves the modified pdf as a new file.

        Arguments:
                - page_num (int): The page to apply the noise
                - amount (float): The intensity of the salt noise
                - noise_name (str): Name of the noise applied

        Returns:
                - (None)
        """
        page = self.doc[page_num]
        rect = page.rect
        pixmap = fitz.Pixmap(
            fitz.csRGB,
            new_img_array.shape[1],
            new_img_array.shape[0],
            bytearray(new_img_array.tobytes()),
        )
        new_page = self.doc.new_page(
            pno=page_num + 1, width=page.rect.width, height=page.rect.height
        )
        new_page.insert_image(rect, pixmap=pixmap)
        self.doc.delete_page(page_num)
        original_file_name = pathlib.Path(self.pdf_path).stem
        parent_path = str(pathlib.Path(self.pdf_path).parent)
        new_file_name = "".join([original_file_name, noise_name, ".pdf"])
        new_file_path = os.path.join(parent_path, new_file_name)
        self.doc.save(new_file_path)
        self.doc = fitz.open(self.pdf_path)


class PDFReader:
    """
    PDFReader class is used to read pages of a PDF
    and extract text along with the bounding box coordinates
    of the text from it.
    """

    def __init__(self, pdf_path) -> None:
        """
        Iniatialization of the class is done here.

        Arguments:
                - pdf_path (str): Path to the pdf file

        Returns:
                - (None)
        """
        self.pdf_path = pdf_path
        self.doc = fitz.open(pdf_path)
        self.page_details = PageInfo(page=0, regions=[])

    def get_page_data(self, page_num: int) -> PageInfo:
        """
        This method returns the text and bounding box
        coordinates of a page of a PDF file.

        Arguments:
                - page_num (int): The page to read text from.

        Returns:
                - (PageInfo): Regions with text and coordinates data.
        """
        words = self.doc[page_num].get_text("words")
        regions = [RegionInfo(coordinates=x[:4], text=x[4]) for x in words]
        self.page_details.regions = regions
        self.page_details.page = page_num
        return self.page_details


def add_file(subset_label: GroundTruth, file: FileInfo):
    """
    This method updates the files attribute of the subset.
    It adds the new labels to the subsets
    Arguments:
            - subset_label (GroundTruth): The ground truth of the susbet
            - file (FileInfo): The ground truth from the file

    Returns:
            - (None)
    """
    file_list = subset_label.files
    file_list.append(file)


def generate_synthetic_data_ocr(
    dataset: str,
    version: str,
    subset: str,
    data_dir: Optional[str],
    label_file: Optional[str],
) -> None:
    """
    This function generates an augmented dataset.
    It applies noise to a specific pages in a pdf.
    The new files are stored with the originals with new names.
    The label file is also updated with the new files

    Arguments:
            - dataset (str): The dataset of choice
            - version (str): The version of the dataset
            - subset (str): The subset of choice
            - data_dir Optional[str]: Path to the pdf files
            - label_file Optional[str]: Path to the label file

    Returns:
            - (None)
    """
    config = load_config()
    augmentations = [Noise.salt.value, Noise.pepper.value, Noise.salt_pepper.value]
    if not data_dir and not label_file:
        download_dataset = Download(dataset, [subset], version)
        download_dataset.create_local_directory_structure()
        download_dataset.download_dataset()
        data_dir = os.path.join(
            config.cache_dir,
            config.root_dataset,
            dataset,
            version,
            subset,
            config.dt_dir,
        )
        label_file = os.path.join(
            config.cache_dir,
            config.root_dataset,
            dataset,
            version,
            subset,
            config.gt_dir,
            config.gt_file,
        )

    with open(label_file) as f:
        loaded_label = yaml.load(f, Loader=yaml.UnsafeLoader)

    ground_truth = GroundTruth(**loaded_label)
    new_ground_truth = ground_truth.copy(deep=True)

    for file in tqdm(ground_truth.files):
        file_data = file
        file_path = join(
            config.cache_dir,
            config.root_dataset,
            dataset,
            version,
            subset,
            config.dt_dir,
            file_data.filename,
        )
        synth_page = SynthDataProcessor(pdf_path=file_path)
        file_reader = PDFReader(pdf_path=file_path)

        for aug in augmentations:
            pages = []
            for page in file_data.pages:
                if aug == Noise.salt.value:
                    synth_page.add_salt_noise(page.page)
                elif aug == Noise.pepper.value:
                    synth_page.add_pepper_noise(page.page)
                elif aug == Noise.salt_pepper.value:
                    synth_page.add_salt_pepper_noise(page.page)

                pages.append(file_reader.get_page_data(page.page))

            name = file_data.filename.split(".pdf")[0]
            filename = "{name}_{aug}.pdf"
            filename = filename.format(name=name, aug=aug)
            new_file = FileInfo(filename=filename, pages=pages)
            add_file(new_ground_truth, new_file)

    with open(label_file, "w") as output_file:
        yaml.dump(new_ground_truth.dict(), output_file, sort_keys=False)
