import glob
import logging
import os
from pathlib import Path
from typing import Optional

import typer
import yaml
from dsd.hydra.benchmarks.blinding.blinding_config import (
    BlindingAnnotation,
    FileTypes,
    Labels,
)
from dsd.hydra.benchmarks.blinding.synthetic_data_generation.generate_helper import (
    assign_coordinates,
    assign_entities,
    get_font_details,
    get_match_found_df,
    get_raw_df,
    get_synthetic_df,
    insert_replacement_text,
    resolve_conflicts,
)
from dsd.hydra.benchmarks.blinding.synthetic_data_generation.utils import (
    dict_to_json,
    get_synthetic_file_name,
)
from dsd.hydra.benchmarks.blinding.utils import get_target_df, read_document

logger = logging.getLogger(__name__)


def generate(input_dir: str, output_dir: Optional[str]) -> None:
    """
    Function to process all pdf files in the input directory and generate corresponding
    synthetic data files
    Argument(s):
        - input_dir(str): path of directory containing the original pdf files
        - output_dir(Optional[str]): path to store synthetic pdf files in

    Assume input_dir contains directories
    with the raw pdf and yaml files
    """

    # Convert input directory path to absolute path
    input_dir_abs = os.path.abspath(input_dir)

    # Create an empty dictionary to hold file name mappings
    file_name_mapping = {}

    # Loop through each directory in the input directory
    for directory in os.listdir(input_dir):

        # Create the full file directory path
        file_directory_path = os.path.join(input_dir_abs, directory)
        logger.info(f"Processing directory: {file_directory_path}")

        # Try to find a YAML file in the directory
        try:
            yaml_file_path = glob.glob(
                os.path.join(file_directory_path, f"*{FileTypes.yml.value}")
            )[0]
        except IndexError:
            # Exit the program if a YAML file is not found
            typer.Exit()

        # Load the YAML file as a BlindingAnnotation object
        annotations = BlindingAnnotation(
            **dict(yaml.load(open(yaml_file_path), yaml.Loader))
        )

        # Extract the target entities from the
        # BlindingAnnotation object and convert them to a dataframe
        target_df = get_target_df(annotations.entities)

        # Try to find a PDF file in the directory
        try:
            raw_pdf_path = glob.glob(
                os.path.join(file_directory_path, f"*{FileTypes.pdf.value}")
            )[0]
        except IndexError:
            # Exit the program if a PDF file is not found
            typer.Exit()

        # Read the raw PDF
        raw = read_document(raw_pdf_path)

        # convert the raw pdf into a dataframe with the necessary information
        raw_df = get_raw_df(raw)

        # Match the target entities with the raw text and create a dataframe of matches
        matched_df = get_match_found_df(raw_df, target_df)

        # resolve conflicts in matching
        matched_df = matched_df[matched_df[Labels.match_found.value]]
        matched_df[
            [Labels.p_match_found.value, Labels.target_matched.value]
        ] = matched_df.apply(
            lambda r: resolve_conflicts(
                r[Labels.text.value],
                r[Labels.p_match_found.value],
                r[Labels.target_matched.value],
            ),
            axis=1,
        )

        # select required columns
        matched_df = matched_df[
            [
                Labels.text.value,
                Labels.coordinates.value,
                Labels.page.value,
                Labels.p_match_found.value,
                Labels.target_matched.value,
            ]
        ]

        # assign entities
        matched_df[Labels.entity_types.value] = matched_df.apply(
            lambda row: assign_entities(
                row[Labels.target_matched.value], row[Labels.page.value], target_df
            ),
            axis=1,
        )

        # assign coordinates to each target after resolving conflicts
        matched_df[Labels.exact_coordinates.value] = matched_df.apply(
            lambda row: assign_coordinates(
                raw,
                row[Labels.page.value],
                row[Labels.text.value],
                row[Labels.target_matched.value],
                row[Labels.coordinates.value],
                row[Labels.p_match_found.value],
            ),
            axis=1,
        )

        # synthetic data dataframe with replacement text
        synthetic_data = get_synthetic_df(matched_df)

        # get original font details of targets from the pdf
        synthetic_data = get_font_details(synthetic_data, raw)

        # insert replacement texts in raw pdf in place of targets and get the pdf
        # with synthetic data
        generate_synthetic_pdf = insert_replacement_text(synthetic_data, raw)

        # get synthetic file name
        synthetic_filename_pdf = get_synthetic_file_name(synthetic_data)

        # add the mapping of raw and synthetic file names to the mapping dict
        file_name_mapping[Path(yaml_file_path).stem] = synthetic_filename_pdf

        # create output dir if it does not exist
        if not os.path.exists(output_dir):
            os.mkdir(output_dir)

        # save the synthetic pdf in the output dir
        out_file_pdf = os.path.join(output_dir, synthetic_filename_pdf)
        generate_synthetic_pdf.save(out_file_pdf)

    # save the mappings of complete subset in a json
    dict_to_json(file_name_mapping, output_dir)
