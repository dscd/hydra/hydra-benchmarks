Description:
The objective of this specific unit test is to ensure that the function analyze_prediction in main.py performs accurately when called upon to process a typical file input via the OCR module.
The Ground Truth and prediction each has one file with Region to be modified as Empty. The expected output should be 1 file with all values in metrics as 0 except CER.CER should be 1.0 for OCR module and pages will be [].
