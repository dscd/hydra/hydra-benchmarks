Description:
The objective of this specific unit test is to ensure that the function analyze_prediction in main.py performs accurately when called upon to process a typical file input via the tabular module.
The Ground Truth File has one file with all values populated whereas the prediction file is modified to have files:[]. The expected output in this scenario should have all metrics as 0 and files [].
