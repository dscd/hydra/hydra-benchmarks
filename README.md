## HYDRA Benchmarks

HYDRA Benchmarks is a CLI Tool to perform basic download, run and compare operations on the HYDRA Module Benchmark datasets.

## Quick Usage Guide

- Clone the repository using either the HTTPS/SSH link:
`git clone <link>`

- Create the conda environment

```
$cd hydra-benchmarks
$conda install -f environment.yml
```

- Activate the conda environment

`source activate hydra-benchmarks`

- Add the .env file containing the Minio credentials to the root folder in this format:

```
MINIO_URL=
MINIO_ACCESS_KEY=
MINIO_SECRET_KEY=
```

Command to fetch the Minio credentials: `cat /vault/secrets/minio-standard-tenant-1`

- Install the hydra-benchmarks package:

`pip install -e .`

More details on setting up: https://gitlab.k8s.cloud.statcan.ca/data-science-division/dsmqr-mqrsd/hydra/hydra-module-template/-/wikis/Getting-Started

The Hydra benchmarks CLI consists of eight commands:

* `download`: Downloads the dataset and ground truth for each module
* `run-dataset`: Generates the prediction file by running an algorithm on a downloaded dataset
* `run-folder` : Generates the prediction file by running an algorithm on a dataset path entered by the user
* `compare` : Generates a detection file by comparing the ground truth with the prediction file
* `report` : Generates a report of the metrics based on the detection file
* `generate` : Generates synthetic data based on a dataset and subset
* `blinding`: Commands specific to the blinding module
  * `redact` : redacts a document based on the coordinates detected by run command's prediction file
  * `annotate` : annotate a document based on the coordinates detected by run command's prediction file

**Usage**:

```console
$ hydra-benchmarks [OPTIONS] COMMAND [ARGS]...
```

**Options**:

* `--install-completion`: Install completion for the current shell.
* `--show-completion`: Show completion for the current shell, to copy it or customize the installation.
* `--help`: Show this message and exit.

**Commands**:

1. `hydra-benchmarks download`

   The download command downloads the particular dataset, version and subset from MinIO based on user arguments and stores them locally in the current directory. The command includes options for downloading only the dataset or the ground truth file or both.

   **Usage**:

   ```
   $ hydra-benchmarks download [OPTIONS] DATASET VERSION SUBSETS...
   ```

   **Arguments**:
   * `DATASET`: name of the hydra module \[required\]
   * `VERSION`: version of the dataset to be used \[required\]
   * `SUBSETS...`: list of subset of the dataset to be used \[required\]

   **Options**:
   * `--label / --no-label`: download only ground truth file \[default: no-label\]
   * `--data / --no-data`: download only data files \[default: no-data\]
   * `--help`: Show this message and exit.
2. `hydra-benchmarks run_folder`

   \
   The run_folder command can be used to run an algorithm on the downloaded dataset. The output of the run_folder command is called a `prediction file`

   **Usage**:

   ```
   $ hydra-benchmarks run [OPTIONS] DATASET ALGORITHM_NAME INPUT_DIR... OUTPUT_DIR MODEL_NAME
   ```

   **Arguments**:
   * `DATASET`: name of the hydra module \[required\]
   * `ALGORITHM_NAME`: algorithm to be used \[required\]
   * `INPUT_DIR...`: directory where the data files are stored \[required\]
   * `OUTPUT_DIR`: directory where predictions will be stored \[required\]

   **Options**:
   * `--ner-threshold FLOAT`: threshold value for the NER model
   * `--model-name TEXT`: type of model to be used
   * `--help`: Show this message and exit.
3. `hydra-benchmarks run-dataset`

   \
   The run-dataset command can be used to run an algorithm on the downloaded dataset. The output of the run_dataset command is called a `prediction file`

   **Usage**:

   ```
   $ hydra-benchmarks run-dataset [OPTIONS] DATASET VERSION SUBSET ALGORITHM_NAME OUTPUT_DIR
   ```

   **Arguments**:
   * `DATASET`: name of the hydra module \[required\]
   * `VERSION`: version of the dataset to be used \[required\]
   * `SUBSETS...`: list subset of the dataset to be used \[required\]
   * `ALGORITHM_NAME`: algorithm to be used \[required\]
   * `OUTPUT_DIR`: directory where predictions will be stored \[required\]

   **Options:**
   * `--ner-threshold FLOAT`: threshold value for the NER model
   * `--output-filename TEXT`: name of prediction file
   * `--model-name TEXT`: type of model to be used
   * `--help`: Show this message and exit.
4. `hydra-benchmarks run-folder`\
   \
   The run-folder command can be used to run an algorithm on a folder path specified by the user (input_dir)\
   \
   **Usage**:

   <pre>

   <span dir="">$ </span>hydra-benchmarks run-folder \\\[OPTIONS\\\] DATASET ALGORITHM_NAME INPUT_DIR OUTPUT_DIR

   </pre>\*\*Arguments\*\*:
   * `DATASET`: name of the hydra module \[required\]
   * `ALGORITHM_NAME`: algorithm to be used \[required\]
   * `INPUT_DIR`: directory where the data files are stored
   * `OUTPUT_DIR`: directory where predictions will be stored \[required\]

   **Options**:
   * `--ner-threshold FLOAT`: threshold value for the NER model
   * `--model-name TEXT`: type of model to be used
   * `--help`: Show this message and exit.
5. `hydra-benchmarks compare`

   The compare command is used to compare one or more prediction files against the ground truth and generates a file with the metrics per subset and algorithm. The output of the compare command is called a `detection file`

   **Usage**:

   ```
   $ hydra-benchmarks compare [OPTIONS] PRED_FILES... OUTPUT_DIR
   ```

   **Arguments**:
   * `PRED_FILES...`: List of prediction files \[required\]
   * `OUTPUT_DIR`: Location of compare output results \[required\]

   **Options**:
   * `--region-level TEXT`: Process block of text / table or cells or words \[default: regions\]
   * `--ious FLOAT`: IOU threshold values for evaluation \[default: 0.5, 0.6, 0.7, 0.8, 0.9, 0.95\]
   * `--help`: Show this message and exit.
6. `hydra-benchmarks report`

   The report command is used to display the Metrics in a Dash app based on the output of the detection file.

   **Usage**:

   ```console
   $ hydra-benchmarks report [OPTIONS] DETECTION_FILE_PATH
   ```

   **Arguments**:
   * `DETECTION_FILE_PATH`: Path of the detection file \[required\]

   **Options**:
   * `--help`: Show this message and exit.
7. `hydra-benchmarks generate`

   The generate command is used to generate new or augmented data based on a subset from either the blinding or ocr dataset.

   **Usage**:

   ```console
   $ hydra-benchmarks generate [OPTIONS] DATASET VERSION SUBSETS OUTPUT_DIR
   ```

   **Arguments**:
   * `DATASET`: name of the hydra module \[required\]
   * `VERSION`: version of the dataset to be used \[required\]
   * `SUBSETS`: list subset of the dataset to be used \[required\]

   **Options**:
   * `--help`: Show this message and exit.
   * `OUTPUT_DIR`: Location of synthetic data
8. `hydra-benchmarks blinding`

   The blinding command is a parent command to run subcommands annotate, redact and generate.

   **Usage**:

   ```
   $ hydra-benchmarks blinding [OPTIONS] COMMAND [ARGS]...
   ```

   **Options**:
   * `--help`: Show this message and exit.

   **commands**:
   * `annotate`: Command to annotate documents based on the...
   * `generate`: Command to generate synthetic data
   * `redact`: Command to redact documents based on the...

   ### `a) hydra-benchmarks blinding annotate`

   Command to annotate documents based on the prediction file. For blinding this command highlights the sensitive information detected by the model by drawing a bounding box around it.

   **Usage**:

   ```
   $ hydra-benchmarks blinding annotate [OPTIONS] PRED_FILES...
   ```

   **Arguments**:
   * `PRED_FILES...`: List of prediction files \[required\]

   **Options**:
   1. `--out-dir TEXT`: Directory where annotated files will be stored \[default: annotated_documents\]
   2. `--help`: Show this message and exit.

   ### `b) hydra-benchmarks blinding redact`

   Command to redact documents based on the prediction file. For the blinding functionality this command removes the sensitive information detected by the model and blinds it.

   **Usage**:

   ```
   $ hydra-benchmarks blinding redact [OPTIONS] PRED_FILES...
   ```

   **Arguments**:
   * `PRED_FILES...`: List of prediction files \[required\]

   **Options**:
   1. `--out-dir TEXT`: Directory where annotated files will be stored \[default: annotated_documents\]
   2. `--help`: Show this message and exit.


### How to Contribute

See [CONTRIBUTING.md](CONTRIBUTING.md)

### License

Unless otherwise noted, the source code of this project is covered under Crown Copyright, Government of Canada, and is distributed under the [MIT License](LICENSE).

The Canada wordmark and related graphics associated with this distribution are protected under trademark law and copyright law. No permission is granted to use them outside the parameters of the Government of Canada's corporate identity program. For more information, see [Federal identity requirements](https://www.canada.ca/en/treasury-board-secretariat/topics/government-communications/federal-identity-requirements.html).

______________________
