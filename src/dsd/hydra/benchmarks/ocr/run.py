import os
import time
from os.path import join

import yaml
from dsd.hydra.benchmarks.config.api import load_config
from dsd.hydra.benchmarks.models.schemas import (
    FileInfo,
    GroundTruth,
    PageInfo,
    Prediction,
)
from dsd.hydra.benchmarks.modules.utils import save_file
from dsd.hydra.benchmarks.ocr.file_reader import FileReader
from dsd.hydra.benchmarks.ocr.utils import get_ocr_helper
from tqdm.auto import tqdm


def generate_prediction(ground_truth: GroundTruth, algorithm_name: str) -> Prediction:
    """
    This function takes the above arguments and creates a prediction file.
    In this function, a subset of a dataset is parsed through an OCR engine
    and the output, with necessary data, are stored in the created yaml file.

    Parameters:
        - ground_truth (GroundTruth): A GroundTruth object
        - algorithm_name (str): A string of the OCR engine name
    Returns:
        - Prediction
    """
    loc = load_config()
    cache_dir = loc.root_dataset
    minio_dt_dir = loc.dt_dir

    # Get OCR Engine
    OCR = get_ocr_helper(algorithm_name)
    dataset_start_time = time.time()

    files = []

    dataset = ground_truth.dataset
    subset = ground_truth.subset
    version = ground_truth.version

    # Loop through every file in the groundtruth file
    for data in tqdm(ground_truth.files, desc="Processing files"):
        file_data = FileInfo(filename=data.filename, pages=[])
        file_path = join(
            loc.cache_dir,
            cache_dir,
            dataset,
            version,
            subset,
            minio_dt_dir,
            data.filename,
        )
        image_reader = FileReader(file_path)
        file_pages_list = []

        # Loop through every page in the file
        for page in data.pages:
            page_no = page.page
            image = image_reader.reader(page_no)
            file_start_time = time.time()
            ocr_output_regions = OCR.get_regions(image)
            file_end_time = time.time()
            file_pages_list.append(PageInfo(page=page_no, regions=ocr_output_regions))
            file_pages_list.append(PageInfo(page=page_no, regions=ocr_output_regions))

        file_data.pages = file_pages_list
        # Calculate file processing time
        file_data.exec_time = file_end_time - file_start_time
        files.append(file_data)

    # Add predictions to data structure
    Output = Prediction(
        dataset=dataset,
        subset=subset,
        algorithm_name=algorithm_name,
        version=version,
        files=files,
    )
    dataset_end_time = time.time()

    # Calculate dataset processing time
    Output.exec_time = dataset_end_time - dataset_start_time
    Output.files = files

    return Output


def run_ocr_prediction_pipeline(
    groundtruth_file_path: str,
    algorithm_name: str,
    output_dir: str,
    output_filename: str = "output_{subset}_{algorithm_name}.yml",
) -> None:
    """
    This function takes the above arguments and creates a prediction file.
    In this function, a subset of a dataset is parsed through an OCR engine
    and the output, with necessary data, are stored in the created yaml file.

    Parameters:
        - groundtruth_file_path (str): A string with the path of the GroundTruth file.
        - algorithm_name (str): A string of the OCR engine name
        - output_dir (str): A string containing the output directory path
    Returns:
        - None
    """

    # Open Groundtruth File
    with open(groundtruth_file_path) as f:
        data_loaded = yaml.load(f, Loader=yaml.UnsafeLoader)

    # Parse Groundtruth file to GroundTruth Object
    ground_truth = GroundTruth(**data_loaded)

    # Generate predictions
    prediction = generate_prediction(ground_truth, algorithm_name)
    subset = prediction.subset

    # Define prediction filename
    output_filename = output_filename.format(
        subset=subset, algorithm_name=algorithm_name
    )

    # Create output directory
    os.makedirs(output_dir, exist_ok=True)

    # Write prediction to yaml file
    save_file(prediction, output_filename, output_dir)


def run_ocr_prediction_pipeline_folder(
    algorithm_name: str,
    input_dir: str,
    output_dir: str,
) -> None:
    """
    This function takes the above arguments and creates a prediction file.
    In this function, input directory path is passed to get the data
    and label file.
    After processing the input, an output, with necessary data is
    stored in the created yaml file.

    Parameters:
        - algorithm_name (str): A string of the OCR engine name
        - input_dir (str): A string containing the input data directory path
        - output_dir (str): A string containing the output directory path
    Returns:
        - None
    """
    config = load_config()
    groundtruth_file_path = join(input_dir, config.gt_dir, config.gt_file)

    run_ocr_prediction_pipeline(groundtruth_file_path, algorithm_name, output_dir)
