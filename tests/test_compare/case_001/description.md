Description:
The objective of this specific unit test is to ensure that the function analyze_prediction in main.py performs accurately when called upon to process a typical file input via the tabular module.The Ground Truth File and the prediction file contains multiple pages and regions.The Expected Output should have multiple files and multiple Regions.
