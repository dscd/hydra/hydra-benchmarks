from typing import Dict, List, Optional, Tuple
from pydantic import BaseModel, Field
from pyqtree import Index
from enum import Enum

# Compare Output


"""
Algorithm
    |_____metrics: List[Metrics] #subset level
    |_____files: List[FileInfo]
         |____name
         |____List[Metrics]
         |____List[PageInfo]
            |_____page
            |_____List[Metric]
                |__iou, detection, quality
"""


class DetectionMetrics(BaseModel):
    """
    DetectionMetrics class that contains evaluation of algorithms in their ability to
    detect regions of interest to annotate

    Attributes:
        fn (int): Number of false negatives
        fp (int): Number of false positives
        tp (int): Number of true positives
        rc (int): Number of regions
        fc (int): Number of files
        p (float): Precision value
        r (float): Recall value
        f1 (float): F1-score
    """

    fn: int  # False Negative
    fp: int  # False Positive
    tp: int  # True Positive
    rc: int  # Region Counts
    fc: int  # File Counts
    p: float  # Precision
    r: float  # Recall
    f1: float  # F1-Score


class QualityMetrics(BaseModel):
    """
    QualityMetrics class that contains evaluation of algorithms in their ability to
    extract text from the annotated regions of interest

    Attributes:
        ta (float): Fraction of words correctly extracted among the actual number of
            words
        cer (float): Measure of the difference between the actual word and the
            prediction based on number of edits required
    """

    ta: float  # Text accuracy
    cer: float  # Character error rate


class Metric(BaseModel):
    """
    Metric class representing evaluation of an algorithm on the page, file
    or subset level

    Attributes:
        iou (float): The IoU threshold to calculate the metrics
        detection (DetectionMetrics): The detection metrics of the prediction output
        quality (QualityMetrics): The extraction based metrics of the prediction output
    """

    iou: float
    detection: DetectionMetrics
    quality: QualityMetrics


class PageInfo(BaseModel):
    """
    PageInfo class representing evaluation of an algorithm on a page

    Attributes:
        page (int): Page number on the document
        page_metrics (List[Metric]): List of Metrics at multiple IOU thresholds
    """

    page: int
    page_metrics: List[Metric]


class FileInfo(BaseModel):
    """
    FileInfo class representing evaluation of an algorithm on a file

    Attributes:
        name (str): Name of the file
        file_exec_time (float): Execution time for predictions of the entire file
        file_metrics (List[Metric]): List of Metrics object for each IOU threshold on a
            file
        pages (List[PageInfo]): List of PageInfo objects for each page of the file
    """

    name: str
    file_exec_time: float
    file_metrics: List[Metric]
    pages: List[PageInfo]


class Algorithm(BaseModel):
    """
    Algorithm class representing evaluation of an algorithm on an entire subset

    Attributes:
        name (str): Name of the algorithm
        dataset (str): The primary type of dataset - blinding/ocr/tabular
        version (str): Version of the dataset whose evaluation is generated
        subset (str): Subset within the dataset of a particular version
        subset_exec_time (float): Execution time for the prediction of the entire subset
        subset_metrics (List[Metric]): List of Metric objects for each IOU threshold on
            a subset
        files (List[FileInfo]): List of FileInfo objects for each file in the subset
    """

    name: str
    dataset: str
    version: str
    subset: str
    subset_exec_time: float
    subset_metrics: List[Metric]  # This will store the subset level metrics
    files: List[FileInfo]  # This will store the file level metrics.
    # For each file we can store an iou based metric

    class Config:
        arbitrary_types_allowed = True


class CompareOutput(BaseModel):
    """
    CompareOutput class for evaluation of each algorithm for the corresponding subsets

    Attributes:
        output (List[Algorithm]): List of Algorithm objects for each algorithm that is
            evaluated
    """

    output: List[Algorithm]


# Connected Components
"""
FileComponents
    |_____name
    |_____List[IOUComponents]
         |____iou threshold
         |____List[PageComponents]
            |_____page
            |_____Components
"""


class Components(BaseModel):
    """
    Component class that represents a pair of Prediction and actual Groundtruth

    Attributes:
        pkey (Optional[str]): Key to represent Prediction
        tkey (Optional[str]): Key to represent Groundtruth
        ptext (Optional[str]): The text contained in the prediction bounding box
        ttext (Optional[str]): The text contained in the ground truth bounding box
        iou (Optional[float]): The IoU of the prediction and groundtruth bounding boxes
        pbb (Tuple[float, float, float, float]): Coordinates of the prediction text box
        tbb (Tuple[float, float, float, float]): Coordinates of the groundtruth text box
        tp (Optional[bool]): Boolean to denote if the given pair is a true positive
        fp (Optional[bool]): Boolean to denote if the given pair is a false positive
        fn (Optional[bool]): Boolean to denote if the given pair is a false negative
    """

    pkey: Optional[str]  # Prediction key
    tkey: Optional[str]  # Ground Truth key
    ptext: Optional[str]  # Prediction Text
    ttext: Optional[str]  # Ground Truth Text
    iou: Optional[float]  # IOU value between prediction and ground truth bounding box
    pbb: Tuple[float, float, float, float]  # Prediction Bounding Box
    tbb: Tuple[float, float, float, float]  # Ground Truth Bounding Box
    tp: Optional[bool]  # True Positive flag
    fp: Optional[bool]  # False Positve flag
    fn: Optional[bool]  # False Negative flag


class PageComponents(BaseModel):
    """
    PageComponents class to represent pairs of groundtruth and prediction in a page of
    the document

    Attributes:
        page (int): Page number in the document file
        components (List[Components]): List of Components pairs oF groundtruth and
            prediction
    """

    page: int
    components: List[Components]


class IOUComponent(BaseModel):
    """
    IOUComponent class to represent evaluation of pairs of groundtruth and prediction of
    a file at a given IoU threshold

    Attributes:
        iou_thr (float): IoU threshold to evaluate performance
        pages (List[PageComponents]): List of PageComponents for each page in the
            document
    """

    iou_thr: float
    pages: List[PageComponents]


class FileComponents(BaseModel):
    """
    FileComponents class to represent the evaluation of pairs of ground truth and
    prediction pairs at every given IoU threshold

    Attributes:
        name (str): Name of the file
        ious (List[IOUComponent]): List of IOUComponent for the evaluation at all given
            thresholds of IoU of a file
    """

    name: str  # File name
    ious: List[IOUComponent]


class ConnectedComponents(BaseModel):
    """
    Connected components class containing connectected components pairs of all files
    in a subset to evaluate on an algorithms

    Arguments
        algorithm_name (str): Name of the algorithm being evaluated
        dataset (str): Primary type of dataset - tabular/ocr/blinding
        version (str): Version of the dataset
        subset (str): Subset within the dataset of a particular version
        files (List[FileComponents]): List of FileComponents to represent evaluation of
            all files in the subset
    """

    algorithm_name: str
    dataset: str
    version: str
    subset: str
    files: List[FileComponents]


class BBoxInfo(BaseModel):
    """
    BboxInfo objext to denote one bounding box and contained text

    Attributes:
        bounding_box (Tuple[float, float, float, float]): The bounding box coordinates
            of the text box
        text (Optional[str]): Text contained within a bounding box
    """

    bounding_box: Tuple[float, float, float, float]
    text: Optional[str]


class IOU(BaseModel):
    """
    IOU class containing IoU between two bounding boxes

    Attributes:
        parent (str): A bounding box key whose intersecting bounding boxes are
            retrieved from the constructed trees
        child (str): A bounding box key that intersects with the parent bounding box
        iou_value (float): The IoU value between the parent and child bounding boxes
    """

    parent: str
    child: str
    iou_value: float


class CalculatedIOU(BaseModel):
    """
    CalculatedIOU class containing IoU of parent and all overlapping bounding boxes
    from tree.

    Attributes:
        iou (List[IOU]): List of IOU objects containing IoU of a parent and all children
    """

    iou: List[IOU] = Field(default_factory=list)


class PageRegions(BaseModel):
    """
    PageRegions class containing all bounding boxes, their respective keys and text.

    Attributes:
        pred_keys (List[str]): List of identifiers of all prediction bounding boxes
        node_keys (List[str]): List of identifiers of all prediction and groundtruth
            identifiers
        bbox_dict (Dict[str, BBoxInfo]): Dictionary to hold all bboxes of a file
        pred_tree (pyqtree.Index): Tree of all prediction bounding boxes
        truth_tree (pyqtree.Index): Tree for all ground truth bounding boxes
        pred_text (Optional[str]): Predicted text within the region
        truth_text (Optional[str]): The actual text wihin the region
    """

    # Prediction bbox keys
    pred_keys: List[str]
    # List of pred and gt keys
    node_keys: List[str]
    # Local dictionary to hold all bboxes of file
    bbox_dict: Dict[str, BBoxInfo]
    # Pred tree
    pred_tree: Index
    # GT tree
    truth_tree: Index
    # Pred text
    pred_text: Optional[str]
    # Truth text
    truth_text: Optional[str]

    # Config class to allow pyqtree.Index classes
    class Config:
        arbitrary_types_allowed = True


class RegionLevels(Enum):
    """
    Enumeration of all Regions Levels:
        - regions: For all regions - tables, OCR text or Blinding text
        - regions_text: For cells within tables
    """

    regions = "regions"
    regions_text = "regions_text"
