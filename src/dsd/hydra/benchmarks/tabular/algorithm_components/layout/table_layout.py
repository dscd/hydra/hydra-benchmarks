from dsd.hydra.benchmarks.tabular.algorithm_components.layout.slice import SLICE
from dsd.hydra.benchmarks.tabular.models.tabular_schemas import LayoutMethod


def init_layout(layout_method, config):
    layout = 0
    if layout_method == LayoutMethod.slice.value:
        layout = SLICE(config.horizontal_window, config.vertical_window)
    return layout
