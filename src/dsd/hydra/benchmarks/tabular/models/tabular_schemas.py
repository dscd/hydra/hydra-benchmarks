"""Schemas to be used in the tabular module


@authors : Johan Fernandes, Niranjan Ramesh
"""

import fitz
import numpy as np
from enum import Enum
from typing import List, Optional, Tuple, Any
from pydantic import BaseModel


class TabularCell(BaseModel):
    """
    TabularCell class containing metadata about a cell of a table

    Attributes:
        cell_coordinates (Optional[Tuple[float, float, float, float]]): Tuple containing
            the coordinates of the table cell in (x1, x2, y1, y2) format
        cell_text (Optional[str]): Text contained in the cell
        row_id (Optional[int]): Row index the cell belongs to in the table
        col_id (Optional[int]): Column index the cell belongs to in the table
    """

    cell_coordinates: Optional[Tuple[float, float, float, float]]
    cell_text: Optional[str]
    row_id: Optional[int]
    col_id: Optional[int]


class TabularAlgorithmComponents(BaseModel):
    """
    TabularAlgorithmComponents class containing configurations for the pipeline
    algorithm for tabular detection and extraction

    Attributes:
        tabledet_model (str): The model architecture to use for table detection
        tabledet_model_cfg (str): Name of pre-trained table detection model config
        tabledet_model_weight (str): Name of pre-trained weights for table detection
            model
        extraction (str): Module to use for extraction of text from detected tables
        layout (str): Module to use for analysing layout of text and constructing the
            table structure
    """

    tabledet_model: str
    tabledet_model_cfg: str
    tabledet_model_weight: str
    extraction: str
    layout: str


class TabularAlgorithm(BaseModel):
    """
    TabularAlgorithm class encapsulating the configurations for the pipeline algorithm
    for tabular detection and extraction

    Attributes:
        algorithm_name (str): Name of algorithm for table detection and extraction
        components (TabularAlgorithmComponents): Components of the pipeline

    """

    algorithm_name: str
    components: TabularAlgorithmComponents


# Extracted content of a single image / pdf page
class ExtractedContent(BaseModel):
    """
    ExtractedContent class containing the extracted text, bounding boxes and the image
    of the PDF page

    Attributes:
        image (Optional[fitz.fitz.Pixmap]): Image of the PDF page
        lines_bboxes (Optional[List[Tuple[float, float, float, float]]]): Bounding boxes
            of all lines of the page
        word_bboxes ( List[Tuple[float, float, float, float]]): Bounding boxes of all
            words in the page
        word_text (List[str]): The contained text in all word bounDing boxes
    """

    image: Optional[fitz.fitz.Pixmap] = None
    lines_bboxes: Optional[List[Tuple[float, float, float, float]]] = None
    word_bboxes: List[Tuple[float, float, float, float]]
    word_text: List[str]

    # Config class to allow fitz classes
    class Config:
        arbitrary_types_allowed = True


class TabularImageScale(BaseModel):
    """
    TabularImageScale class to convert generated PDF image dimentions to original image
    dimensions

    Attributes:
        img_width_scale (float): Scale of the width
        img_height_scale (float): Scale of the height
    """

    img_width_scale: float
    img_height_scale: float


class SliceTableContents(BaseModel):
    """
    SliceTableContents for Slice generated image based on table size and its dimensions

    Attributes:
        image (Any): Black and white image with text boxes drawn
        image_width (float): Width of the new image
        image_height (float): Height of the image
    """

    image: Any
    image_width: float
    image_height: float


# Box for Table Grid layout
class GridBBox(BaseModel):
    """
    GridBBox class containing information of generated grid in SLICE

    Attributes:
        col_id (int):
        row_id (int):
        coordinates (Tuple[float, float, float, float]):
    """

    col_id: int
    row_id: int
    coordinates: Tuple[float, float, float, float]


# DocPage
class DocPage(BaseModel):
    """
    The dataset object contained in the batches fed to the models

    Attributes:
        img (np.ndarray): Image to be passed for inference
        filename (str): Name of the file being processed
        dim (Tuple[int, int]): Dimensions of the image
        page (int): Page of the document or image being processed
    """

    img: np.ndarray
    filename: str
    dim: Tuple[int, int]  # height and width of image
    page: int

    class Config:
        """
        To support validation of numpy
        """

        arbitrary_types_allowed = True


# Collated batch of DocPage
class BatchData(BaseModel):
    """
    A complete batch of DocPage(s)

    Attributes:
        images (List[np.ndarray]): List of images to be passed for inference as a batch
        filenames (List[str]): List of file names in the batch
        dims (List[Tuple[int, int]]): List of dimensions of all images in the batch
        pages (List) : List of pages or images of the documents in the batch
    """

    images: List
    filenames: List
    dims: List[Tuple[int, int]]
    pages: List


class DetectionMethod(Enum):
    """
    Enum for all table detection models
    """

    table_transformer = "table_transformer"
    cascadetabnet = "cascadetabnet"
    deformable_detr = "deformable_detr"


class ExtractionMethod(Enum):
    """
    Enum for available extraction methods
    """

    pymupdf = "pymupdf"
    ocrmypdf = "ocrmypdf"
    pytesseract = "pytesseract"
    pdfplumber = "pdfplumber"


class LayoutMethod(Enum):
    """
    Enum for all layout analysis methods
    """

    slice = "slice"


class Files(Enum):
    """
    Enum for name of output files
    """

    prediction_file = "output_{}.yml"
    write_mode = "w"
    read_mode = "r"
