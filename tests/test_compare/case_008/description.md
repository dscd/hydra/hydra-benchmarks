Description:
The objective of this specific unit test is to ensure that the function analyze_prediction in main.py performs accurately when called upon to process a typical file input via the tabular module.
The Ground Truth and the Prediction File will each contain 1 file with regions modified to be []. The expected output should contain 1 file with pages as [] and all other metrics as 0.
