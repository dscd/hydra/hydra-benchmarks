Description:
The objective of this specific unit test is to ensure that the function analyze_prediction in main.py performs accurately when called upon to process a typical file input via the OCR module.
The Ground Truth and Prediction file both contain one file where Page is Empty. The expected output should be one file with all values in metrics as 0. As discussed with the team, CER will be 1.0 for OCR module.
