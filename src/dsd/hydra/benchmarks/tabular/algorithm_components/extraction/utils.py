"""Extract table within PDF page
   Contains a set of functions to format and extract
   content of a page that has a table within it.


@authors : Johan Fernandes, Niranjan Ramesh
"""

import numpy as np
from typing import Union, Tuple

from dsd.hydra.benchmarks.tabular.models.tabular_schemas import (
    ExtractedContent,
    TabularImageScale,
)
from dsd.hydra.benchmarks.tabular.eval.eval_utils import overlap_percentage


def get_img_scale(
    page_img: np.array,
    org_img_width: Union[int, float],
    org_img_height: Union[int, float],
) -> TabularImageScale:
    """Returns the scale of the image after a page from pdf is converted to image

    Arguments:
        page_img (np.array): Image of the page as a numpy array
        org_img_width (Union[int,float]): original width of the image that was
                                            provided
        org_img_height (Union[int,float]: original height of the image that was
                                            provided

    Returns:
        TabularImageScale : Tuple of image width scale and height scale
    """

    # The new width and height of the image that was formerly a page in the pdf
    new_img_width, new_img_height = page_img.width, page_img.height
    # Scale of the original width to which the page image from pdf needs to be
    # transformed
    img_width_scale = org_img_width / new_img_width
    # Scale of the original height to which the page image from pdf needs to be
    # transformed
    img_height_scale = org_img_height / new_img_height

    return TabularImageScale(
        img_width_scale=img_width_scale, img_height_scale=img_height_scale
    )


def rescale_document_content(
    document_content: ExtractedContent,
    img_scale: TabularImageScale,
    org_img_xmin: Union[int, float] = None,
    org_img_ymin: Union[int, float] = None,
) -> ExtractedContent:
    """Transform the bboxes of the lines and words to the original image dimension
       If table_img is provided then the image is also rescaled to original
       image size as an additional step

    Arguments:
        document_content (ExtractedContent) : list of words and line locations in
            the page
        img_scale (TabularImageScale) : Scale (width, height) of image to be scaled to
            1. For the case of ocrmypdf, this will be the orignal image scale
            2. For the case of pymupdf, this will be the table image scale
        org_img_xmin (float) : For pymupdf, this is the xmin of the table image
            1. This need to be provided for pymupdf to reorient the word bboxes
            to the original image scale
        org_img_ymin (float) : For pymupdf, this is the ymin of the table image
            1. This need to be provided for pymupdf to reorient the word bboxes
            to the original image scale

    Returns:
        rescale_content (ExtractedContent) : Rescaled word and line locations
    """

    word_bboxes = []

    # First transform the words
    for word_bbox in document_content.word_bboxes:
        x1, y1, x2, y2 = word_bbox
        x1 *= img_scale.img_width_scale
        y1 *= img_scale.img_height_scale
        x2 *= img_scale.img_width_scale
        y2 *= img_scale.img_height_scale

        # Check if original image width is provided.
        # It will be provided along with the height for table_imgs
        if org_img_xmin is not None:
            x1 += org_img_xmin
            y1 += org_img_ymin
            x2 += org_img_xmin
            y2 += org_img_ymin

        word_bboxes.append([x1, y1, x2, y2])

    rescaled_content = ExtractedContent(
        word_bboxes=word_bboxes, word_text=document_content.word_text
    )

    # Second transform the lines if available
    lines_bboxes = []

    if document_content.lines_bboxes is not None:

        for bbox in document_content.lines_bboxes:
            x1, y1, x2, y2 = bbox
            x1 *= img_scale.img_width_scale
            y1 *= img_scale.img_height_scale
            x2 *= img_scale.img_width_scale
            y2 *= img_scale.img_height_scale

            # Check if original image width is provided.
            # It will be provided along with the height for table_imgs
            if org_img_xmin is not None:
                x1 += org_img_xmin
                y1 += org_img_ymin
                x2 += org_img_xmin
                y2 += org_img_ymin

            lines_bboxes.append([x1, y1, x2, y2])

        rescaled_content.lines_bboxes = lines_bboxes

    return rescaled_content


def check_if_content_in_table(
    table_bbox: Tuple[float, float, float, float],
    overlap_thr: float,
    document_content: ExtractedContent,
):
    """
    Returns the words and line bboxes that belong to table regions

    Arguments:
        table_bbox (Tuple) : BBox location of table
        document_content (ExtractedContent) : list of words and line locations in
            the page

    Returns:
        updated_document_content (ExtractedContent) : Only content within a table
    """

    # Words in the page
    word_bboxes_in_table = []
    text_in_table = []

    for word_bbox, word_text in zip(
        document_content.word_bboxes, document_content.word_text
    ):
        box1 = word_bbox
        box2 = table_bbox

        overlap_box1, _ = overlap_percentage(box1, box2)

        if overlap_box1 > overlap_thr:
            word_bboxes_in_table.append(word_bbox)
            text_in_table.append(word_text)

    updated_document_content = ExtractedContent(
        word_bboxes=word_bboxes_in_table, word_text=text_in_table
    )

    # Lines in the page
    lines_bboxes_in_table = []

    if document_content.lines_bboxes is not None:
        for line_bbox in document_content.lines_bboxes:
            box1 = line_bbox
            box2 = table_bbox

            overlap_box1, _ = overlap_percentage(box1, box2)

            if overlap_box1 > overlap_thr:
                lines_bboxes_in_table.append(line_bbox)

        updated_document_content.lines_bboxes = lines_bboxes_in_table

    return updated_document_content
