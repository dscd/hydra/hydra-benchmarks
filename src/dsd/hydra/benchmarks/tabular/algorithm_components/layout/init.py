from typing import Tuple, List
from dsd.hydra.benchmarks.tabular.models.tabular_schemas import (
    ExtractedContent,
    TabularCell,
)


class TableLayout:
    def slice_table(
        table_bbox: Tuple[float, float, float, float],
        document_content: ExtractedContent,
    ) -> List[List[TabularCell]]:
        raise NotImplementedError("get_table must be implemented")
