import copy
from statistics import mean
from typing import List, Tuple

import Levenshtein
from dsd.hydra.benchmarks.models.compare_schema import (
    Algorithm,
    Components,
    DetectionMetrics,
    FileComponents,
    FileInfo,
    IOUComponent,
    Metric,
    PageComponents,
    PageInfo,
    QualityMetrics,
)


def generate_metrics_list(iou_list: List) -> List[Metric]:
    """Generate a list of metrics based on the list of
       iou values provided by the user.

    Arguments:
        - iou_list (List): List of IOU thresholds

    Returns:
        - List[Metric] : A list of Metric pydantic models each of
                          which can be used to store results at
                          different IOU levels

    """
    return [
        Metric(
            iou=iou,
            detection=DetectionMetrics(
                fn=0,
                fp=0,
                tp=0,
                rc=0,
                fc=0,
                p=0.0,
                r=0.0,
                f1=0.0,
            ),
            quality=QualityMetrics(
                ta=0.0,
                cer=0.0,
            ),
        )
        for iou in iou_list
    ]


def get_iou_component_list(iou_list: List) -> List[IOUComponent]:
    """Generate a list of metrics based on the list of
       iou values provided by the user.

    Arguments:
        - iou_list (List): List of IOU thresholds

    Returns:
        - List[IOUComponent] : A list of IOUComponent pydantic models each of
                          which can be used to store results at
                          different IOU levels to describe the Pages and
                          region connected components at that IOU level

    """
    return [IOUComponent(iou_thr=iou, pages=[]) for iou in iou_list]


class MetricsCalculator:
    def __init__(self, iou_list: List):
        """
        Create a Metric calculator class to help calculate
        file metrics and subset metrics

        Arguments:
            - iou_list (List): List of IOU thresholds

        Returns:
            - None

        """
        self.iou_list = iou_list

    def calculate_positive_negative(
        self,
        pred_key: str,
        gt_key: str,
        iou_value: float,
        iou_thr: float,
        metrics: Metric,
        component: Components,
    ) -> Tuple[Metric, Components]:
        """
        Computes the true positives, false positives and false negatives.

        Arguments:
            - pred_key (str): A key to extract bounding box coordinates
            - gt_key (str): A key to extract bounding box coordinates
            - iou_value (float): Calculated IOU value between the bounding boxes
                                derived from using pred_key and gt_key
            - iou_thr (float): IOU threshold
            - metrics (Metric) : A model to hold the TP, FP and FN calculation
                                 for a page / file
            - component (Component): A single connected component instance
                        which stores the bbox, text and id of pred and gt
                        regions that are connected.

        Returns:
            - Tuple(Metric, Components) : Returns the updated metric model for this
                        page / file along with the updated connected component

        """

        if pred_key is not None and gt_key is not None:
            if iou_value >= iou_thr:
                metrics.detection.tp += 1
                component.tp = True
            else:
                metrics.detection.fp += 1
                component.fp = True

            metrics.detection.rc += 1

        # You have found a false negative
        elif pred_key is None and gt_key is not None:
            metrics.detection.fn += 1
            metrics.detection.rc += 1
            component.fn = True

        # You have found a false positive
        elif pred_key is not None and gt_key is None:
            metrics.detection.fp += 1
            component.fp = True

        # Send the updated node / component back
        return metrics, component

    def _add_metrics(
        self, parent_metrics: List[Metric], child_metrics: List[Metric]
    ) -> List[Metric]:
        """
        Updates the subset metrics based on the file that has been
        recently evaluated.

        Arguments:
            - parent_metrics: List[Metric] : Metrics of the parent (file / subset)
                                             to be updated
            - child_metrics: List[Metric] : Metrics of the child (page) that will be
                                             used a reference

        Returns:
            - List[Metric] : The updated parent (file / subset) metric list
        """
        for iou_idx, iou_thr in enumerate(self.iou_list):

            parent_metrics[iou_idx].detection.tp += child_metrics[iou_idx].detection.tp
            parent_metrics[iou_idx].detection.fp += child_metrics[iou_idx].detection.fp
            parent_metrics[iou_idx].detection.fn += child_metrics[iou_idx].detection.fn
            parent_metrics[iou_idx].detection.rc += child_metrics[iou_idx].detection.rc

        return parent_metrics

    def _compute_page_metrics(
        self, page_component: PageComponents
    ) -> Tuple[List[Metric], List[PageComponents]]:
        """
        Calculates the confusion matrix for a file in the subset

        Arguments:
            - page_component (PageComponents) :
                        A pydantic model that holds all the connected components
                        to each region in the page

        Returns:
            - (List[Metric], List[PageComponents]) : A tuple of metrics and updated
                                connected components.
                            - List[Metric] -> Updated Metrics with detection scores
                            - List[PageComponents] : Each element has a PageComponents
                                 model with the list of connected components found on
                                 this page. Each PageComponents here is for an IOU value
                                 to be attached to the global lists of IOUComponents
                                 from the source call of this function.
                                 Each connected component having an
                                 updated TP, FP and FN flag
        """

        # --------- Calculate TP, FP and FN for this file ----------------------------

        # Captures the metrics at each IOU for a page
        iou_page_metrics_list: List[Metric] = generate_metrics_list(self.iou_list)

        iou_page_component_list: List[PageComponents] = []

        for iou_idx, iou_thr in enumerate(self.iou_list):

            iou_page_cc_list = []

            for components in page_component.components:

                # Create a copy of the page metric for this IOU
                iou_page_metric = copy.deepcopy(iou_page_metrics_list[iou_idx])
                # Create a copy of connected components of this page
                page_cc = copy.deepcopy(components)

                iou_page_metric, iou_component = self.calculate_positive_negative(
                    components.pkey,
                    components.tkey,
                    components.iou,
                    iou_thr,
                    iou_page_metric,
                    page_cc,
                )

                # Update the metrics for this iou at this page
                iou_page_metrics_list[iou_idx] = iou_page_metric
                # Update the list of TP, FP and FN flag set connected components
                iou_page_cc_list.append(iou_component)

            # Update the connected component list for this page
            # The TP, FP and FN flags are set for each component at this IOU
            iou_page_component = PageComponents(
                page=page_component.page, components=iou_page_cc_list
            )

            # Add Page component at this IOU for IOUComponents.pages
            iou_page_component_list.append(iou_page_component)

        # Calcualting precision recall for the page at each IOU
        upd_iou_page_metrics_list = self._calculate_precision_recall_f1(
            metrics_list=iou_page_metrics_list
        )

        return upd_iou_page_metrics_list, iou_page_component_list

    def _calculate_precision_recall_f1(
        self, metrics_list: List[Metric]
    ) -> List[Metric]:
        """Calculates the precision, recall and f1 score
        for all values within the list Metric pydantic
        models. This function is called:
        - When a single page is processed
        - When a single file is processed (Conditional to it having TP and RC values)
        - When a single subset is processed (Conditional to it having TP and RC values)

        Arguments:
            - List[Metric] : List of metric

        Returns:
            - List[Metric]: Updated list of metric where at each IOU the recall,
                            precision and f1 score are updated.

        """
        for metric in metrics_list:
            # Check to see if both values are 0
            if metric.detection.tp != 0 or metric.detection.fn != 0:
                # Not using false_negative here since our definition of false
                # negative does not account for gt_regions of a second class
                metric.detection.r = metric.detection.tp / metric.detection.rc
            else:
                metric.detection.r = 0

            # Check to see if both values are 0
            if metric.detection.tp != 0 or metric.detection.fp != 0:
                metric.detection.p = metric.detection.tp / (
                    metric.detection.tp + metric.detection.fp
                )
            else:
                metric.detection.p = 0

            # Check to see if both values are 0
            if metric.detection.p != 0 or metric.detection.r != 0:
                metric.detection.f1 = (
                    2
                    * (metric.detection.p * metric.detection.r)
                    / (metric.detection.p + metric.detection.r)
                )
            else:
                metric.detection.f1 = 0
        return metrics_list


def dict_average(list_of_dictionaries: list) -> None:
    """Calculates the average scores (CER / Accuracy)
    for each region and for each iou level

    Arguments:
        - list_of_dictionaries (list): List of dictionaries for
                each region in the file / subset. Each dictionary
                contains the CER / Accuracy at an IOU level

    Returns:
        - None
    """

    result = {}
    total_per_key = {}
    for d in list_of_dictionaries:
        for k, v in d.items():
            if k in result:
                result[k] += v
                total_per_key[k] += 1
            else:
                result[k] = v
                total_per_key[k] = 1

    for k in result:
        result[k] /= total_per_key[k]
    return result


def compare_labels(pred: str, label: str) -> Tuple[int, float]:
    """This function compares the prediction from the OCR
    and the groundtruth. The function returns the number of
    correct predictions and the character error rate

    Arguments:
        - pred (str): A string predicted by the OCR engine
        - label (str): A string of the ground truth

    Returns:
        - Tuple[correct_count (int), cer (float)]:
            The total number of correctly predicted words (correct_count)
            and the character error rate (cer)

    """
    if label is None or pred is None:
        cer = 1
    else:
        distance = Levenshtein.distance(label, pred)

        cer = distance / (max(len(label), len(pred)))

    if label == pred:
        correct_count = 1
    else:
        correct_count = 0

    return correct_count, cer


def evaluate_page_components(
    page_component: PageComponents,
) -> Tuple[QualityMetrics, bool]:
    """This function processes the components in a page

    Arguments:
        - page_component (PageComponents): An object containing the components in a page

    Returns:
        - Tuple[quality_metric (QualityMetrics), tp (bool)]:
            An object containing the cer and text accuracy for the page.
            And tp a boolean to tell if the page had any True positives.

    """

    gt_word_count = 0
    region_cer = 0
    file_correct_word_count = 0

    for component in page_component.components:
        # Process only True Positives
        if component.tp is True:

            gt_word_count += 1

            # Calculate CER and if pred_text == truth_text
            correct_count, cer = compare_labels(component.ptext, component.ttext)
            region_cer += cer
            file_correct_word_count += correct_count

    if gt_word_count == 0:
        ta = 0
        f_cer = 1
        tp = False

    else:
        ta = file_correct_word_count / gt_word_count
        f_cer = region_cer / gt_word_count
        tp = True

    quality_metric = QualityMetrics(ta=ta, cer=f_cer)
    return quality_metric, tp


def evaluate_iou_components(
    iou_component: IOUComponent, page_infos: List[PageInfo], iou_thr: float
) -> QualityMetrics:
    """This function computes quality metric of a file at an IOU threshold.

    Arguments:
        - iou_component (IOUComponent): An IOUComponent object containing the
                                        pages of a file
        - page_infos (List(PageInfo)): List of PageInfo objects for the file
        - iou_thr (float): an IOU threshold

    Returns:
        - iou_quality_metric (QualityMetric):
            An object containing the quality metrics for the file at the IOU
            threshold

    """
    page_ta_list = []
    page_cer_list = []
    for page, page_info in zip(iou_component.pages, page_infos):
        quality_metric, tp = evaluate_page_components(page)

        if tp:
            page_ta_list.append(quality_metric.ta)
            page_cer_list.append(quality_metric.cer)

        for metric in page_info.page_metrics:
            if metric.iou == iou_thr:
                metric.quality = quality_metric
    if sum(page_cer_list) > 0:
        iou_ta = mean(page_ta_list)
        iou_cer = mean(page_cer_list)
    else:
        iou_ta = 0
        iou_cer = 1
    iou_quality_metric = QualityMetrics(ta=iou_ta, cer=iou_cer)
    return iou_quality_metric


def evaluate_file_components(
    file_component: FileComponents, file_info: FileInfo
) -> None:
    """This function computes all quality metrics of a file.

    Arguments:
        - file_component (FileComponents): An object containing all the file
                                           components at different IOU thresholds
        - file_info (FileInfo): An object for storing the metrics for the file at the
                                different IOU thresholds

    Returns:
        - None

    """

    for iou_component, metric in zip(file_component.ious, file_info.file_metrics):
        quality_metric = evaluate_iou_components(
            iou_component, file_info.pages, iou_component.iou_thr
        )
        metric.quality = quality_metric


def evaluate_file_quality_metrics(
    file_infos: List[FileInfo], files: List[FileComponents]
) -> None:
    """This function computes all quality metrics for all the files in a subset.

    Arguments:
        - file_infos (List(FileInfo)): A list of FileInfo for each file
        - files (List(FileComponents)): A list of FileComponents for each file

    Returns:
        - None

    """
    for file_component, file_info in zip(files, file_infos):
        evaluate_file_components(file_component, file_info)


def evaluate_subset_quality_metrics(algorithm: Algorithm) -> None:
    """This function computes all quality metrics of a subset.

    Arguments:
        - algorithm (Algorithm): An object containing all the metrics
                                for all the files in the subset

    Returns:
        - None
    """
    iou_ta = []
    iou_cer = []
    for file in algorithm.files:
        file_iou_ta = {}
        file_iou_cer = {}
        for metric in file.file_metrics:

            iou = metric.iou
            file_iou_ta[iou] = metric.quality.ta
            file_iou_cer[iou] = metric.quality.cer

        iou_ta.append(file_iou_ta)
        iou_cer.append(file_iou_cer)

    iou_average_ta = dict_average(iou_ta)
    iou_average_cer = dict_average(iou_cer)
    for metric in algorithm.subset_metrics:
        iou = metric.iou
        metric.quality.cer = iou_average_cer.get(iou, 1)
        metric.quality.ta = iou_average_ta.get(iou, 0)
