from enum import Enum


class ModelNames(Enum):
    stanza = "en"
    spacy = "en_core_web_lg"
    bert = "dslim/bert-base-NER"
    flair_en = "flair/ner-english"
    flair_fr = "flair/ner-french"
